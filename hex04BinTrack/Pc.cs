using System;
using System.Collections.Generic;
using System.Text;
using SlimDX.Direct3D9;
using SlimDX;
using System.Runtime.InteropServices;

namespace hex04BinTrack {
    [StructLayout(LayoutKind.Sequential)]
    public struct Pc {
        public const VertexFormat Format = VertexFormat.Diffuse | VertexFormat.Position;

        public static int StrideSize { get { return UtVFS.Compute(Format); } }

        public float X;
        public float Y;
        public float Z;
        public int Color;

        public Pc(Vector3 pos, int c) {
            X = pos.X;
            Y = pos.Y;
            Z = pos.Z;
            Color = c;
        }
    }
}
