﻿namespace ee1Dec.C {
    using System.Collections.Generic;
    
    
    public partial class Mobrc2 {
        
        private ee1Dec.C.CustEE ee = new ee1Dec.C.CustEE();
        
        private SortedDictionary<uint, ee1Dec.C.MobUt.Tx8> dicti2a = new SortedDictionary<uint, ee1Dec.C.MobUt.Tx8>();
        
        public virtual void funct001283d8() {
            this.ee.pc = 1213440u;
            // @001283D8   LW t5, $0004(a0)
            MobUt.LW(this.ee, this.ee.t5, (4u + this.ee.a0.UL0));
            // @001283DC   LW t6, $0044(t5)
            MobUt.LW(this.ee, this.ee.t6, (68u + this.ee.t5.UL0));
            // @001283E0   LW t4, $0020(t5)
            MobUt.LW(this.ee, this.ee.t4, (32u + this.ee.t5.UL0));
            // @001283E4   ADDU t3, t5, t6
            this.ee.t3.SD0 = ((int)((this.ee.t5.SD0 + this.ee.t6.SD0)));
            // @001283E8   SLL t7, t4, 2
            MobUt.SLL(this.ee.t7, this.ee.t4, 2);
            // @001283EC   ADDU t7, t3, t7
            this.ee.t7.SD0 = ((int)((this.ee.t3.SD0 + this.ee.t7.SD0)));
            // @001283F0   LWC1 $f0, $fffc(t7)
            MobUt.LWC1(this.ee, this.ee.fpr[0], (4294967292u + this.ee.t7.UL0));
            // @001283F4   C.LE.S $f0, $f12
            this.ee.fcr31_23 = (this.ee.fpr[0].f <= this.ee.fpr[12].f);
            // @001283F8   BC1FL $00128414
            if ((this.ee.fcr31_23 == false)) {
                this.ee.pc = 1213460u;
                // @001283FC   LWC1 $f0, $0000(t3)
                MobUt.LWC1(this.ee, this.ee.fpr[0], (0u + this.ee.t3.UL0));
            }
        }
        
        public virtual void funct00128400() {
            this.ee.pc = 1213460u;
            // @00128400   ADDIU t7, t4, $ffff
            this.ee.t7.SD0 = ((int)((this.ee.t4.SD0 + -1)));
            // @00128404   SW t7, $0000(a1)
            MobUt.SW(this.ee, this.ee.t7, (0u + this.ee.a1.UL0));
            // @00128408   LW t6, $0020(t5)
            MobUt.LW(this.ee, this.ee.t6, (32u + this.ee.t5.UL0));
            // @0012840C   JR ra
            this.ee.pc = this.ee.ra.UL[0];
            // @00128410   SW t6, $0000(a2)
            MobUt.SW(this.ee, this.ee.t6, (0u + this.ee.a2.UL0));
        }
        
        public virtual void funct00128414() {
            this.ee.pc = 1213472u;
            // @00128414   C.LE.S $f12, $f0
            this.ee.fcr31_23 = (this.ee.fpr[12].f <= this.ee.fpr[0].f);
            // @00128418   BC1FL $00128430
            if ((this.ee.fcr31_23 == false)) {
                this.ee.pc = 1213488u;
                // @0012841C   SW zero, $0000(a1)
                MobUt.SW(this.ee, this.ee.r0, (0u + this.ee.a1.UL0));
            }
        }
        
        public virtual void funct00128420() {
            this.ee.pc = 1213488u;
            // @00128420   ADDIU t7, zero, $ffff
            this.ee.t7.SD0 = ((int)((this.ee.r0.SD0 + -1)));
            // @00128424   SW t7, $0000(a1)
            MobUt.SW(this.ee, this.ee.t7, (0u + this.ee.a1.UL0));
            // @00128428   JR ra
            this.ee.pc = this.ee.ra.UL[0];
            // @0012842C   SW zero, $0000(a2)
            MobUt.SW(this.ee, this.ee.r0, (0u + this.ee.a2.UL0));
        }
        
        public virtual void funct00128430() {
            this.ee.pc = 1213520u;
            // @00128430   LW t7, $0020(t5)
            MobUt.LW(this.ee, this.ee.t7, (32u + this.ee.t5.UL0));
            // @00128434   ADDIU t6, t7, $ffff
            this.ee.t6.SD0 = ((int)((this.ee.t7.SD0 + -1)));
            // @00128438   SW t6, $0000(a2)
            MobUt.SW(this.ee, this.ee.t6, (0u + this.ee.a2.UL0));
            // @0012843C   LW t5, $0000(a1)
            MobUt.LW(this.ee, this.ee.t5, (0u + this.ee.a1.UL0));
            // @00128440   SUBU t7, t6, t5
            this.ee.t7.SD0 = ((int)((this.ee.t6.UD0 - this.ee.t5.UD0)));
            // @00128444   SLTI t7, t7, $0002
            this.ee.t7.UD0 = System.Convert.ToByte((this.ee.t7.SD0 < 2));
            // @00128448   BNE t7, zero, $00128490
            if ((this.ee.t7.UD0 != this.ee.r0.UD0)) {
                this.ee.pc = 1213584u;
            }
            // @0012844C   ADDU t7, t5, t6
            this.ee.t7.SD0 = ((int)((this.ee.t5.SD0 + this.ee.t6.SD0)));
        }
        
        public virtual void funct00128450() {
            this.ee.pc = 1213556u;
            // @00128450   SRL t6, t7, 31
            MobUt.SRL(this.ee.t6, this.ee.t7, 31);
            // @00128454   ADDU t7, t7, t6
            this.ee.t7.SD0 = ((int)((this.ee.t7.SD0 + this.ee.t6.SD0)));
            // @00128458   SRA t7, t7, 1
            MobUt.SRA(this.ee.t7, this.ee.t7, 1);
            // @0012845C   SLL t6, t7, 2
            MobUt.SLL(this.ee.t6, this.ee.t7, 2);
            // @00128460   ADDU t6, t6, t3
            this.ee.t6.SD0 = ((int)((this.ee.t6.SD0 + this.ee.t3.SD0)));
            // @00128464   LWC1 $f0, $0000(t6)
            MobUt.LWC1(this.ee, this.ee.fpr[0], (0u + this.ee.t6.UL0));
            // @00128468   C.LT.S $f12, $f0
            this.ee.fcr31_23 = (this.ee.fpr[12].f < this.ee.fpr[0].f);
            // @0012846C   BC1FL $00128498
            if ((this.ee.fcr31_23 == false)) {
                this.ee.pc = 1213592u;
                // @00128470   C.LT.S $f0, $f12
                this.ee.fcr31_23 = (this.ee.fpr[0].f < this.ee.fpr[12].f);
            }
        }
        
        public virtual void funct00128474() {
            this.ee.pc = 1213584u;
            // @00128474   SW t7, $0000(a2)
            MobUt.SW(this.ee, this.ee.t7, (0u + this.ee.a2.UL0));
            // @00128478   LW t6, $0000(a2)
            MobUt.LW(this.ee, this.ee.t6, (0u + this.ee.a2.UL0));
            // @0012847C   LW t5, $0000(a1)
            MobUt.LW(this.ee, this.ee.t5, (0u + this.ee.a1.UL0));
            // @00128480   SUBU t7, t6, t5
            this.ee.t7.SD0 = ((int)((this.ee.t6.UD0 - this.ee.t5.UD0)));
            // @00128484   SLTI t7, t7, $0002
            this.ee.t7.UD0 = System.Convert.ToByte((this.ee.t7.SD0 < 2));
            // @00128488   BEQ t7, zero, $00128450
            if ((this.ee.t7.UD0 == this.ee.r0.UD0)) {
                this.ee.pc = 1213520u;
            }
            // @0012848C   ADDU t7, t5, t6
            this.ee.t7.SD0 = ((int)((this.ee.t5.SD0 + this.ee.t6.SD0)));
        }
        
        public virtual void funct00128478() {
            this.ee.pc = 1213584u;
            // @00128478   LW t6, $0000(a2)
            MobUt.LW(this.ee, this.ee.t6, (0u + this.ee.a2.UL0));
            // @0012847C   LW t5, $0000(a1)
            MobUt.LW(this.ee, this.ee.t5, (0u + this.ee.a1.UL0));
            // @00128480   SUBU t7, t6, t5
            this.ee.t7.SD0 = ((int)((this.ee.t6.UD0 - this.ee.t5.UD0)));
            // @00128484   SLTI t7, t7, $0002
            this.ee.t7.UD0 = System.Convert.ToByte((this.ee.t7.SD0 < 2));
            // @00128488   BEQ t7, zero, $00128450
            if ((this.ee.t7.UD0 == this.ee.r0.UD0)) {
                this.ee.pc = 1213520u;
            }
            // @0012848C   ADDU t7, t5, t6
            this.ee.t7.SD0 = ((int)((this.ee.t5.SD0 + this.ee.t6.SD0)));
        }
        
        public virtual void funct00128490() {
            this.ee.pc = 1213592u;
            // @00128490   JR ra
            this.ee.pc = this.ee.ra.UL[0];
            // @00128494   NOP 
            MobUt.Latency();
        }
        
        public virtual void funct00128498() {
            this.ee.pc = 1213600u;
            // @00128498   BC1FL $001284a8
            if ((this.ee.fcr31_23 == false)) {
                this.ee.pc = 1213608u;
                // @0012849C   SW t7, $0000(a2)
                MobUt.SW(this.ee, this.ee.t7, (0u + this.ee.a2.UL0));
            }
        }
        
        public virtual void funct001284a0() {
            this.ee.pc = 1213608u;
            // @001284A0   BEQ zero, zero, $00128478
            if ((this.ee.r0.UD0 == this.ee.r0.UD0)) {
                this.ee.pc = 1213560u;
            }
            // @001284A4   SW t7, $0000(a1)
            MobUt.SW(this.ee, this.ee.t7, (0u + this.ee.a1.UL0));
        }
        
        public virtual void funct001284a8() {
            this.ee.pc = 1213616u;
            // @001284A8   JR ra
            this.ee.pc = this.ee.ra.UL[0];
            // @001284AC   SW t7, $0000(a1)
            MobUt.SW(this.ee, this.ee.t7, (0u + this.ee.a1.UL0));
        }
        
        public virtual void funct001284b0() {
            this.ee.pc = 1213744u;
            // @001284B0   SUB.S $f16, $f16, $f13
            this.ee.fpr[16].f = (this.ee.fpr[16].f - this.ee.fpr[13].f);
            // @001284B4   LUI t7, $0038
            this.ee.t7.SD0 = 3670016;
            // @001284B8   ADDIU t7, t7, $823c
            this.ee.t7.SD0 = ((int)((this.ee.t7.SD0 + -32196)));
            // @001284BC   SUB.S $f12, $f12, $f13
            this.ee.fpr[12].f = (this.ee.fpr[12].f - this.ee.fpr[13].f);
            // @001284C0   LWC1 $f7, $0000(t7)
            MobUt.LWC1(this.ee, this.ee.fpr[7], (0u + this.ee.t7.UL0));
            // @001284C4   NOP 
            MobUt.Latency();
            // @001284C8   NOP 
            MobUt.Latency();
            // @001284CC   DIV.S $f16, $f7, $f16
            this.ee.fpr[16].f = (this.ee.fpr[7].f / this.ee.fpr[16].f);
            // @001284D0   LUI t7, $0038
            this.ee.t7.SD0 = 3670016;
            // @001284D4   MUL.S $f5, $f12, $f12
            this.ee.fpr[5].f = (this.ee.fpr[12].f * this.ee.fpr[12].f);
            // @001284D8   ADDIU t7, t7, $8240
            this.ee.t7.SD0 = ((int)((this.ee.t7.SD0 + -32192)));
            // @001284DC   LWC1 $f3, $0000(t7)
            MobUt.LWC1(this.ee, this.ee.fpr[3], (0u + this.ee.t7.UL0));
            // @001284E0   MUL.S $f2, $f5, $f12
            this.ee.fpr[2].f = (this.ee.fpr[5].f * this.ee.fpr[12].f);
            // @001284E4   MUL.S $f3, $f5, $f3
            this.ee.fpr[3].f = (this.ee.fpr[5].f * this.ee.fpr[3].f);
            // @001284E8   MUL.S $f1, $f16, $f16
            this.ee.fpr[1].f = (this.ee.fpr[16].f * this.ee.fpr[16].f);
            // @001284EC   MUL.S $f5, $f5, $f16
            this.ee.fpr[5].f = (this.ee.fpr[5].f * this.ee.fpr[16].f);
            // @001284F0   MUL.S $f2, $f2, $f1
            this.ee.fpr[2].f = (this.ee.fpr[2].f * this.ee.fpr[1].f);
            // @001284F4   MUL.S $f3, $f3, $f1
            this.ee.fpr[3].f = (this.ee.fpr[3].f * this.ee.fpr[1].f);
            // @001284F8   ADD.S $f6, $f5, $f5
            this.ee.fpr[6].f = (this.ee.fpr[5].f + this.ee.fpr[5].f);
            // @001284FC   ADD.S $f4, $f2, $f2
            this.ee.fpr[4].f = (this.ee.fpr[2].f + this.ee.fpr[2].f);
            // @00128500   SUB.S $f5, $f2, $f5
            this.ee.fpr[5].f = (this.ee.fpr[2].f - this.ee.fpr[5].f);
            // @00128504   MUL.S $f4, $f4, $f16
            this.ee.fpr[4].f = (this.ee.fpr[4].f * this.ee.fpr[16].f);
            // @00128508   SUB.S $f2, $f2, $f6
            this.ee.fpr[2].f = (this.ee.fpr[2].f - this.ee.fpr[6].f);
            // @0012850C   SUB.S $f1, $f4, $f3
            this.ee.fpr[1].f = (this.ee.fpr[4].f - this.ee.fpr[3].f);
            // @00128510   ADD.S $f2, $f2, $f12
            this.ee.fpr[2].f = (this.ee.fpr[2].f + this.ee.fpr[12].f);
            // @00128514   ADD.S $f1, $f1, $f7
            this.ee.fpr[1].f = (this.ee.fpr[1].f + this.ee.fpr[7].f);
            // @00128518   SUB.S $f3, $f3, $f4
            this.ee.fpr[3].f = (this.ee.fpr[3].f - this.ee.fpr[4].f);
            // @0012851C   MULA.S $f14, $f1
            this.ee.fpracc.f = (this.ee.fpr[14].f * this.ee.fpr[1].f);
            // @00128520   MADDA.S $f17, $f3
            this.ee.fpracc.f = ee1Dec.C.MobUt.MADD(this.ee.fpracc.f, this.ee.fpr[17].f, this.ee.fpr[3].f);
            // @00128524   MADDA.S $f15, $f2
            this.ee.fpracc.f = ee1Dec.C.MobUt.MADD(this.ee.fpracc.f, this.ee.fpr[15].f, this.ee.fpr[2].f);
            // @00128528   JR ra
            this.ee.pc = this.ee.ra.UL[0];
            // @0012852C   MADD.S $f0, $f18, $f5
            this.ee.fpr[0].f = ee1Dec.C.MobUt.MADD(this.ee.fpracc.f, this.ee.fpr[18].f, this.ee.fpr[5].f);
        }
        
        public virtual void funct00128530() {
            this.ee.pc = 1213892u;
            // @00128530   ADDIU sp, sp, $ffa0
            this.ee.sp.SD0 = ((int)((this.ee.sp.SD0 + -96)));
            // @00128534   LW t5, $0004(a0)
            MobUt.LW(this.ee, this.ee.t5, (4u + this.ee.a0.UL0));
            // @00128538   SD s0, $0010(sp)
            MobUt.SD(this.ee, this.ee.s0, (16u + this.ee.sp.UL0));
            // @0012853C   SD s1, $0018(sp)
            MobUt.SD(this.ee, this.ee.s1, (24u + this.ee.sp.UL0));
            // @00128540   SD s2, $0020(sp)
            MobUt.SD(this.ee, this.ee.s2, (32u + this.ee.sp.UL0));
            // @00128544   DADDU s0, a1, zero
            this.ee.s0.UD0 = (this.ee.a1.UD0 + this.ee.r0.UD0);
            // @00128548   SD s3, $0028(sp)
            MobUt.SD(this.ee, this.ee.s3, (40u + this.ee.sp.UL0));
            // @0012854C   SD s4, $0030(sp)
            MobUt.SD(this.ee, this.ee.s4, (48u + this.ee.sp.UL0));
            // @00128550   SD s5, $0038(sp)
            MobUt.SD(this.ee, this.ee.s5, (56u + this.ee.sp.UL0));
            // @00128554   SD s6, $0040(sp)
            MobUt.SD(this.ee, this.ee.s6, (64u + this.ee.sp.UL0));
            // @00128558   SWC1 $f20, $0050(sp)
            MobUt.SWC1(this.ee, this.ee.fpr[20], (80u + this.ee.sp.UL0));
            // @0012855C   SW a3, $0004(sp)
            MobUt.SW(this.ee, this.ee.a3, (4u + this.ee.sp.UL0));
            // @00128560   SD ra, $0048(sp)
            MobUt.SD(this.ee, this.ee.ra, (72u + this.ee.sp.UL0));
            // @00128564   MOV.S $f20, $f12
            this.ee.fpr[20].f = this.ee.fpr[12].f;
            // @00128568   SW a2, $0000(sp)
            MobUt.SW(this.ee, this.ee.a2, (0u + this.ee.sp.UL0));
            // @0012856C   LHU t0, $0004(a1)
            MobUt.LHU(this.ee, this.ee.t0, (4u + this.ee.a1.UL0));
            // @00128570   LW t6, $0040(t5)
            MobUt.LW(this.ee, this.ee.t6, (64u + this.ee.t5.UL0));
            // @00128574   ANDI t4, t0, $ffff
            this.ee.t4.UD0 = ((ushort)((this.ee.t0.US0 & 65535)));
            // @00128578   LBU t1, $0003(a1)
            MobUt.LBU(this.ee, this.ee.t1, (3u + this.ee.a1.UL0));
            // @0012857C   ADDU s2, t5, t6
            this.ee.s2.SD0 = ((int)((this.ee.t5.SD0 + this.ee.t6.SD0)));
            // @00128580   LW t3, $0048(t5)
            MobUt.LW(this.ee, this.ee.t3, (72u + this.ee.t5.UL0));
            // @00128584   SLL t7, t4, 3
            MobUt.SLL(this.ee.t7, this.ee.t4, 3);
            // @00128588   ANDI t6, t1, $00ff
            this.ee.t6.UD0 = ((ushort)((this.ee.t1.US0 & 255)));
            // @0012858C   ADDU s1, s2, t7
            this.ee.s1.SD0 = ((int)((this.ee.s2.SD0 + this.ee.t7.SD0)));
            // @00128590   ADDU t4, t4, t6
            this.ee.t4.SD0 = ((int)((this.ee.t4.SD0 + this.ee.t6.SD0)));
            // @00128594   LHU t2, $0000(s1)
            MobUt.LHU(this.ee, this.ee.t2, (0u + this.ee.s1.UL0));
            // @00128598   SLL t4, t4, 3
            MobUt.SLL(this.ee.t4, this.ee.t4, 3);
            // @0012859C   LW t6, $0044(t5)
            MobUt.LW(this.ee, this.ee.t6, (68u + this.ee.t5.UL0));
            // @001285A0   ADDU t4, s2, t4
            this.ee.t4.SD0 = ((int)((this.ee.s2.SD0 + this.ee.t4.SD0)));
            // @001285A4   LW t7, $004c(t5)
            MobUt.LW(this.ee, this.ee.t7, (76u + this.ee.t5.UL0));
            // @001285A8   SRL t2, t2, 2
            MobUt.SRL(this.ee.t2, this.ee.t2, 2);
            // @001285AC   ADDU s3, t5, t6
            this.ee.s3.SD0 = ((int)((this.ee.t5.SD0 + this.ee.t6.SD0)));
            // @001285B0   ADDU s5, t5, t3
            this.ee.s5.SD0 = ((int)((this.ee.t5.SD0 + this.ee.t3.SD0)));
            // @001285B4   ADDU s6, t5, t7
            this.ee.s6.SD0 = ((int)((this.ee.t5.SD0 + this.ee.t7.SD0)));
            // @001285B8   SLT a3, t2, a3
            this.ee.a3.UD0 = System.Convert.ToByte((this.ee.t2.SD0 < this.ee.a3.SD0));
            // @001285BC   BNE a3, zero, $00128840
            if ((this.ee.a3.UD0 != this.ee.r0.UD0)) {
                this.ee.pc = 1214528u;
            }
            // @001285C0   ADDIU s4, t4, $fff8
            this.ee.s4.SD0 = ((int)((this.ee.t4.SD0 + -8)));
        }
        
        public virtual void funct001285c4() {
            this.ee.pc = 1213916u;
            // @001285C4   LHU t7, $0002(a1)
            MobUt.LHU(this.ee, this.ee.t7, (2u + this.ee.a1.UL0));
            // @001285C8   ADDIU t6, zero, $0001
            this.ee.t6.SD0 = ((int)((this.ee.r0.SD0 + 1)));
            // @001285CC   SRL t7, t7, 4
            MobUt.SRL(this.ee.t7, this.ee.t7, 4);
            // @001285D0   ANDI t5, t7, $0003
            this.ee.t5.UD0 = ((ushort)((this.ee.t7.US0 & 3)));
            // @001285D4   BEQ t5, t6, $00128804
            if ((this.ee.t5.UD0 == this.ee.t6.UD0)) {
                this.ee.pc = 1214468u;
            }
            // @001285D8   SLTI t7, t5, $0002
            this.ee.t7.UD0 = System.Convert.ToByte((this.ee.t5.SD0 < 2));
        }
        
        public virtual void funct001285dc() {
            this.ee.pc = 1213924u;
            // @001285DC   BEQL t7, zero, $0012862c
            if ((this.ee.t7.UD0 == this.ee.r0.UD0)) {
                this.ee.pc = 1213996u;
                // @001285E0   ADDIU t7, zero, $0002
                this.ee.t7.SD0 = ((int)((this.ee.r0.SD0 + 2)));
            }
        }
        
        public virtual void funct001285e4() {
            this.ee.pc = 1213932u;
            // @001285E4   BEQL t5, zero, $0012861c
            if ((this.ee.t5.UD0 == this.ee.r0.UD0)) {
                this.ee.pc = 1213980u;
                // @001285E8   LHU t7, $0002(s1)
                MobUt.LHU(this.ee, this.ee.t7, (2u + this.ee.s1.UL0));
            }
        }
        
        public virtual void funct001285f0() {
            this.ee.pc = 1213980u;
            // @001285F0   LD s0, $0010(sp)
            MobUt.LD(this.ee, this.ee.s0, (16u + this.ee.sp.UL0));
            // @001285F4   LD s1, $0018(sp)
            MobUt.LD(this.ee, this.ee.s1, (24u + this.ee.sp.UL0));
            // @001285F8   LD s2, $0020(sp)
            MobUt.LD(this.ee, this.ee.s2, (32u + this.ee.sp.UL0));
            // @001285FC   LD s3, $0028(sp)
            MobUt.LD(this.ee, this.ee.s3, (40u + this.ee.sp.UL0));
            // @00128600   LD s4, $0030(sp)
            MobUt.LD(this.ee, this.ee.s4, (48u + this.ee.sp.UL0));
            // @00128604   LD s5, $0038(sp)
            MobUt.LD(this.ee, this.ee.s5, (56u + this.ee.sp.UL0));
            // @00128608   LD s6, $0040(sp)
            MobUt.LD(this.ee, this.ee.s6, (64u + this.ee.sp.UL0));
            // @0012860C   LD ra, $0048(sp)
            MobUt.LD(this.ee, this.ee.ra, (72u + this.ee.sp.UL0));
            // @00128610   LWC1 $f20, $0050(sp)
            MobUt.LWC1(this.ee, this.ee.fpr[20], (80u + this.ee.sp.UL0));
            // @00128614   JR ra
            this.ee.pc = this.ee.ra.UL[0];
            // @00128618   ADDIU sp, sp, $0060
            this.ee.sp.SD0 = ((int)((this.ee.sp.SD0 + 96)));
        }
        
        public virtual void funct001285f4() {
            this.ee.pc = 1213980u;
            // @001285F4   LD s1, $0018(sp)
            MobUt.LD(this.ee, this.ee.s1, (24u + this.ee.sp.UL0));
            // @001285F8   LD s2, $0020(sp)
            MobUt.LD(this.ee, this.ee.s2, (32u + this.ee.sp.UL0));
            // @001285FC   LD s3, $0028(sp)
            MobUt.LD(this.ee, this.ee.s3, (40u + this.ee.sp.UL0));
            // @00128600   LD s4, $0030(sp)
            MobUt.LD(this.ee, this.ee.s4, (48u + this.ee.sp.UL0));
            // @00128604   LD s5, $0038(sp)
            MobUt.LD(this.ee, this.ee.s5, (56u + this.ee.sp.UL0));
            // @00128608   LD s6, $0040(sp)
            MobUt.LD(this.ee, this.ee.s6, (64u + this.ee.sp.UL0));
            // @0012860C   LD ra, $0048(sp)
            MobUt.LD(this.ee, this.ee.ra, (72u + this.ee.sp.UL0));
            // @00128610   LWC1 $f20, $0050(sp)
            MobUt.LWC1(this.ee, this.ee.fpr[20], (80u + this.ee.sp.UL0));
            // @00128614   JR ra
            this.ee.pc = this.ee.ra.UL[0];
            // @00128618   ADDIU sp, sp, $0060
            this.ee.sp.SD0 = ((int)((this.ee.sp.SD0 + 96)));
        }
        
        public virtual void funct0012861c() {
            this.ee.pc = 1213996u;
            // @0012861C   SLL t7, t7, 2
            MobUt.SLL(this.ee.t7, this.ee.t7, 2);
            // @00128620   ADDU t7, t7, s5
            this.ee.t7.SD0 = ((int)((this.ee.t7.SD0 + this.ee.s5.SD0)));
            // @00128624   BEQ zero, zero, $001285f0
            if ((this.ee.r0.UD0 == this.ee.r0.UD0)) {
                this.ee.pc = 1213936u;
            }
            // @00128628   LWC1 $f0, $0000(t7)
            MobUt.LWC1(this.ee, this.ee.fpr[0], (0u + this.ee.t7.UL0));
        }
        
        public virtual void funct0012862c() {
            this.ee.pc = 1214004u;
            // @0012862C   BNEL t5, t7, $001285f0
            if ((this.ee.t5.UD0 != this.ee.t7.UD0)) {
                this.ee.pc = 1213936u;
                // @00128630   MTC1 zero, $f0
                this.ee.fpr[0].f = MobUt.UL2F(this.ee.r0.UL[0]);
            }
        }
        
        public virtual void funct00128634() {
            this.ee.pc = 1214044u;
            // @00128634   LHU t7, $fff8(t4)
            MobUt.LHU(this.ee, this.ee.t7, (4294967288u + this.ee.t4.UL0));
            // @00128638   SLL t6, t2, 2
            MobUt.SLL(this.ee.t6, this.ee.t2, 2);
            // @0012863C   ADDU t6, t6, s3
            this.ee.t6.SD0 = ((int)((this.ee.t6.SD0 + this.ee.s3.SD0)));
            // @00128640   ANDI t7, t7, $fffc
            this.ee.t7.UD0 = ((ushort)((this.ee.t7.US0 & 65532)));
            // @00128644   LWC1 $f2, $0000(t6)
            MobUt.LWC1(this.ee, this.ee.fpr[2], (0u + this.ee.t6.UL0));
            // @00128648   ADDU t7, t7, s3
            this.ee.t7.SD0 = ((int)((this.ee.t7.SD0 + this.ee.s3.SD0)));
            // @0012864C   LWC1 $f0, $0000(t7)
            MobUt.LWC1(this.ee, this.ee.fpr[0], (0u + this.ee.t7.UL0));
            // @00128650   C.LT.S $f12, $f2
            this.ee.fcr31_23 = (this.ee.fpr[12].f < this.ee.fpr[2].f);
            // @00128654   BC1F $00128670
            if ((this.ee.fcr31_23 == false)) {
                this.ee.pc = 1214064u;
            }
            // @00128658   SUB.S $f0, $f0, $f2
            this.ee.fpr[0].f = (this.ee.fpr[0].f - this.ee.fpr[2].f);
        }
        
        public virtual void funct0012865c() {
            this.ee.pc = 1214060u;
            // @0012865C   ADD.S $f12, $f12, $f0
            this.ee.fpr[12].f = (this.ee.fpr[12].f + this.ee.fpr[0].f);
            // @00128660   C.LT.S $f12, $f2
            this.ee.fcr31_23 = (this.ee.fpr[12].f < this.ee.fpr[2].f);
            // @00128664   BC1TL $00128660
            if ((this.ee.fcr31_23 == true)) {
                this.ee.pc = 1214048u;
                // @00128668   ADD.S $f12, $f12, $f0
                this.ee.fpr[12].f = (this.ee.fpr[12].f + this.ee.fpr[0].f);
            }
        }
        
        public virtual void funct0012866c() {
            this.ee.pc = 1214080u;
            // @0012866C   MOV.S $f20, $f12
            this.ee.fpr[20].f = this.ee.fpr[12].f;
            // @00128670   MOV.S $f12, $f20
            this.ee.fpr[12].f = this.ee.fpr[20].f;
            // @00128674   DADDU a1, sp, zero
            this.ee.a1.UD0 = (this.ee.sp.UD0 + this.ee.r0.UD0);
            // @00128678   JAL $001283d8
            this.ee.ra.UL0 = 1214080u;
            this.ee.pc = 1213400u;
            // @0012867C   ADDIU a2, sp, $0004
            this.ee.a2.SD0 = ((int)((this.ee.sp.SD0 + 4)));
        }
        
        public virtual void funct00128670() {
            this.ee.pc = 1214080u;
            // @00128670   MOV.S $f12, $f20
            this.ee.fpr[12].f = this.ee.fpr[20].f;
            // @00128674   DADDU a1, sp, zero
            this.ee.a1.UD0 = (this.ee.sp.UD0 + this.ee.r0.UD0);
            // @00128678   JAL $001283d8
            this.ee.ra.UL0 = 1214080u;
            this.ee.pc = 1213400u;
            // @0012867C   ADDIU a2, sp, $0004
            this.ee.a2.SD0 = ((int)((this.ee.sp.SD0 + 4)));
        }
        
        public virtual void funct00128680() {
            this.ee.pc = 1214120u;
            // @00128680   LBU t1, $0003(s0)
            MobUt.LBU(this.ee, this.ee.t1, (3u + this.ee.s0.UL0));
            // @00128684   LHU t0, $0004(s0)
            MobUt.LHU(this.ee, this.ee.t0, (4u + this.ee.s0.UL0));
            // @00128688   ANDI t4, t0, $ffff
            this.ee.t4.UD0 = ((ushort)((this.ee.t0.US0 & 65535)));
            // @0012868C   ANDI t7, t1, $00ff
            this.ee.t7.UD0 = ((ushort)((this.ee.t1.US0 & 255)));
            // @00128690   ADDU t7, t4, t7
            this.ee.t7.SD0 = ((int)((this.ee.t4.SD0 + this.ee.t7.SD0)));
            // @00128694   ADDIU a3, t7, $ffff
            this.ee.a3.SD0 = ((int)((this.ee.t7.SD0 + -1)));
            // @00128698   SUBU t0, a3, t4
            this.ee.t0.SD0 = ((int)((this.ee.a3.UD0 - this.ee.t4.UD0)));
            // @0012869C   SLTI t7, t0, $0002
            this.ee.t7.UD0 = System.Convert.ToByte((this.ee.t0.SD0 < 2));
            // @001286A0   BNEL t7, zero, $00128708
            if ((this.ee.t7.UD0 != this.ee.r0.UD0)) {
                this.ee.pc = 1214216u;
                // @001286A4   LHU t4, $0000(s1)
                MobUt.LHU(this.ee, this.ee.t4, (0u + this.ee.s1.UL0));
            }
        }
        
        public virtual void funct0012868c() {
            this.ee.pc = 1214120u;
            // @0012868C   ANDI t7, t1, $00ff
            this.ee.t7.UD0 = ((ushort)((this.ee.t1.US0 & 255)));
            // @00128690   ADDU t7, t4, t7
            this.ee.t7.SD0 = ((int)((this.ee.t4.SD0 + this.ee.t7.SD0)));
            // @00128694   ADDIU a3, t7, $ffff
            this.ee.a3.SD0 = ((int)((this.ee.t7.SD0 + -1)));
            // @00128698   SUBU t0, a3, t4
            this.ee.t0.SD0 = ((int)((this.ee.a3.UD0 - this.ee.t4.UD0)));
            // @0012869C   SLTI t7, t0, $0002
            this.ee.t7.UD0 = System.Convert.ToByte((this.ee.t0.SD0 < 2));
            // @001286A0   BNEL t7, zero, $00128708
            if ((this.ee.t7.UD0 != this.ee.r0.UD0)) {
                this.ee.pc = 1214216u;
                // @001286A4   LHU t4, $0000(s1)
                MobUt.LHU(this.ee, this.ee.t4, (0u + this.ee.s1.UL0));
            }
        }
        
        public virtual void funct001286a8() {
            this.ee.pc = 1214176u;
            // @001286A8   ADDU t7, t4, a3
            this.ee.t7.SD0 = ((int)((this.ee.t4.SD0 + this.ee.a3.SD0)));
            // @001286AC   SRL t6, t7, 31
            MobUt.SRL(this.ee.t6, this.ee.t7, 31);
            // @001286B0   ADDU t7, t7, t6
            this.ee.t7.SD0 = ((int)((this.ee.t7.SD0 + this.ee.t6.SD0)));
            // @001286B4   SRA t3, t7, 1
            MobUt.SRA(this.ee.t3, this.ee.t7, 1);
            // @001286B8   SLL t6, t3, 3
            MobUt.SLL(this.ee.t6, this.ee.t3, 3);
            // @001286BC   ADDU t1, s2, t6
            this.ee.t1.SD0 = ((int)((this.ee.s2.SD0 + this.ee.t6.SD0)));
            // @001286C0   LHU t5, $0000(t1)
            MobUt.LHU(this.ee, this.ee.t5, (0u + this.ee.t1.UL0));
            // @001286C4   SRL t5, t5, 2
            MobUt.SRL(this.ee.t5, this.ee.t5, 2);
            // @001286C8   SLL t7, t5, 2
            MobUt.SLL(this.ee.t7, this.ee.t5, 2);
            // @001286CC   ADDU t7, t7, s3
            this.ee.t7.SD0 = ((int)((this.ee.t7.SD0 + this.ee.s3.SD0)));
            // @001286D0   LWC1 $f0, $0000(t7)
            MobUt.LWC1(this.ee, this.ee.fpr[0], (0u + this.ee.t7.UL0));
            // @001286D4   C.EQ.S $f0, $f20
            this.ee.fcr31_23 = (this.ee.fpr[0].f == this.ee.fpr[20].f);
            // @001286D8   BC1T $001287fc
            if (this.ee.fcr31_23) {
                this.ee.pc = 1214460u;
            }
            // @001286DC   LW t6, $0000(sp)
            MobUt.LW(this.ee, this.ee.t6, (0u + this.ee.sp.UL0));
        }
        
        public virtual void funct001286ac() {
            this.ee.pc = 1214176u;
            // @001286AC   SRL t6, t7, 31
            MobUt.SRL(this.ee.t6, this.ee.t7, 31);
            // @001286B0   ADDU t7, t7, t6
            this.ee.t7.SD0 = ((int)((this.ee.t7.SD0 + this.ee.t6.SD0)));
            // @001286B4   SRA t3, t7, 1
            MobUt.SRA(this.ee.t3, this.ee.t7, 1);
            // @001286B8   SLL t6, t3, 3
            MobUt.SLL(this.ee.t6, this.ee.t3, 3);
            // @001286BC   ADDU t1, s2, t6
            this.ee.t1.SD0 = ((int)((this.ee.s2.SD0 + this.ee.t6.SD0)));
            // @001286C0   LHU t5, $0000(t1)
            MobUt.LHU(this.ee, this.ee.t5, (0u + this.ee.t1.UL0));
            // @001286C4   SRL t5, t5, 2
            MobUt.SRL(this.ee.t5, this.ee.t5, 2);
            // @001286C8   SLL t7, t5, 2
            MobUt.SLL(this.ee.t7, this.ee.t5, 2);
            // @001286CC   ADDU t7, t7, s3
            this.ee.t7.SD0 = ((int)((this.ee.t7.SD0 + this.ee.s3.SD0)));
            // @001286D0   LWC1 $f0, $0000(t7)
            MobUt.LWC1(this.ee, this.ee.fpr[0], (0u + this.ee.t7.UL0));
            // @001286D4   C.EQ.S $f0, $f20
            this.ee.fcr31_23 = (this.ee.fpr[0].f == this.ee.fpr[20].f);
            // @001286D8   BC1T $001287fc
            if (this.ee.fcr31_23) {
                this.ee.pc = 1214460u;
            }
            // @001286DC   LW t6, $0000(sp)
            MobUt.LW(this.ee, this.ee.t6, (0u + this.ee.sp.UL0));
        }
        
        public virtual void funct001286e0() {
            this.ee.pc = 1214188u;
            // @001286E0   SLT t7, t5, t6
            this.ee.t7.UD0 = System.Convert.ToByte((this.ee.t5.SD0 < this.ee.t6.SD0));
            // @001286E4   BEQ t7, zero, $001287c0
            if ((this.ee.t7.UD0 == this.ee.r0.UD0)) {
                this.ee.pc = 1214400u;
            }
            // @001286E8   LW t2, $0004(sp)
            MobUt.LW(this.ee, this.ee.t2, (4u + this.ee.sp.UL0));
        }
        
        public virtual void funct001286ec() {
            this.ee.pc = 1214212u;
            // @001286EC   DADDU t4, t3, zero
            this.ee.t4.UD0 = (this.ee.t3.UD0 + this.ee.r0.UD0);
            // @001286F0   DADDU s1, t1, zero
            this.ee.s1.UD0 = (this.ee.t1.UD0 + this.ee.r0.UD0);
            // @001286F4   SUBU t0, a3, t4
            this.ee.t0.SD0 = ((int)((this.ee.a3.UD0 - this.ee.t4.UD0)));
            // @001286F8   SLTI t7, t0, $0002
            this.ee.t7.UD0 = System.Convert.ToByte((this.ee.t0.SD0 < 2));
            // @001286FC   BEQ t7, zero, $001286ac
            if ((this.ee.t7.UD0 == this.ee.r0.UD0)) {
                this.ee.pc = 1214124u;
            }
            // @00128700   ADDU t7, t4, a3
            this.ee.t7.SD0 = ((int)((this.ee.t4.SD0 + this.ee.a3.SD0)));
        }
        
        public virtual void funct001286f4() {
            this.ee.pc = 1214212u;
            // @001286F4   SUBU t0, a3, t4
            this.ee.t0.SD0 = ((int)((this.ee.a3.UD0 - this.ee.t4.UD0)));
            // @001286F8   SLTI t7, t0, $0002
            this.ee.t7.UD0 = System.Convert.ToByte((this.ee.t0.SD0 < 2));
            // @001286FC   BEQ t7, zero, $001286ac
            if ((this.ee.t7.UD0 == this.ee.r0.UD0)) {
                this.ee.pc = 1214124u;
            }
            // @00128700   ADDU t7, t4, a3
            this.ee.t7.SD0 = ((int)((this.ee.t4.SD0 + this.ee.a3.SD0)));
        }
        
        public virtual void funct00128704() {
            this.ee.pc = 1214232u;
            // @00128704   LHU t4, $0000(s1)
            MobUt.LHU(this.ee, this.ee.t4, (0u + this.ee.s1.UL0));
            // @00128708   ANDI t7, t4, $0003
            this.ee.t7.UD0 = ((ushort)((this.ee.t4.US0 & 3)));
            // @0012870C   ANDI t1, t7, $ffff
            this.ee.t1.UD0 = ((ushort)((this.ee.t7.US0 & 65535)));
            // @00128710   BEQL t1, zero, $0012861c
            if ((this.ee.t1.UD0 == this.ee.r0.UD0)) {
                this.ee.pc = 1213980u;
                // @00128714   LHU t7, $0002(s1)
                MobUt.LHU(this.ee, this.ee.t7, (2u + this.ee.s1.UL0));
            }
        }
        
        public virtual void funct00128708() {
            this.ee.pc = 1214232u;
            // @00128708   ANDI t7, t4, $0003
            this.ee.t7.UD0 = ((ushort)((this.ee.t4.US0 & 3)));
            // @0012870C   ANDI t1, t7, $ffff
            this.ee.t1.UD0 = ((ushort)((this.ee.t7.US0 & 65535)));
            // @00128710   BEQL t1, zero, $0012861c
            if ((this.ee.t1.UD0 == this.ee.r0.UD0)) {
                this.ee.pc = 1213980u;
                // @00128714   LHU t7, $0002(s1)
                MobUt.LHU(this.ee, this.ee.t7, (2u + this.ee.s1.UL0));
            }
        }
        
        public virtual void funct00128718() {
            this.ee.pc = 1214336u;
            // @00128718   LHU t7, $0002(s4)
            MobUt.LHU(this.ee, this.ee.t7, (2u + this.ee.s4.UL0));
            // @0012871C   ANDI t4, t4, $ffff
            this.ee.t4.UD0 = ((ushort)((this.ee.t4.US0 & 65535)));
            // @00128720   LHU t3, $0006(s1)
            MobUt.LHU(this.ee, this.ee.t3, (6u + this.ee.s1.UL0));
            // @00128724   ANDI t4, t4, $fffc
            this.ee.t4.UD0 = ((ushort)((this.ee.t4.US0 & 65532)));
            // @00128728   LHU t2, $0004(s4)
            MobUt.LHU(this.ee, this.ee.t2, (4u + this.ee.s4.UL0));
            // @0012872C   SLL t7, t7, 2
            MobUt.SLL(this.ee.t7, this.ee.t7, 2);
            // @00128730   LHU t6, $0002(s1)
            MobUt.LHU(this.ee, this.ee.t6, (2u + this.ee.s1.UL0));
            // @00128734   ADDU t7, t7, s5
            this.ee.t7.SD0 = ((int)((this.ee.t7.SD0 + this.ee.s5.SD0)));
            // @00128738   LHU t5, $0000(s4)
            MobUt.LHU(this.ee, this.ee.t5, (0u + this.ee.s4.UL0));
            // @0012873C   SLL t3, t3, 2
            MobUt.SLL(this.ee.t3, this.ee.t3, 2);
            // @00128740   SLL t2, t2, 2
            MobUt.SLL(this.ee.t2, this.ee.t2, 2);
            // @00128744   LWC1 $f17, $0000(t7)
            MobUt.LWC1(this.ee, this.ee.fpr[17], (0u + this.ee.t7.UL0));
            // @00128748   SLL t6, t6, 2
            MobUt.SLL(this.ee.t6, this.ee.t6, 2);
            // @0012874C   ANDI t5, t5, $fffc
            this.ee.t5.UD0 = ((ushort)((this.ee.t5.US0 & 65532)));
            // @00128750   ADDU t2, t2, s6
            this.ee.t2.SD0 = ((int)((this.ee.t2.SD0 + this.ee.s6.SD0)));
            // @00128754   ADDU t5, t5, s3
            this.ee.t5.SD0 = ((int)((this.ee.t5.SD0 + this.ee.s3.SD0)));
            // @00128758   ADDU t4, t4, s3
            this.ee.t4.SD0 = ((int)((this.ee.t4.SD0 + this.ee.s3.SD0)));
            // @0012875C   LWC1 $f16, $0000(t5)
            MobUt.LWC1(this.ee, this.ee.fpr[16], (0u + this.ee.t5.UL0));
            // @00128760   ADDU t6, t6, s5
            this.ee.t6.SD0 = ((int)((this.ee.t6.SD0 + this.ee.s5.SD0)));
            // @00128764   LWC1 $f13, $0000(t4)
            MobUt.LWC1(this.ee, this.ee.fpr[13], (0u + this.ee.t4.UL0));
            // @00128768   ADDU t3, t3, s6
            this.ee.t3.SD0 = ((int)((this.ee.t3.SD0 + this.ee.s6.SD0)));
            // @0012876C   LWC1 $f14, $0000(t6)
            MobUt.LWC1(this.ee, this.ee.fpr[14], (0u + this.ee.t6.UL0));
            // @00128770   LWC1 $f15, $0000(t3)
            MobUt.LWC1(this.ee, this.ee.fpr[15], (0u + this.ee.t3.UL0));
            // @00128774   ADDIU t7, zero, $0001
            this.ee.t7.SD0 = ((int)((this.ee.r0.SD0 + 1)));
            // @00128778   BEQ t1, t7, $0012879c
            if ((this.ee.t1.UD0 == this.ee.t7.UD0)) {
                this.ee.pc = 1214364u;
            }
            // @0012877C   LWC1 $f18, $0000(t2)
            MobUt.LWC1(this.ee, this.ee.fpr[18], (0u + this.ee.t2.UL0));
        }
        
        public virtual void funct00128780() {
            this.ee.pc = 1214348u;
            // @00128780   ADDIU t7, zero, $0002
            this.ee.t7.SD0 = ((int)((this.ee.r0.SD0 + 2)));
            // @00128784   BNEL t1, t7, $001285f0
            if ((this.ee.t1.UD0 != this.ee.t7.UD0)) {
                this.ee.pc = 1213936u;
                // @00128788   MTC1 zero, $f0
                this.ee.fpr[0].f = MobUt.UL2F(this.ee.r0.UL[0]);
            }
        }
        
        public virtual void funct0012878c() {
            this.ee.pc = 1214356u;
            // @0012878C   JAL $001284b0
            this.ee.ra.UL0 = 1214356u;
            this.ee.pc = 1213616u;
            // @00128790   MOV.S $f12, $f20
            this.ee.fpr[12].f = this.ee.fpr[20].f;
        }
        
        public virtual void funct00128794() {
            this.ee.pc = 1214364u;
            // @00128794   BEQ zero, zero, $001285f4
            if ((this.ee.r0.UD0 == this.ee.r0.UD0)) {
                this.ee.pc = 1213940u;
            }
            // @00128798   LD s0, $0010(sp)
            MobUt.LD(this.ee, this.ee.s0, (16u + this.ee.sp.UL0));
        }
        
        public virtual void funct0012879c() {
            this.ee.pc = 1214400u;
            // @0012879C   SUB.S $f0, $f17, $f14
            this.ee.fpr[0].f = (this.ee.fpr[17].f - this.ee.fpr[14].f);
            // @001287A0   SUB.S $f1, $f20, $f13
            this.ee.fpr[1].f = (this.ee.fpr[20].f - this.ee.fpr[13].f);
            // @001287A4   SUB.S $f2, $f16, $f13
            this.ee.fpr[2].f = (this.ee.fpr[16].f - this.ee.fpr[13].f);
            // @001287A8   MUL.S $f0, $f0, $f1
            this.ee.fpr[0].f = (this.ee.fpr[0].f * this.ee.fpr[1].f);
            // @001287AC   NOP 
            MobUt.Latency();
            // @001287B0   NOP 
            MobUt.Latency();
            // @001287B4   DIV.S $f0, $f0, $f2
            this.ee.fpr[0].f = (this.ee.fpr[0].f / this.ee.fpr[2].f);
            // @001287B8   BEQ zero, zero, $001285f0
            if ((this.ee.r0.UD0 == this.ee.r0.UD0)) {
                this.ee.pc = 1213936u;
            }
            // @001287BC   ADD.S $f0, $f14, $f0
            this.ee.fpr[0].f = (this.ee.fpr[14].f + this.ee.fpr[0].f);
        }
        
        public virtual void funct001287c0() {
            this.ee.pc = 1214412u;
            // @001287C0   SLT t7, t2, t5
            this.ee.t7.UD0 = System.Convert.ToByte((this.ee.t2.SD0 < this.ee.t5.SD0));
            // @001287C4   BEQ t7, zero, $001287d8
            if ((this.ee.t7.UD0 == this.ee.r0.UD0)) {
                this.ee.pc = 1214424u;
            }
            // @001287C8   NOP 
            MobUt.Latency();
        }
        
        public virtual void funct001287cc() {
            this.ee.pc = 1214424u;
            // @001287CC   DADDU a3, t3, zero
            this.ee.a3.UD0 = (this.ee.t3.UD0 + this.ee.r0.UD0);
            // @001287D0   BEQ zero, zero, $001286f4
            if ((this.ee.r0.UD0 == this.ee.r0.UD0)) {
                this.ee.pc = 1214196u;
            }
            // @001287D4   DADDU s4, t1, zero
            this.ee.s4.UD0 = (this.ee.t1.UD0 + this.ee.r0.UD0);
        }
        
        public virtual void funct001287d8() {
            this.ee.pc = 1214432u;
            // @001287D8   BEQL t5, t6, $001287f4
            if ((this.ee.t5.UD0 == this.ee.t6.UD0)) {
                this.ee.pc = 1214452u;
                // @001287DC   DADDU s1, t1, zero
                this.ee.s1.UD0 = (this.ee.t1.UD0 + this.ee.r0.UD0);
            }
        }
        
        public virtual void funct001287e0() {
            this.ee.pc = 1214440u;
            // @001287E0   BNE t5, t2, $001286fc
            if ((this.ee.t5.UD0 != this.ee.t2.UD0)) {
                this.ee.pc = 1214204u;
            }
            // @001287E4   SLTI t7, t0, $0002
            this.ee.t7.UD0 = System.Convert.ToByte((this.ee.t0.SD0 < 2));
        }
        
        public virtual void funct001287e8() {
            this.ee.pc = 1214452u;
            // @001287E8   DADDU s4, t1, zero
            this.ee.s4.UD0 = (this.ee.t1.UD0 + this.ee.r0.UD0);
            // @001287EC   BEQ zero, zero, $00128704
            if ((this.ee.r0.UD0 == this.ee.r0.UD0)) {
                this.ee.pc = 1214212u;
            }
            // @001287F0   ADDIU s1, t1, $fff8
            this.ee.s1.SD0 = ((int)((this.ee.t1.SD0 + -8)));
        }
        
        public virtual void funct001287f4() {
            this.ee.pc = 1214460u;
            // @001287F4   BEQ zero, zero, $00128704
            if ((this.ee.r0.UD0 == this.ee.r0.UD0)) {
                this.ee.pc = 1214212u;
            }
            // @001287F8   ADDIU s4, t1, $0008
            this.ee.s4.SD0 = ((int)((this.ee.t1.SD0 + 8)));
        }
        
        public virtual void funct001287fc() {
            this.ee.pc = 1214468u;
            // @001287FC   BEQ zero, zero, $0012861c
            if ((this.ee.r0.UD0 == this.ee.r0.UD0)) {
                this.ee.pc = 1213980u;
            }
            // @00128800   LHU t7, $0002(t1)
            MobUt.LHU(this.ee, this.ee.t7, (2u + this.ee.t1.UL0));
        }
        
        public virtual void funct00128804() {
            this.ee.pc = 1214528u;
            // @00128804   SLL t7, t2, 2
            MobUt.SLL(this.ee.t7, this.ee.t2, 2);
            // @00128808   LHU t6, $0004(s1)
            MobUt.LHU(this.ee, this.ee.t6, (4u + this.ee.s1.UL0));
            // @0012880C   ADDU t7, t7, s3
            this.ee.t7.SD0 = ((int)((this.ee.t7.SD0 + this.ee.s3.SD0)));
            // @00128810   LHU t5, $0002(s1)
            MobUt.LHU(this.ee, this.ee.t5, (2u + this.ee.s1.UL0));
            // @00128814   LWC1 $f0, $0000(t7)
            MobUt.LWC1(this.ee, this.ee.fpr[0], (0u + this.ee.t7.UL0));
            // @00128818   SLL t6, t6, 2
            MobUt.SLL(this.ee.t6, this.ee.t6, 2);
            // @0012881C   ADDU t6, t6, s6
            this.ee.t6.SD0 = ((int)((this.ee.t6.SD0 + this.ee.s6.SD0)));
            // @00128820   SLL t5, t5, 2
            MobUt.SLL(this.ee.t5, this.ee.t5, 2);
            // @00128824   SUB.S $f0, $f0, $f12
            this.ee.fpr[0].f = (this.ee.fpr[0].f - this.ee.fpr[12].f);
            // @00128828   ADDU t5, t5, s5
            this.ee.t5.SD0 = ((int)((this.ee.t5.SD0 + this.ee.s5.SD0)));
            // @0012882C   LWC1 $f2, $0000(t6)
            MobUt.LWC1(this.ee, this.ee.fpr[2], (0u + this.ee.t6.UL0));
            // @00128830   LWC1 $f1, $0000(t5)
            MobUt.LWC1(this.ee, this.ee.fpr[1], (0u + this.ee.t5.UL0));
            // @00128834   MUL.S $f0, $f0, $f2
            this.ee.fpr[0].f = (this.ee.fpr[0].f * this.ee.fpr[2].f);
            // @00128838   BEQ zero, zero, $001285f0
            if ((this.ee.r0.UD0 == this.ee.r0.UD0)) {
                this.ee.pc = 1213936u;
            }
            // @0012883C   SUB.S $f0, $f1, $f0
            this.ee.fpr[0].f = (this.ee.fpr[1].f - this.ee.fpr[0].f);
        }
        
        public virtual void funct00128840() {
            this.ee.pc = 1214548u;
            // @00128840   LHU t7, $fff8(t4)
            MobUt.LHU(this.ee, this.ee.t7, (4294967288u + this.ee.t4.UL0));
            // @00128844   SRL t4, t7, 2
            MobUt.SRL(this.ee.t4, this.ee.t7, 2);
            // @00128848   SLT a2, a2, t4
            this.ee.a2.UD0 = System.Convert.ToByte((this.ee.a2.SD0 < this.ee.t4.SD0));
            // @0012884C   BNEL a2, zero, $0012868c
            if ((this.ee.a2.UD0 != this.ee.r0.UD0)) {
                this.ee.pc = 1214092u;
                // @00128850   ANDI t4, t0, $ffff
                this.ee.t4.UD0 = ((ushort)((this.ee.t0.US0 & 65535)));
            }
        }
        
        public virtual void funct00128854() {
            this.ee.pc = 1214572u;
            // @00128854   LHU t7, $0002(a1)
            MobUt.LHU(this.ee, this.ee.t7, (2u + this.ee.a1.UL0));
            // @00128858   ADDIU t6, zero, $0001
            this.ee.t6.SD0 = ((int)((this.ee.r0.SD0 + 1)));
            // @0012885C   SRL t7, t7, 6
            MobUt.SRL(this.ee.t7, this.ee.t7, 6);
            // @00128860   ANDI t5, t7, $0003
            this.ee.t5.UD0 = ((ushort)((this.ee.t7.US0 & 3)));
            // @00128864   BEQ t5, t6, $001288d8
            if ((this.ee.t5.UD0 == this.ee.t6.UD0)) {
                this.ee.pc = 1214680u;
            }
            // @00128868   SLTI t7, t5, $0002
            this.ee.t7.UD0 = System.Convert.ToByte((this.ee.t5.SD0 < 2));
        }
        
        public virtual void funct0012886c() {
            this.ee.pc = 1214580u;
            // @0012886C   BEQ t7, zero, $00128884
            if ((this.ee.t7.UD0 == this.ee.r0.UD0)) {
                this.ee.pc = 1214596u;
            }
            // @00128870   ADDIU t7, zero, $0002
            this.ee.t7.SD0 = ((int)((this.ee.r0.SD0 + 2)));
        }
        
        public virtual void funct00128874() {
            this.ee.pc = 1214588u;
            // @00128874   BNEL t5, zero, $001285f0
            if ((this.ee.t5.UD0 != this.ee.r0.UD0)) {
                this.ee.pc = 1213936u;
                // @00128878   MTC1 zero, $f0
                this.ee.fpr[0].f = MobUt.UL2F(this.ee.r0.UL[0]);
            }
        }
        
        public virtual void funct0012887c() {
            this.ee.pc = 1214596u;
            // @0012887C   BEQ zero, zero, $0012861c
            if ((this.ee.r0.UD0 == this.ee.r0.UD0)) {
                this.ee.pc = 1213980u;
            }
            // @00128880   LHU t7, $0002(s4)
            MobUt.LHU(this.ee, this.ee.t7, (2u + this.ee.s4.UL0));
        }
        
        public virtual void funct00128884() {
            this.ee.pc = 1214604u;
            // @00128884   BNEL t5, t7, $001285f0
            if ((this.ee.t5.UD0 != this.ee.t7.UD0)) {
                this.ee.pc = 1213936u;
                // @00128888   MTC1 zero, $f0
                this.ee.fpr[0].f = MobUt.UL2F(this.ee.r0.UL[0]);
            }
        }
        
        public virtual void funct0012888c() {
            this.ee.pc = 1214640u;
            // @0012888C   SLL t7, t2, 2
            MobUt.SLL(this.ee.t7, this.ee.t2, 2);
            // @00128890   SLL t6, t4, 2
            MobUt.SLL(this.ee.t6, this.ee.t4, 2);
            // @00128894   ADDU t7, t7, s3
            this.ee.t7.SD0 = ((int)((this.ee.t7.SD0 + this.ee.s3.SD0)));
            // @00128898   ADDU t6, t6, s3
            this.ee.t6.SD0 = ((int)((this.ee.t6.SD0 + this.ee.s3.SD0)));
            // @0012889C   LWC1 $f2, $0000(t6)
            MobUt.LWC1(this.ee, this.ee.fpr[2], (0u + this.ee.t6.UL0));
            // @001288A0   LWC1 $f0, $0000(t7)
            MobUt.LWC1(this.ee, this.ee.fpr[0], (0u + this.ee.t7.UL0));
            // @001288A4   C.LT.S $f2, $f12
            this.ee.fcr31_23 = (this.ee.fpr[2].f < this.ee.fpr[12].f);
            // @001288A8   BC1F $00128670
            if ((this.ee.fcr31_23 == false)) {
                this.ee.pc = 1214064u;
            }
            // @001288AC   SUB.S $f0, $f2, $f0
            this.ee.fpr[0].f = (this.ee.fpr[2].f - this.ee.fpr[0].f);
        }
        
        public virtual void funct001288b0() {
            this.ee.pc = 1214672u;
            // @001288B0   SUB.S $f12, $f12, $f0
            this.ee.fpr[12].f = (this.ee.fpr[12].f - this.ee.fpr[0].f);
            // @001288B4   C.LT.S $f2, $f12
            this.ee.fcr31_23 = (this.ee.fpr[2].f < this.ee.fpr[12].f);
            // @001288B8   NOP 
            MobUt.Latency();
            // @001288BC   NOP 
            MobUt.Latency();
            // @001288C0   NOP 
            MobUt.Latency();
            // @001288C4   NOP 
            MobUt.Latency();
            // @001288C8   BC1TL $001288b4
            if ((this.ee.fcr31_23 == true)) {
                this.ee.pc = 1214644u;
                // @001288CC   SUB.S $f12, $f12, $f0
                this.ee.fpr[12].f = (this.ee.fpr[12].f - this.ee.fpr[0].f);
            }
        }
        
        public virtual void funct001288b4() {
            this.ee.pc = 1214672u;
            // @001288B4   C.LT.S $f2, $f12
            this.ee.fcr31_23 = (this.ee.fpr[2].f < this.ee.fpr[12].f);
            // @001288B8   NOP 
            MobUt.Latency();
            // @001288BC   NOP 
            MobUt.Latency();
            // @001288C0   NOP 
            MobUt.Latency();
            // @001288C4   NOP 
            MobUt.Latency();
            // @001288C8   BC1TL $001288b4
            if ((this.ee.fcr31_23 == true)) {
                this.ee.pc = 1214644u;
                // @001288CC   SUB.S $f12, $f12, $f0
                this.ee.fpr[12].f = (this.ee.fpr[12].f - this.ee.fpr[0].f);
            }
        }
        
        public virtual void funct001288d0() {
            this.ee.pc = 1214680u;
            // @001288D0   BEQ zero, zero, $00128670
            if ((this.ee.r0.UD0 == this.ee.r0.UD0)) {
                this.ee.pc = 1214064u;
            }
            // @001288D4   MOV.S $f20, $f12
            this.ee.fpr[20].f = this.ee.fpr[12].f;
        }
        
        public virtual void funct00128918() {
            this.ee.pc = 1214808u;
            // @00128918   ADDIU sp, sp, $ffb0
            this.ee.sp.SD0 = ((int)((this.ee.sp.SD0 + -80)));
            // @0012891C   SWC1 $f20, $0048(sp)
            MobUt.SWC1(this.ee, this.ee.fpr[20], (72u + this.ee.sp.UL0));
            // @00128920   ADDIU a2, sp, $0004
            this.ee.a2.SD0 = ((int)((this.ee.sp.SD0 + 4)));
            // @00128924   SD s5, $0038(sp)
            MobUt.SD(this.ee, this.ee.s5, (56u + this.ee.sp.UL0));
            // @00128928   MOV.S $f20, $f12
            this.ee.fpr[20].f = this.ee.fpr[12].f;
            // @0012892C   SD s4, $0030(sp)
            MobUt.SD(this.ee, this.ee.s4, (48u + this.ee.sp.UL0));
            // @00128930   SD s0, $0010(sp)
            MobUt.SD(this.ee, this.ee.s0, (16u + this.ee.sp.UL0));
            // @00128934   DADDU s5, a1, zero
            this.ee.s5.UD0 = (this.ee.a1.UD0 + this.ee.r0.UD0);
            // @00128938   SD s1, $0018(sp)
            MobUt.SD(this.ee, this.ee.s1, (24u + this.ee.sp.UL0));
            // @0012893C   DADDU s4, a0, zero
            this.ee.s4.UD0 = (this.ee.a0.UD0 + this.ee.r0.UD0);
            // @00128940   SD s2, $0020(sp)
            MobUt.SD(this.ee, this.ee.s2, (32u + this.ee.sp.UL0));
            // @00128944   DADDU a1, sp, zero
            this.ee.a1.UD0 = (this.ee.sp.UD0 + this.ee.r0.UD0);
            // @00128948   SD s3, $0028(sp)
            MobUt.SD(this.ee, this.ee.s3, (40u + this.ee.sp.UL0));
            // @0012894C   SD ra, $0040(sp)
            MobUt.SD(this.ee, this.ee.ra, (64u + this.ee.sp.UL0));
            // @00128950   JAL $001283d8
            this.ee.ra.UL0 = 1214808u;
            this.ee.pc = 1213400u;
            // @00128954   DADDU s2, zero, zero
            this.ee.s2.UD0 = (this.ee.r0.UD0 + this.ee.r0.UD0);
        }
        
        public virtual void funct00128958() {
            this.ee.pc = 1214828u;
            // @00128958   LW s1, $0004(s4)
            MobUt.LW(this.ee, this.ee.s1, (4u + this.ee.s4.UL0));
            // @0012895C   LW t7, $0030(s1)
            MobUt.LW(this.ee, this.ee.t7, (48u + this.ee.s1.UL0));
            // @00128960   LW s3, $0034(s1)
            MobUt.LW(this.ee, this.ee.s3, (52u + this.ee.s1.UL0));
            // @00128964   BLEZ s3, $001289e4
            if ((this.ee.s3.SD0 <= 0)) {
                this.ee.pc = 1214948u;
            }
            // @00128968   ADDU s0, s1, t7
            this.ee.s0.SD0 = ((int)((this.ee.s1.SD0 + this.ee.t7.SD0)));
        }
        
        public virtual void funct0012896c() {
            this.ee.pc = 1214852u;
            // @0012896C   MOV.S $f12, $f20
            this.ee.fpr[12].f = this.ee.fpr[20].f;
            // @00128970   LW a2, $0000(sp)
            MobUt.LW(this.ee, this.ee.a2, (0u + this.ee.sp.UL0));
            // @00128974   LW a3, $0004(sp)
            MobUt.LW(this.ee, this.ee.a3, (4u + this.ee.sp.UL0));
            // @00128978   DADDU a0, s4, zero
            this.ee.a0.UD0 = (this.ee.s4.UD0 + this.ee.r0.UD0);
            // @0012897C   JAL $00128530
            this.ee.ra.UL0 = 1214852u;
            this.ee.pc = 1213744u;
            // @00128980   DADDU a1, s0, zero
            this.ee.a1.UD0 = (this.ee.s0.UD0 + this.ee.r0.UD0);
        }
        
        public virtual void funct00128984() {
            this.ee.pc = 1214876u;
            // @00128984   LBU t4, $0002(s0)
            MobUt.LBU(this.ee, this.ee.t4, (2u + this.ee.s0.UL0));
            // @00128988   ANDI t7, t4, $000f
            this.ee.t7.UD0 = ((ushort)((this.ee.t4.US0 & 15)));
            // @0012898C   ANDI t7, t7, $00ff
            this.ee.t7.UD0 = ((ushort)((this.ee.t7.US0 & 255)));
            // @00128990   SLTIU t6, t7, $0009
            this.ee.t6.UD0 = System.Convert.ToByte((this.ee.t7.UD0 < 9ul));
            // @00128994   BEQ t6, zero, $001289d4
            if ((this.ee.t6.UD0 == this.ee.r0.UD0)) {
                this.ee.pc = 1214932u;
            }
            // @00128998   LUI t6, $0038
            this.ee.t6.SD0 = 3670016;
        }
        
        public virtual void funct0012899c() {
            this.ee.pc = 1214900u;
            // @0012899C   SLL t7, t7, 2
            MobUt.SLL(this.ee.t7, this.ee.t7, 2);
            // @001289A0   ADDIU t6, t6, $8244
            this.ee.t6.SD0 = ((int)((this.ee.t6.SD0 + -32188)));
            // @001289A4   ADDU t7, t7, t6
            this.ee.t7.SD0 = ((int)((this.ee.t7.SD0 + this.ee.t6.SD0)));
            // @001289A8   LW t5, $0000(t7)
            MobUt.LW(this.ee, this.ee.t5, (0u + this.ee.t7.UL0));
            // @001289AC   JR t5
            this.ee.pc = this.ee.t5.UL[0];
            // @001289B0   NOP 
            MobUt.Latency();
        }
        
        public virtual void funct001289b4() {
            this.ee.pc = 1214948u;
            // @001289B4   LHU t7, $0000(s0)
            MobUt.LHU(this.ee, this.ee.t7, (0u + this.ee.s0.UL0));
            // @001289B8   ANDI t5, t4, $000f
            this.ee.t5.UD0 = ((ushort)((this.ee.t4.US0 & 15)));
            // @001289BC   LW t6, $001c(s5)
            MobUt.LW(this.ee, this.ee.t6, (28u + this.ee.s5.UL0));
            // @001289C0   SLL t5, t5, 2
            MobUt.SLL(this.ee.t5, this.ee.t5, 2);
            // @001289C4   SLL t7, t7, 4
            MobUt.SLL(this.ee.t7, this.ee.t7, 4);
            // @001289C8   ADDU t6, t6, t7
            this.ee.t6.SD0 = ((int)((this.ee.t6.SD0 + this.ee.t7.SD0)));
            // @001289CC   ADDU t6, t6, t5
            this.ee.t6.SD0 = ((int)((this.ee.t6.SD0 + this.ee.t5.SD0)));
            // @001289D0   SWC1 $f0, $0000(t6)
            MobUt.SWC1(this.ee, this.ee.fpr[0], (0u + this.ee.t6.UL0));
            // @001289D4   ADDIU s2, s2, $0001
            this.ee.s2.SD0 = ((int)((this.ee.s2.SD0 + 1)));
            // @001289D8   SLT t7, s2, s3
            this.ee.t7.UD0 = System.Convert.ToByte((this.ee.s2.SD0 < this.ee.s3.SD0));
            // @001289DC   BNE t7, zero, $0012896c
            if ((this.ee.t7.UD0 != this.ee.r0.UD0)) {
                this.ee.pc = 1214828u;
            }
            // @001289E0   ADDIU s0, s0, $0006
            this.ee.s0.SD0 = ((int)((this.ee.s0.SD0 + 6)));
        }
        
        public virtual void funct001289d4() {
            this.ee.pc = 1214948u;
            // @001289D4   ADDIU s2, s2, $0001
            this.ee.s2.SD0 = ((int)((this.ee.s2.SD0 + 1)));
            // @001289D8   SLT t7, s2, s3
            this.ee.t7.UD0 = System.Convert.ToByte((this.ee.s2.SD0 < this.ee.s3.SD0));
            // @001289DC   BNE t7, zero, $0012896c
            if ((this.ee.t7.UD0 != this.ee.r0.UD0)) {
                this.ee.pc = 1214828u;
            }
            // @001289E0   ADDIU s0, s0, $0006
            this.ee.s0.SD0 = ((int)((this.ee.s0.SD0 + 6)));
        }
        
        public virtual void funct001289e4() {
            this.ee.pc = 1214976u;
            // @001289E4   LW t6, $0018(s1)
            MobUt.LW(this.ee, this.ee.t6, (24u + this.ee.s1.UL0));
            // @001289E8   DADDU s2, zero, zero
            this.ee.s2.UD0 = (this.ee.r0.UD0 + this.ee.r0.UD0);
            // @001289EC   LW t7, $0038(s1)
            MobUt.LW(this.ee, this.ee.t7, (56u + this.ee.s1.UL0));
            // @001289F0   LW s3, $003c(s1)
            MobUt.LW(this.ee, this.ee.s3, (60u + this.ee.s1.UL0));
            // @001289F4   ADDU s5, s1, t6
            this.ee.s5.SD0 = ((int)((this.ee.s1.SD0 + this.ee.t6.SD0)));
            // @001289F8   BLEZ s3, $00128a74
            if ((this.ee.s3.SD0 <= 0)) {
                this.ee.pc = 1215092u;
            }
            // @001289FC   ADDU s0, s1, t7
            this.ee.s0.SD0 = ((int)((this.ee.s1.SD0 + this.ee.t7.SD0)));
        }
        
        public virtual void funct00128a00() {
            this.ee.pc = 1215000u;
            // @00128A00   MOV.S $f12, $f20
            this.ee.fpr[12].f = this.ee.fpr[20].f;
            // @00128A04   LW a2, $0000(sp)
            MobUt.LW(this.ee, this.ee.a2, (0u + this.ee.sp.UL0));
            // @00128A08   LW a3, $0004(sp)
            MobUt.LW(this.ee, this.ee.a3, (4u + this.ee.sp.UL0));
            // @00128A0C   DADDU a0, s4, zero
            this.ee.a0.UD0 = (this.ee.s4.UD0 + this.ee.r0.UD0);
            // @00128A10   JAL $00128530
            this.ee.ra.UL0 = 1215000u;
            this.ee.pc = 1213744u;
            // @00128A14   DADDU a1, s0, zero
            this.ee.a1.UD0 = (this.ee.s0.UD0 + this.ee.r0.UD0);
        }
        
        public virtual void funct00128a18() {
            this.ee.pc = 1215024u;
            // @00128A18   LBU t4, $0002(s0)
            MobUt.LBU(this.ee, this.ee.t4, (2u + this.ee.s0.UL0));
            // @00128A1C   ANDI t7, t4, $000f
            this.ee.t7.UD0 = ((ushort)((this.ee.t4.US0 & 15)));
            // @00128A20   ANDI t7, t7, $00ff
            this.ee.t7.UD0 = ((ushort)((this.ee.t7.US0 & 255)));
            // @00128A24   SLTIU t6, t7, $0009
            this.ee.t6.UD0 = System.Convert.ToByte((this.ee.t7.UD0 < 9ul));
            // @00128A28   BEQ t6, zero, $00128a64
            if ((this.ee.t6.UD0 == this.ee.r0.UD0)) {
                this.ee.pc = 1215076u;
            }
            // @00128A2C   LUI t6, $0038
            this.ee.t6.SD0 = 3670016;
        }
        
        public virtual void funct00128a30() {
            this.ee.pc = 1215048u;
            // @00128A30   SLL t7, t7, 2
            MobUt.SLL(this.ee.t7, this.ee.t7, 2);
            // @00128A34   ADDIU t6, t6, $8268
            this.ee.t6.SD0 = ((int)((this.ee.t6.SD0 + -32152)));
            // @00128A38   ADDU t7, t7, t6
            this.ee.t7.SD0 = ((int)((this.ee.t7.SD0 + this.ee.t6.SD0)));
            // @00128A3C   LW t5, $0000(t7)
            MobUt.LW(this.ee, this.ee.t5, (0u + this.ee.t7.UL0));
            // @00128A40   JR t5
            this.ee.pc = this.ee.t5.UL[0];
            // @00128A44   NOP 
            MobUt.Latency();
        }
        
        public virtual void funct00128a64() {
            this.ee.pc = 1215092u;
            // @00128A64   ADDIU s2, s2, $0001
            this.ee.s2.SD0 = ((int)((this.ee.s2.SD0 + 1)));
            // @00128A68   SLT t7, s2, s3
            this.ee.t7.UD0 = System.Convert.ToByte((this.ee.s2.SD0 < this.ee.s3.SD0));
            // @00128A6C   BNE t7, zero, $00128a00
            if ((this.ee.t7.UD0 != this.ee.r0.UD0)) {
                this.ee.pc = 1214976u;
            }
            // @00128A70   ADDIU s0, s0, $0006
            this.ee.s0.SD0 = ((int)((this.ee.s0.SD0 + 6)));
        }
        
        public virtual void funct00128a74() {
            this.ee.pc = 1215132u;
            // @00128A74   LD s0, $0010(sp)
            MobUt.LD(this.ee, this.ee.s0, (16u + this.ee.sp.UL0));
            // @00128A78   LD s1, $0018(sp)
            MobUt.LD(this.ee, this.ee.s1, (24u + this.ee.sp.UL0));
            // @00128A7C   LD s2, $0020(sp)
            MobUt.LD(this.ee, this.ee.s2, (32u + this.ee.sp.UL0));
            // @00128A80   LD s3, $0028(sp)
            MobUt.LD(this.ee, this.ee.s3, (40u + this.ee.sp.UL0));
            // @00128A84   LD s4, $0030(sp)
            MobUt.LD(this.ee, this.ee.s4, (48u + this.ee.sp.UL0));
            // @00128A88   LD s5, $0038(sp)
            MobUt.LD(this.ee, this.ee.s5, (56u + this.ee.sp.UL0));
            // @00128A8C   LD ra, $0040(sp)
            MobUt.LD(this.ee, this.ee.ra, (64u + this.ee.sp.UL0));
            // @00128A90   LWC1 $f20, $0048(sp)
            MobUt.LWC1(this.ee, this.ee.fpr[20], (72u + this.ee.sp.UL0));
            // @00128A94   JR ra
            this.ee.pc = this.ee.ra.UL[0];
            // @00128A98   ADDIU sp, sp, $0050
            this.ee.sp.SD0 = ((int)((this.ee.sp.SD0 + 80)));
        }
        
        public virtual void funct00128a9c() {
            this.ee.pc = 1215168u;
            // @00128A9C   LHU t7, $0000(s0)
            MobUt.LHU(this.ee, this.ee.t7, (0u + this.ee.s0.UL0));
            // @00128AA0   ANDI t6, t4, $000f
            this.ee.t6.UD0 = ((ushort)((this.ee.t4.US0 & 15)));
            // @00128AA4   ADDIU t6, t6, $fffd
            this.ee.t6.SD0 = ((int)((this.ee.t6.SD0 + -3)));
            // @00128AA8   SLL t7, t7, 6
            MobUt.SLL(this.ee.t7, this.ee.t7, 6);
            // @00128AAC   SLL t6, t6, 2
            MobUt.SLL(this.ee.t6, this.ee.t6, 2);
            // @00128AB0   ADDU t7, s5, t7
            this.ee.t7.SD0 = ((int)((this.ee.s5.SD0 + this.ee.t7.SD0)));
            // @00128AB4   ADDU t7, t7, t6
            this.ee.t7.SD0 = ((int)((this.ee.t7.SD0 + this.ee.t6.SD0)));
            // @00128AB8   BEQ zero, zero, $00128a64
            if ((this.ee.r0.UD0 == this.ee.r0.UD0)) {
                this.ee.pc = 1215076u;
            }
            // @00128ABC   SWC1 $f0, $0020(t7)
            MobUt.SWC1(this.ee, this.ee.fpr[0], (32u + this.ee.t7.UL0));
        }
        
        public virtual void funct00128ac0() {
            this.ee.pc = 1215204u;
            // @00128AC0   LHU t7, $0000(s0)
            MobUt.LHU(this.ee, this.ee.t7, (0u + this.ee.s0.UL0));
            // @00128AC4   ANDI t6, t4, $000f
            this.ee.t6.UD0 = ((ushort)((this.ee.t4.US0 & 15)));
            // @00128AC8   ADDIU t6, t6, $fffa
            this.ee.t6.SD0 = ((int)((this.ee.t6.SD0 + -6)));
            // @00128ACC   SLL t7, t7, 6
            MobUt.SLL(this.ee.t7, this.ee.t7, 6);
            // @00128AD0   SLL t6, t6, 2
            MobUt.SLL(this.ee.t6, this.ee.t6, 2);
            // @00128AD4   ADDU t7, s5, t7
            this.ee.t7.SD0 = ((int)((this.ee.s5.SD0 + this.ee.t7.SD0)));
            // @00128AD8   ADDU t7, t7, t6
            this.ee.t7.SD0 = ((int)((this.ee.t7.SD0 + this.ee.t6.SD0)));
            // @00128ADC   BEQ zero, zero, $00128a64
            if ((this.ee.r0.UD0 == this.ee.r0.UD0)) {
                this.ee.pc = 1215076u;
            }
            // @00128AE0   SWC1 $f0, $0030(t7)
            MobUt.SWC1(this.ee, this.ee.fpr[0], (48u + this.ee.t7.UL0));
        }
        
        public virtual void funct00128ae4() {
            this.ee.pc = 1215244u;
            // @00128AE4   ANDI t6, t4, $000f
            this.ee.t6.UD0 = ((ushort)((this.ee.t4.US0 & 15)));
            // @00128AE8   LHU t7, $0000(s0)
            MobUt.LHU(this.ee, this.ee.t7, (0u + this.ee.s0.UL0));
            // @00128AEC   LW t5, $0020(s5)
            MobUt.LW(this.ee, this.ee.t5, (32u + this.ee.s5.UL0));
            // @00128AF0   ADDIU t6, t6, $fffd
            this.ee.t6.SD0 = ((int)((this.ee.t6.SD0 + -3)));
            // @00128AF4   SLL t7, t7, 4
            MobUt.SLL(this.ee.t7, this.ee.t7, 4);
            // @00128AF8   SLL t6, t6, 2
            MobUt.SLL(this.ee.t6, this.ee.t6, 2);
            // @00128AFC   ADDU t5, t5, t7
            this.ee.t5.SD0 = ((int)((this.ee.t5.SD0 + this.ee.t7.SD0)));
            // @00128B00   ADDU t5, t5, t6
            this.ee.t5.SD0 = ((int)((this.ee.t5.SD0 + this.ee.t6.SD0)));
            // @00128B04   BEQ zero, zero, $001289d4
            if ((this.ee.r0.UD0 == this.ee.r0.UD0)) {
                this.ee.pc = 1214932u;
            }
            // @00128B08   SWC1 $f0, $0000(t5)
            MobUt.SWC1(this.ee, this.ee.fpr[0], (0u + this.ee.t5.UL0));
        }
        
        public virtual void funct00128af4() {
            this.ee.pc = 1215244u;
            // @00128AF4   SLL t7, t7, 4
            MobUt.SLL(this.ee.t7, this.ee.t7, 4);
            // @00128AF8   SLL t6, t6, 2
            MobUt.SLL(this.ee.t6, this.ee.t6, 2);
            // @00128AFC   ADDU t5, t5, t7
            this.ee.t5.SD0 = ((int)((this.ee.t5.SD0 + this.ee.t7.SD0)));
            // @00128B00   ADDU t5, t5, t6
            this.ee.t5.SD0 = ((int)((this.ee.t5.SD0 + this.ee.t6.SD0)));
            // @00128B04   BEQ zero, zero, $001289d4
            if ((this.ee.r0.UD0 == this.ee.r0.UD0)) {
                this.ee.pc = 1214932u;
            }
            // @00128B08   SWC1 $f0, $0000(t5)
            MobUt.SWC1(this.ee, this.ee.fpr[0], (0u + this.ee.t5.UL0));
        }
        
        public virtual void funct00128b0c() {
            this.ee.pc = 1215264u;
            // @00128B0C   ANDI t6, t4, $000f
            this.ee.t6.UD0 = ((ushort)((this.ee.t4.US0 & 15)));
            // @00128B10   LHU t7, $0000(s0)
            MobUt.LHU(this.ee, this.ee.t7, (0u + this.ee.s0.UL0));
            // @00128B14   LW t5, $0024(s5)
            MobUt.LW(this.ee, this.ee.t5, (36u + this.ee.s5.UL0));
            // @00128B18   BEQ zero, zero, $00128af4
            if ((this.ee.r0.UD0 == this.ee.r0.UD0)) {
                this.ee.pc = 1215220u;
            }
            // @00128B1C   ADDIU t6, t6, $fffa
            this.ee.t6.SD0 = ((int)((this.ee.t6.SD0 + -6)));
        }
        
        private void initstate() {
            this.initregs();
            this.initfns();
        }
        
        private void initregs() {
            this.ee.r0.UD0 = 0ul;
            this.ee.r0.UD1 = 0ul;
            this.ee.at.UD0 = 1070141403ul;
            this.ee.at.UD1 = 0ul;
            this.ee.v0.UD0 = 229ul;
            this.ee.v0.UD1 = 0ul;
            this.ee.v1.UD0 = 3728656ul;
            this.ee.v1.UD1 = 11678ul;
            this.ee.a0.UD0 = 10454288ul;
            this.ee.a0.UD1 = 11633ul;
            this.ee.a1.UD0 = 28030352ul;
            this.ee.a1.UD1 = 0ul;
            this.ee.a2.UD0 = 28029104ul;
            this.ee.a2.UD1 = 0ul;
            this.ee.a3.UD0 = 0ul;
            this.ee.a3.UD1 = 0ul;
            this.ee.t0.UD0 = 3730800ul;
            this.ee.t0.UD1 = 0ul;
            this.ee.t1.UD0 = 4575657221408423936ul;
            this.ee.t1.UD1 = 0ul;
            this.ee.t2.UD0 = 0ul;
            this.ee.t2.UD1 = 1065353216ul;
            this.ee.t3.UD0 = 0ul;
            this.ee.t3.UD1 = 4575657221408423936ul;
            this.ee.t4.UD0 = 28108432ul;
            this.ee.t4.UD1 = 4575657221408423936ul;
            this.ee.t5.UD0 = 3638276ul;
            this.ee.t5.UD1 = 0ul;
            this.ee.t6.UD0 = 10454288ul;
            this.ee.t6.UD1 = 6660591616ul;
            this.ee.t7.UD0 = 3473408ul;
            this.ee.t7.UD1 = 78ul;
            this.ee.s0.UD0 = 10454432ul;
            this.ee.s0.UD1 = 0ul;
            this.ee.s1.UD0 = 10454288ul;
            this.ee.s1.UD1 = 0ul;
            this.ee.s2.UD0 = 28030352ul;
            this.ee.s2.UD1 = 0ul;
            this.ee.s3.UD0 = 3730800ul;
            this.ee.s3.UD1 = 0ul;
            this.ee.s4.UD0 = 28029104ul;
            this.ee.s4.UD1 = 0ul;
            this.ee.s5.UD0 = 0ul;
            this.ee.s5.UD1 = 0ul;
            this.ee.s6.UD0 = 3730800ul;
            this.ee.s6.UD1 = 0ul;
            this.ee.s7.UD0 = 0ul;
            this.ee.s7.UD1 = 0ul;
            this.ee.t8.UD0 = 3728624ul;
            this.ee.t8.UD1 = 0ul;
            this.ee.t9.UD0 = 3728640ul;
            this.ee.t9.UD1 = 0ul;
            this.ee.k0.UD0 = 1879247891ul;
            this.ee.k0.UD1 = 0ul;
            this.ee.k1.UD0 = 0ul;
            this.ee.k1.UD1 = 0ul;
            this.ee.gp.UD0 = 3704508ul;
            this.ee.gp.UD1 = 0ul;
            this.ee.sp.UD0 = 3730592ul;
            this.ee.sp.UD1 = 0ul;
            this.ee.s8.UD0 = 0ul;
            this.ee.s8.UD1 = 0ul;
            this.ee.ra.UD0 = 1248204ul;
            this.ee.ra.UD1 = 0ul;
            this.ee.fpr[0].f = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.fpr[1].f = ee1Dec.C.MobUt.UL2F(1082130432u);
            this.ee.fpr[2].f = ee1Dec.C.MobUt.UL2F(1082130432u);
            this.ee.fpr[3].f = ee1Dec.C.MobUt.UL2F(1114636288u);
            this.ee.fpr[4].f = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.fpr[5].f = ee1Dec.C.MobUt.UL2F(3212836864u);
            this.ee.fpr[6].f = ee1Dec.C.MobUt.UL2F(1073741824u);
            this.ee.fpr[7].f = ee1Dec.C.MobUt.UL2F(1124007936u);
            this.ee.fpr[8].f = ee1Dec.C.MobUt.UL2F(1147806946u);
            this.ee.fpr[9].f = ee1Dec.C.MobUt.UL2F(1124209790u);
            this.ee.fpr[10].f = ee1Dec.C.MobUt.UL2F(1108805435u);
            this.ee.fpr[11].f = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.fpr[12].f = ee1Dec.C.MobUt.UL2F(1090519040u);
            this.ee.fpr[13].f = ee1Dec.C.MobUt.UL2F(1086918619u);
            this.ee.fpr[14].f = ee1Dec.C.MobUt.UL2F(3308588622u);
            this.ee.fpr[15].f = ee1Dec.C.MobUt.UL2F(3298955796u);
            this.ee.fpr[16].f = ee1Dec.C.MobUt.UL2F(1218199392u);
            this.ee.fpr[17].f = ee1Dec.C.MobUt.UL2F(3308588622u);
            this.ee.fpr[18].f = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.fpr[19].f = ee1Dec.C.MobUt.UL2F(1065353216u);
            this.ee.fpr[20].f = ee1Dec.C.MobUt.UL2F(1090519040u);
            this.ee.fpr[21].f = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.fpr[22].f = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.fpr[23].f = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.fpr[24].f = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.fpr[25].f = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.fpr[26].f = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.fpr[27].f = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.fpr[28].f = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.fpr[29].f = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.fpr[30].f = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.fpr[31].f = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.fpracc.f = ee1Dec.C.MobUt.UL2F(2147483648u);
            this.ee.VF[0].x = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[0].y = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[0].z = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[0].w = ee1Dec.C.MobUt.UL2F(1065353216u);
            this.ee.VF[1].x = ee1Dec.C.MobUt.UL2F(1059810839u);
            this.ee.VF[1].y = ee1Dec.C.MobUt.UL2F(2147483648u);
            this.ee.VF[1].z = ee1Dec.C.MobUt.UL2F(3208519719u);
            this.ee.VF[1].w = ee1Dec.C.MobUt.UL2F(2147483648u);
            this.ee.VF[2].x = ee1Dec.C.MobUt.UL2F(3212836864u);
            this.ee.VF[2].y = ee1Dec.C.MobUt.UL2F(4294967295u);
            this.ee.VF[2].z = ee1Dec.C.MobUt.UL2F(1059810839u);
            this.ee.VF[2].w = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[3].x = ee1Dec.C.MobUt.UL2F(3207294487u);
            this.ee.VF[3].y = ee1Dec.C.MobUt.UL2F(2147483648u);
            this.ee.VF[3].z = ee1Dec.C.MobUt.UL2F(1061036071u);
            this.ee.VF[3].w = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[4].x = ee1Dec.C.MobUt.UL2F(3306101109u);
            this.ee.VF[4].y = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[4].z = ee1Dec.C.MobUt.UL2F(3301574943u);
            this.ee.VF[4].w = ee1Dec.C.MobUt.UL2F(1065353216u);
            this.ee.VF[5].x = ee1Dec.C.MobUt.UL2F(1061036071u);
            this.ee.VF[5].y = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[5].z = ee1Dec.C.MobUt.UL2F(1059810839u);
            this.ee.VF[5].w = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[6].x = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[6].y = ee1Dec.C.MobUt.UL2F(1065353216u);
            this.ee.VF[6].z = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[6].w = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[7].x = ee1Dec.C.MobUt.UL2F(3207294487u);
            this.ee.VF[7].y = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[7].z = ee1Dec.C.MobUt.UL2F(1061036071u);
            this.ee.VF[7].w = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[8].x = ee1Dec.C.MobUt.UL2F(3306101109u);
            this.ee.VF[8].y = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[8].z = ee1Dec.C.MobUt.UL2F(3301574943u);
            this.ee.VF[8].w = ee1Dec.C.MobUt.UL2F(1065353216u);
            this.ee.VF[9].x = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[9].y = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[9].z = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[9].w = ee1Dec.C.MobUt.UL2F(1065353216u);
            this.ee.VF[10].x = ee1Dec.C.MobUt.UL2F(1221302671u);
            this.ee.VF[10].y = ee1Dec.C.MobUt.UL2F(1221302671u);
            this.ee.VF[10].z = ee1Dec.C.MobUt.UL2F(3290595208u);
            this.ee.VF[10].w = ee1Dec.C.MobUt.UL2F(1129027983u);
            this.ee.VF[11].x = ee1Dec.C.MobUt.UL2F(1278239296u);
            this.ee.VF[11].y = ee1Dec.C.MobUt.UL2F(1277754648u);
            this.ee.VF[11].z = ee1Dec.C.MobUt.UL2F(1266599817u);
            this.ee.VF[11].w = ee1Dec.C.MobUt.UL2F(1186077023u);
            this.ee.VF[12].x = ee1Dec.C.MobUt.UL2F(1157627904u);
            this.ee.VF[12].y = ee1Dec.C.MobUt.UL2F(1157627904u);
            this.ee.VF[12].z = ee1Dec.C.MobUt.UL2F(1249902592u);
            this.ee.VF[12].w = ee1Dec.C.MobUt.UL2F(1249902592u);
            this.ee.VF[13].x = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[13].y = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[13].z = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[13].w = ee1Dec.C.MobUt.UL2F(1132462080u);
            this.ee.VF[14].x = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[14].y = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[14].z = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[14].w = ee1Dec.C.MobUt.UL2F(1153433600u);
            this.ee.VF[15].x = ee1Dec.C.MobUt.UL2F(1068826624u);
            this.ee.VF[15].y = ee1Dec.C.MobUt.UL2F(1065746432u);
            this.ee.VF[15].z = ee1Dec.C.MobUt.UL2F(1060896768u);
            this.ee.VF[15].w = ee1Dec.C.MobUt.UL2F(1068761088u);
            this.ee.VF[16].x = ee1Dec.C.MobUt.UL2F(32435u);
            this.ee.VF[16].y = ee1Dec.C.MobUt.UL2F(31091u);
            this.ee.VF[16].z = ee1Dec.C.MobUt.UL2F(11678u);
            this.ee.VF[16].w = ee1Dec.C.MobUt.UL2F(943151540u);
            this.ee.VF[17].x = ee1Dec.C.MobUt.UL2F(3301739238u);
            this.ee.VF[17].y = ee1Dec.C.MobUt.UL2F(3321541486u);
            this.ee.VF[17].z = ee1Dec.C.MobUt.UL2F(1186077023u);
            this.ee.VF[17].w = ee1Dec.C.MobUt.UL2F(1065353216u);
            this.ee.VF[18].x = ee1Dec.C.MobUt.UL2F(1154255590u);
            this.ee.VF[18].y = ee1Dec.C.MobUt.UL2F(1174057838u);
            this.ee.VF[18].z = ee1Dec.C.MobUt.UL2F(3333560671u);
            this.ee.VF[18].w = ee1Dec.C.MobUt.UL2F(3221225472u);
            this.ee.VF[19].x = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[19].y = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[19].z = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[19].w = ee1Dec.C.MobUt.UL2F(1065353216u);
            this.ee.VF[20].x = ee1Dec.C.MobUt.UL2F(32224u);
            this.ee.VF[20].y = ee1Dec.C.MobUt.UL2F(30845u);
            this.ee.VF[20].z = ee1Dec.C.MobUt.UL2F(11719u);
            this.ee.VF[20].w = ee1Dec.C.MobUt.UL2F(364746u);
            this.ee.VF[21].x = ee1Dec.C.MobUt.UL2F(1158017723u);
            this.ee.VF[21].y = ee1Dec.C.MobUt.UL2F(1159360723u);
            this.ee.VF[21].z = ee1Dec.C.MobUt.UL2F(1144462391u);
            this.ee.VF[21].w = ee1Dec.C.MobUt.UL2F(1186077023u);
            this.ee.VF[22].x = ee1Dec.C.MobUt.UL2F(1157913735u);
            this.ee.VF[22].y = ee1Dec.C.MobUt.UL2F(1159493858u);
            this.ee.VF[22].z = ee1Dec.C.MobUt.UL2F(1144462391u);
            this.ee.VF[22].w = ee1Dec.C.MobUt.UL2F(1186077023u);
            this.ee.VF[23].x = ee1Dec.C.MobUt.UL2F(32688u);
            this.ee.VF[23].y = ee1Dec.C.MobUt.UL2F(31347u);
            this.ee.VF[23].z = ee1Dec.C.MobUt.UL2F(11719u);
            this.ee.VF[23].w = ee1Dec.C.MobUt.UL2F(364746u);
            this.ee.VF[24].x = ee1Dec.C.MobUt.UL2F(1278158170u);
            this.ee.VF[24].y = ee1Dec.C.MobUt.UL2F(1277666762u);
            this.ee.VF[24].z = ee1Dec.C.MobUt.UL2F(1144462391u);
            this.ee.VF[24].w = ee1Dec.C.MobUt.UL2F(1186077023u);
            this.ee.VF[25].x = ee1Dec.C.MobUt.UL2F(3293934063u);
            this.ee.VF[25].y = ee1Dec.C.MobUt.UL2F(3292049472u);
            this.ee.VF[25].z = ee1Dec.C.MobUt.UL2F(1066904244u);
            this.ee.VF[25].w = ee1Dec.C.MobUt.UL2F(3200124770u);
            this.ee.VF[26].x = ee1Dec.C.MobUt.UL2F(1174396132u);
            this.ee.VF[26].y = ee1Dec.C.MobUt.UL2F(1282769649u);
            this.ee.VF[26].z = ee1Dec.C.MobUt.UL2F(1044218678u);
            this.ee.VF[26].w = ee1Dec.C.MobUt.UL2F(3178066551u);
            this.ee.VF[27].x = ee1Dec.C.MobUt.UL2F(1157627904u);
            this.ee.VF[27].y = ee1Dec.C.MobUt.UL2F(1157627904u);
            this.ee.VF[27].z = ee1Dec.C.MobUt.UL2F(1249902592u);
            this.ee.VF[27].w = ee1Dec.C.MobUt.UL2F(1249902592u);
            this.ee.VF[28].x = ee1Dec.C.MobUt.UL2F(1177474794u);
            this.ee.VF[28].y = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[28].z = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[28].w = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[29].x = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[29].y = ee1Dec.C.MobUt.UL2F(1178429652u);
            this.ee.VF[29].z = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[29].w = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[30].x = ee1Dec.C.MobUt.UL2F(1221302671u);
            this.ee.VF[30].y = ee1Dec.C.MobUt.UL2F(1221302671u);
            this.ee.VF[30].z = ee1Dec.C.MobUt.UL2F(3290595208u);
            this.ee.VF[30].w = ee1Dec.C.MobUt.UL2F(1129027983u);
            this.ee.VF[31].x = ee1Dec.C.MobUt.UL2F(1278239296u);
            this.ee.VF[31].y = ee1Dec.C.MobUt.UL2F(1277754648u);
            this.ee.VF[31].z = ee1Dec.C.MobUt.UL2F(1266599817u);
            this.ee.VF[31].w = ee1Dec.C.MobUt.UL2F(1186077023u);
        }
        
        private void initfns() {
            this.dicti2a[1213400u] = new ee1Dec.C.MobUt.Tx8(this.funct001283d8);
            this.dicti2a[1213440u] = new ee1Dec.C.MobUt.Tx8(this.funct00128400);
            this.dicti2a[1213460u] = new ee1Dec.C.MobUt.Tx8(this.funct00128414);
            this.dicti2a[1213472u] = new ee1Dec.C.MobUt.Tx8(this.funct00128420);
            this.dicti2a[1213488u] = new ee1Dec.C.MobUt.Tx8(this.funct00128430);
            this.dicti2a[1213520u] = new ee1Dec.C.MobUt.Tx8(this.funct00128450);
            this.dicti2a[1213556u] = new ee1Dec.C.MobUt.Tx8(this.funct00128474);
            this.dicti2a[1213560u] = new ee1Dec.C.MobUt.Tx8(this.funct00128478);
            this.dicti2a[1213584u] = new ee1Dec.C.MobUt.Tx8(this.funct00128490);
            this.dicti2a[1213592u] = new ee1Dec.C.MobUt.Tx8(this.funct00128498);
            this.dicti2a[1213600u] = new ee1Dec.C.MobUt.Tx8(this.funct001284a0);
            this.dicti2a[1213608u] = new ee1Dec.C.MobUt.Tx8(this.funct001284a8);
            this.dicti2a[1213616u] = new ee1Dec.C.MobUt.Tx8(this.funct001284b0);
            this.dicti2a[1213744u] = new ee1Dec.C.MobUt.Tx8(this.funct00128530);
            this.dicti2a[1213892u] = new ee1Dec.C.MobUt.Tx8(this.funct001285c4);
            this.dicti2a[1213916u] = new ee1Dec.C.MobUt.Tx8(this.funct001285dc);
            this.dicti2a[1213924u] = new ee1Dec.C.MobUt.Tx8(this.funct001285e4);
            this.dicti2a[1213936u] = new ee1Dec.C.MobUt.Tx8(this.funct001285f0);
            this.dicti2a[1213940u] = new ee1Dec.C.MobUt.Tx8(this.funct001285f4);
            this.dicti2a[1213980u] = new ee1Dec.C.MobUt.Tx8(this.funct0012861c);
            this.dicti2a[1213996u] = new ee1Dec.C.MobUt.Tx8(this.funct0012862c);
            this.dicti2a[1214004u] = new ee1Dec.C.MobUt.Tx8(this.funct00128634);
            this.dicti2a[1214044u] = new ee1Dec.C.MobUt.Tx8(this.funct0012865c);
            this.dicti2a[1214060u] = new ee1Dec.C.MobUt.Tx8(this.funct0012866c);
            this.dicti2a[1214064u] = new ee1Dec.C.MobUt.Tx8(this.funct00128670);
            this.dicti2a[1214080u] = new ee1Dec.C.MobUt.Tx8(this.funct00128680);
            this.dicti2a[1214092u] = new ee1Dec.C.MobUt.Tx8(this.funct0012868c);
            this.dicti2a[1214120u] = new ee1Dec.C.MobUt.Tx8(this.funct001286a8);
            this.dicti2a[1214124u] = new ee1Dec.C.MobUt.Tx8(this.funct001286ac);
            this.dicti2a[1214176u] = new ee1Dec.C.MobUt.Tx8(this.funct001286e0);
            this.dicti2a[1214188u] = new ee1Dec.C.MobUt.Tx8(this.funct001286ec);
            this.dicti2a[1214196u] = new ee1Dec.C.MobUt.Tx8(this.funct001286f4);
            this.dicti2a[1214212u] = new ee1Dec.C.MobUt.Tx8(this.funct00128704);
            this.dicti2a[1214216u] = new ee1Dec.C.MobUt.Tx8(this.funct00128708);
            this.dicti2a[1214232u] = new ee1Dec.C.MobUt.Tx8(this.funct00128718);
            this.dicti2a[1214336u] = new ee1Dec.C.MobUt.Tx8(this.funct00128780);
            this.dicti2a[1214348u] = new ee1Dec.C.MobUt.Tx8(this.funct0012878c);
            this.dicti2a[1214356u] = new ee1Dec.C.MobUt.Tx8(this.funct00128794);
            this.dicti2a[1214364u] = new ee1Dec.C.MobUt.Tx8(this.funct0012879c);
            this.dicti2a[1214400u] = new ee1Dec.C.MobUt.Tx8(this.funct001287c0);
            this.dicti2a[1214412u] = new ee1Dec.C.MobUt.Tx8(this.funct001287cc);
            this.dicti2a[1214424u] = new ee1Dec.C.MobUt.Tx8(this.funct001287d8);
            this.dicti2a[1214432u] = new ee1Dec.C.MobUt.Tx8(this.funct001287e0);
            this.dicti2a[1214440u] = new ee1Dec.C.MobUt.Tx8(this.funct001287e8);
            this.dicti2a[1214452u] = new ee1Dec.C.MobUt.Tx8(this.funct001287f4);
            this.dicti2a[1214460u] = new ee1Dec.C.MobUt.Tx8(this.funct001287fc);
            this.dicti2a[1214468u] = new ee1Dec.C.MobUt.Tx8(this.funct00128804);
            this.dicti2a[1214528u] = new ee1Dec.C.MobUt.Tx8(this.funct00128840);
            this.dicti2a[1214548u] = new ee1Dec.C.MobUt.Tx8(this.funct00128854);
            this.dicti2a[1214572u] = new ee1Dec.C.MobUt.Tx8(this.funct0012886c);
            this.dicti2a[1214580u] = new ee1Dec.C.MobUt.Tx8(this.funct00128874);
            this.dicti2a[1214588u] = new ee1Dec.C.MobUt.Tx8(this.funct0012887c);
            this.dicti2a[1214596u] = new ee1Dec.C.MobUt.Tx8(this.funct00128884);
            this.dicti2a[1214604u] = new ee1Dec.C.MobUt.Tx8(this.funct0012888c);
            this.dicti2a[1214640u] = new ee1Dec.C.MobUt.Tx8(this.funct001288b0);
            this.dicti2a[1214644u] = new ee1Dec.C.MobUt.Tx8(this.funct001288b4);
            this.dicti2a[1214672u] = new ee1Dec.C.MobUt.Tx8(this.funct001288d0);
            this.dicti2a[1214744u] = new ee1Dec.C.MobUt.Tx8(this.funct00128918);
            this.dicti2a[1214808u] = new ee1Dec.C.MobUt.Tx8(this.funct00128958);
            this.dicti2a[1214828u] = new ee1Dec.C.MobUt.Tx8(this.funct0012896c);
            this.dicti2a[1214852u] = new ee1Dec.C.MobUt.Tx8(this.funct00128984);
            this.dicti2a[1214876u] = new ee1Dec.C.MobUt.Tx8(this.funct0012899c);
            this.dicti2a[1214900u] = new ee1Dec.C.MobUt.Tx8(this.funct001289b4);
            this.dicti2a[1214932u] = new ee1Dec.C.MobUt.Tx8(this.funct001289d4);
            this.dicti2a[1214948u] = new ee1Dec.C.MobUt.Tx8(this.funct001289e4);
            this.dicti2a[1214976u] = new ee1Dec.C.MobUt.Tx8(this.funct00128a00);
            this.dicti2a[1215000u] = new ee1Dec.C.MobUt.Tx8(this.funct00128a18);
            this.dicti2a[1215024u] = new ee1Dec.C.MobUt.Tx8(this.funct00128a30);
            this.dicti2a[1215076u] = new ee1Dec.C.MobUt.Tx8(this.funct00128a64);
            this.dicti2a[1215092u] = new ee1Dec.C.MobUt.Tx8(this.funct00128a74);
            this.dicti2a[1215132u] = new ee1Dec.C.MobUt.Tx8(this.funct00128a9c);
            this.dicti2a[1215168u] = new ee1Dec.C.MobUt.Tx8(this.funct00128ac0);
            this.dicti2a[1215204u] = new ee1Dec.C.MobUt.Tx8(this.funct00128ae4);
            this.dicti2a[1215220u] = new ee1Dec.C.MobUt.Tx8(this.funct00128af4);
            this.dicti2a[1215244u] = new ee1Dec.C.MobUt.Tx8(this.funct00128b0c);
        }
    }
}
