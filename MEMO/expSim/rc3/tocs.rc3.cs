﻿namespace ee1Dec.C {
    using System.Collections.Generic;
    
    
    public partial class Mobrc3 {
        
        private ee1Dec.C.CustEE ee = new ee1Dec.C.CustEE();
        
        private SortedDictionary<uint, ee1Dec.C.MobUt.Tx8> dicti2a = new SortedDictionary<uint, ee1Dec.C.MobUt.Tx8>();
        
        public virtual void funct00120690() {
            this.ee.pc = 1181340u;
            // @00120690   LW t7, $0004(a0)
            MobUt.LW(this.ee, this.ee.t7, (4u + this.ee.a0.UL0));
            // @00120694   JR ra
            this.ee.pc = this.ee.ra.UL[0];
            // @00120698   LHU v0, $0010(t7)
            MobUt.LHU(this.ee, this.ee.v0, (16u + this.ee.t7.UL0));
        }
        
        public virtual void funct001206a0() {
            this.ee.pc = 1181372u;
            // @001206A0   ADDIU sp, sp, $fff0
            this.ee.sp.SD0 = ((int)((this.ee.sp.SD0 + -16)));
            // @001206A4   LW t6, $0000(a0)
            MobUt.LW(this.ee, this.ee.t6, (0u + this.ee.a0.UL0));
            // @001206A8   SD s0, $0000(sp)
            MobUt.SD(this.ee, this.ee.s0, (0u + this.ee.sp.UL0));
            // @001206AC   SD ra, $0008(sp)
            MobUt.SD(this.ee, this.ee.ra, (8u + this.ee.sp.UL0));
            // @001206B0   LW v0, $001c(t6)
            MobUt.LW(this.ee, this.ee.v0, (28u + this.ee.t6.UL0));
            // @001206B4   JALR ra, v0
            this.ee.ra.UD0 = 1181372u;
            this.ee.pc = this.ee.v0.UL0;
            // @001206B8   LW s0, $0004(a0)
            MobUt.LW(this.ee, this.ee.s0, (4u + this.ee.a0.UL0));
        }
        
        public virtual void funct001206bc() {
            this.ee.pc = 1181380u;
            // @001206BC   BEQ v0, zero, $001206cc
            if ((this.ee.v0.UD0 == this.ee.r0.UD0)) {
                this.ee.pc = 1181388u;
            }
            // @001206C0   DADDU t7, zero, zero
            this.ee.t7.UD0 = (this.ee.r0.UD0 + this.ee.r0.UD0);
        }
        
        public virtual void funct001206c4() {
            this.ee.pc = 1181408u;
            // @001206C4   LW t7, $0014(s0)
            MobUt.LW(this.ee, this.ee.t7, (20u + this.ee.s0.UL0));
            // @001206C8   ADDU t7, s0, t7
            this.ee.t7.SD0 = ((int)((this.ee.s0.SD0 + this.ee.t7.SD0)));
            // @001206CC   LD s0, $0000(sp)
            MobUt.LD(this.ee, this.ee.s0, (0u + this.ee.sp.UL0));
            // @001206D0   DADDU v0, t7, zero
            this.ee.v0.UD0 = (this.ee.t7.UD0 + this.ee.r0.UD0);
            // @001206D4   LD ra, $0008(sp)
            MobUt.LD(this.ee, this.ee.ra, (8u + this.ee.sp.UL0));
            // @001206D8   JR ra
            this.ee.pc = this.ee.ra.UL[0];
            // @001206DC   ADDIU sp, sp, $0010
            this.ee.sp.SD0 = ((int)((this.ee.sp.SD0 + 16)));
        }
        
        public virtual void funct00128260() {
            this.ee.pc = 1213076u;
            // @00128260   ADDIU sp, sp, $ffe0
            this.ee.sp.SD0 = ((int)((this.ee.sp.SD0 + -32)));
            // @00128264   SD s0, $0000(sp)
            MobUt.SD(this.ee, this.ee.s0, (0u + this.ee.sp.UL0));
            // @00128268   SD s1, $0008(sp)
            MobUt.SD(this.ee, this.ee.s1, (8u + this.ee.sp.UL0));
            // @0012826C   SD s2, $0010(sp)
            MobUt.SD(this.ee, this.ee.s2, (16u + this.ee.sp.UL0));
            // @00128270   DADDU s0, a1, zero
            this.ee.s0.UD0 = (this.ee.a1.UD0 + this.ee.r0.UD0);
            // @00128274   SD ra, $0018(sp)
            MobUt.SD(this.ee, this.ee.ra, (24u + this.ee.sp.UL0));
            // @00128278   LW t7, $0014(a1)
            MobUt.LW(this.ee, this.ee.t7, (20u + this.ee.a1.UL0));
            // @0012827C   LW s2, $0004(a0)
            MobUt.LW(this.ee, this.ee.s2, (4u + this.ee.a0.UL0));
            // @00128280   LW t6, $0000(t7)
            MobUt.LW(this.ee, this.ee.t6, (0u + this.ee.t7.UL0));
            // @00128284   LHU s1, $0010(s2)
            MobUt.LHU(this.ee, this.ee.s1, (16u + this.ee.s2.UL0));
            // @00128288   LW v0, $0020(t6)
            MobUt.LW(this.ee, this.ee.v0, (32u + this.ee.t6.UL0));
            // @0012828C   JALR ra, v0
            this.ee.ra.UD0 = 1213076u;
            this.ee.pc = this.ee.v0.UL0;
            // @00128290   DADDU a0, t7, zero
            this.ee.a0.UD0 = (this.ee.t7.UD0 + this.ee.r0.UD0);
        }
        
        public virtual void funct00128294() {
            this.ee.pc = 1213084u;
            // @00128294   BEQ s1, zero, $00128304
            if ((this.ee.s1.UD0 == this.ee.r0.UD0)) {
                this.ee.pc = 1213188u;
            }
            // @00128298   DADDU t2, zero, zero
            this.ee.t2.UD0 = (this.ee.r0.UD0 + this.ee.r0.UD0);
        }
        
        public virtual void funct0012829c() {
            this.ee.pc = 1213108u;
            // @0012829C   LW t7, $001c(s0)
            MobUt.LW(this.ee, this.ee.t7, (28u + this.ee.s0.UL0));
            // @001282A0   SLL t5, t2, 4
            MobUt.SLL(this.ee.t5, this.ee.t2, 4);
            // @001282A4   ADDIU t6, v0, $0010
            this.ee.t6.SD0 = ((int)((this.ee.v0.SD0 + 16)));
            // @001282A8   ADDU t7, t7, t5
            this.ee.t7.SD0 = ((int)((this.ee.t7.SD0 + this.ee.t5.SD0)));
            // @001282AC   BEQL t7, t6, $001282c0
            if ((this.ee.t7.UD0 == this.ee.t6.UD0)) {
                this.ee.pc = 1213120u;
                // @001282B0   LW t7, $0020(s0)
                MobUt.LW(this.ee, this.ee.t7, (32u + this.ee.s0.UL0));
            }
        }
        
        public virtual void funct001282b4() {
            this.ee.pc = 1213136u;
            // @001282B4   LQ t0, $0000(t6)
            MobUt.LQ(this.ee, this.ee.t0, (0u + this.ee.t6.UL[0]));
            // @001282B8   SQ t0, $0000(t7)
            MobUt.SQ(this.ee, this.ee.t0, (0u + this.ee.t7.UL[0]));
            // @001282BC   LW t7, $0020(s0)
            MobUt.LW(this.ee, this.ee.t7, (32u + this.ee.s0.UL0));
            // @001282C0   ADDIU t6, v0, $0020
            this.ee.t6.SD0 = ((int)((this.ee.v0.SD0 + 32)));
            // @001282C4   ADDU t7, t7, t5
            this.ee.t7.SD0 = ((int)((this.ee.t7.SD0 + this.ee.t5.SD0)));
            // @001282C8   BEQL t7, t6, $001282dc
            if ((this.ee.t7.UD0 == this.ee.t6.UD0)) {
                this.ee.pc = 1213148u;
                // @001282CC   LW t7, $0024(s0)
                MobUt.LW(this.ee, this.ee.t7, (36u + this.ee.s0.UL0));
            }
        }
        
        public virtual void funct001282d0() {
            this.ee.pc = 1213164u;
            // @001282D0   LQ t0, $0000(t6)
            MobUt.LQ(this.ee, this.ee.t0, (0u + this.ee.t6.UL[0]));
            // @001282D4   SQ t0, $0000(t7)
            MobUt.SQ(this.ee, this.ee.t0, (0u + this.ee.t7.UL[0]));
            // @001282D8   LW t7, $0024(s0)
            MobUt.LW(this.ee, this.ee.t7, (36u + this.ee.s0.UL0));
            // @001282DC   ADDIU t6, v0, $0030
            this.ee.t6.SD0 = ((int)((this.ee.v0.SD0 + 48)));
            // @001282E0   ADDU t7, t7, t5
            this.ee.t7.SD0 = ((int)((this.ee.t7.SD0 + this.ee.t5.SD0)));
            // @001282E4   BEQL t7, t6, $001282f8
            if ((this.ee.t7.UD0 == this.ee.t6.UD0)) {
                this.ee.pc = 1213176u;
                // @001282E8   ADDIU t2, t2, $0001
                this.ee.t2.SD0 = ((int)((this.ee.t2.SD0 + 1)));
            }
        }
        
        public virtual void funct001282ec() {
            this.ee.pc = 1213188u;
            // @001282EC   LQ t0, $0000(t6)
            MobUt.LQ(this.ee, this.ee.t0, (0u + this.ee.t6.UL[0]));
            // @001282F0   SQ t0, $0000(t7)
            MobUt.SQ(this.ee, this.ee.t0, (0u + this.ee.t7.UL[0]));
            // @001282F4   ADDIU t2, t2, $0001
            this.ee.t2.SD0 = ((int)((this.ee.t2.SD0 + 1)));
            // @001282F8   SLT t7, t2, s1
            this.ee.t7.UD0 = System.Convert.ToByte((this.ee.t2.SD0 < this.ee.s1.SD0));
            // @001282FC   BNE t7, zero, $0012829c
            if ((this.ee.t7.UD0 != this.ee.r0.UD0)) {
                this.ee.pc = 1213084u;
            }
            // @00128300   ADDIU v0, v0, $0040
            this.ee.v0.SD0 = ((int)((this.ee.v0.SD0 + 64)));
        }
        
        public virtual void funct00128304() {
            this.ee.pc = 1213200u;
            // @00128304   LW s1, $0028(s2)
            MobUt.LW(this.ee, this.ee.s1, (40u + this.ee.s2.UL0));
            // @00128308   BEQL s1, zero, $00128384
            if ((this.ee.s1.UD0 == this.ee.r0.UD0)) {
                this.ee.pc = 1213316u;
                // @0012830C   LD s0, $0000(sp)
                MobUt.LD(this.ee, this.ee.s0, (0u + this.ee.sp.UL0));
            }
        }
        
        public virtual void funct00128310() {
            this.ee.pc = 1213216u;
            // @00128310   LW t7, $0024(s2)
            MobUt.LW(this.ee, this.ee.t7, (36u + this.ee.s2.UL0));
            // @00128314   DADDU t2, zero, zero
            this.ee.t2.UD0 = (this.ee.r0.UD0 + this.ee.r0.UD0);
            // @00128318   BLEZ s1, $00128380
            if ((this.ee.s1.SD0 <= 0)) {
                this.ee.pc = 1213312u;
            }
            // @0012831C   ADDU t3, s2, t7
            this.ee.t3.SD0 = ((int)((this.ee.s2.SD0 + this.ee.t7.SD0)));
        }
        
        public virtual void funct00128320() {
            this.ee.pc = 1213236u;
            // @00128320   LHU t4, $0002(t3)
            MobUt.LHU(this.ee, this.ee.t4, (2u + this.ee.t3.UL0));
            // @00128324   ANDI t6, t4, $ffff
            this.ee.t6.UD0 = ((ushort)((this.ee.t4.US0 & 65535)));
            // @00128328   SLTIU t7, t6, $0009
            this.ee.t7.UD0 = System.Convert.ToByte((this.ee.t6.UD0 < 9ul));
            // @0012832C   BEQ t7, zero, $00128370
            if ((this.ee.t7.UD0 == this.ee.r0.UD0)) {
                this.ee.pc = 1213296u;
            }
            // @00128330   SLL t7, t6, 2
            MobUt.SLL(this.ee.t7, this.ee.t6, 2);
        }
        
        public virtual void funct00128334() {
            this.ee.pc = 1213260u;
            // @00128334   LUI t6, $0038
            this.ee.t6.SD0 = 3670016;
            // @00128338   ADDIU t6, t6, $8218
            this.ee.t6.SD0 = ((int)((this.ee.t6.SD0 + -32232)));
            // @0012833C   ADDU t7, t7, t6
            this.ee.t7.SD0 = ((int)((this.ee.t7.SD0 + this.ee.t6.SD0)));
            // @00128340   LW t5, $0000(t7)
            MobUt.LW(this.ee, this.ee.t5, (0u + this.ee.t7.UL0));
            // @00128344   JR t5
            this.ee.pc = this.ee.t5.UL[0];
            // @00128348   NOP 
            MobUt.Latency();
        }
        
        public virtual void funct0012834c() {
            this.ee.pc = 1213312u;
            // @0012834C   LHU t6, $0000(t3)
            MobUt.LHU(this.ee, this.ee.t6, (0u + this.ee.t3.UL0));
            // @00128350   ANDI t5, t4, $ffff
            this.ee.t5.UD0 = ((ushort)((this.ee.t4.US0 & 65535)));
            // @00128354   LW t7, $001c(s0)
            MobUt.LW(this.ee, this.ee.t7, (28u + this.ee.s0.UL0));
            // @00128358   SLL t5, t5, 2
            MobUt.SLL(this.ee.t5, this.ee.t5, 2);
            // @0012835C   SLL t6, t6, 4
            MobUt.SLL(this.ee.t6, this.ee.t6, 4);
            // @00128360   LWC1 $f0, $0004(t3)
            MobUt.LWC1(this.ee, this.ee.fpr[0], (4u + this.ee.t3.UL0));
            // @00128364   ADDU t7, t7, t6
            this.ee.t7.SD0 = ((int)((this.ee.t7.SD0 + this.ee.t6.SD0)));
            // @00128368   ADDU t7, t7, t5
            this.ee.t7.SD0 = ((int)((this.ee.t7.SD0 + this.ee.t5.SD0)));
            // @0012836C   SWC1 $f0, $0000(t7)
            MobUt.SWC1(this.ee, this.ee.fpr[0], (0u + this.ee.t7.UL0));
            // @00128370   ADDIU t2, t2, $0001
            this.ee.t2.SD0 = ((int)((this.ee.t2.SD0 + 1)));
            // @00128374   SLT t7, t2, s1
            this.ee.t7.UD0 = System.Convert.ToByte((this.ee.t2.SD0 < this.ee.s1.SD0));
            // @00128378   BNE t7, zero, $00128320
            if ((this.ee.t7.UD0 != this.ee.r0.UD0)) {
                this.ee.pc = 1213216u;
            }
            // @0012837C   ADDIU t3, t3, $0008
            this.ee.t3.SD0 = ((int)((this.ee.t3.SD0 + 8)));
        }
        
        public virtual void funct00128370() {
            this.ee.pc = 1213312u;
            // @00128370   ADDIU t2, t2, $0001
            this.ee.t2.SD0 = ((int)((this.ee.t2.SD0 + 1)));
            // @00128374   SLT t7, t2, s1
            this.ee.t7.UD0 = System.Convert.ToByte((this.ee.t2.SD0 < this.ee.s1.SD0));
            // @00128378   BNE t7, zero, $00128320
            if ((this.ee.t7.UD0 != this.ee.r0.UD0)) {
                this.ee.pc = 1213216u;
            }
            // @0012837C   ADDIU t3, t3, $0008
            this.ee.t3.SD0 = ((int)((this.ee.t3.SD0 + 8)));
        }
        
        public virtual void funct00128380() {
            this.ee.pc = 1213336u;
            // @00128380   LD s0, $0000(sp)
            MobUt.LD(this.ee, this.ee.s0, (0u + this.ee.sp.UL0));
            // @00128384   LD s1, $0008(sp)
            MobUt.LD(this.ee, this.ee.s1, (8u + this.ee.sp.UL0));
            // @00128388   LD s2, $0010(sp)
            MobUt.LD(this.ee, this.ee.s2, (16u + this.ee.sp.UL0));
            // @0012838C   LD ra, $0018(sp)
            MobUt.LD(this.ee, this.ee.ra, (24u + this.ee.sp.UL0));
            // @00128390   JR ra
            this.ee.pc = this.ee.ra.UL[0];
            // @00128394   ADDIU sp, sp, $0020
            this.ee.sp.SD0 = ((int)((this.ee.sp.SD0 + 32)));
        }
        
        public virtual void funct00128398() {
            this.ee.pc = 1213380u;
            // @00128398   ANDI t7, t4, $ffff
            this.ee.t7.UD0 = ((ushort)((this.ee.t4.US0 & 65535)));
            // @0012839C   LHU t5, $0000(t3)
            MobUt.LHU(this.ee, this.ee.t5, (0u + this.ee.t3.UL0));
            // @001283A0   LW t6, $0020(s0)
            MobUt.LW(this.ee, this.ee.t6, (32u + this.ee.s0.UL0));
            // @001283A4   ADDIU t7, t7, $fffd
            this.ee.t7.SD0 = ((int)((this.ee.t7.SD0 + -3)));
            // @001283A8   SLL t5, t5, 4
            MobUt.SLL(this.ee.t5, this.ee.t5, 4);
            // @001283AC   LWC1 $f0, $0004(t3)
            MobUt.LWC1(this.ee, this.ee.fpr[0], (4u + this.ee.t3.UL0));
            // @001283B0   SLL t7, t7, 2
            MobUt.SLL(this.ee.t7, this.ee.t7, 2);
            // @001283B4   ADDU t6, t6, t5
            this.ee.t6.SD0 = ((int)((this.ee.t6.SD0 + this.ee.t5.SD0)));
            // @001283B8   ADDU t6, t6, t7
            this.ee.t6.SD0 = ((int)((this.ee.t6.SD0 + this.ee.t7.SD0)));
            // @001283BC   BEQ zero, zero, $00128370
            if ((this.ee.r0.UD0 == this.ee.r0.UD0)) {
                this.ee.pc = 1213296u;
            }
            // @001283C0   SWC1 $f0, $0000(t6)
            MobUt.SWC1(this.ee, this.ee.fpr[0], (0u + this.ee.t6.UL0));
        }
        
        public virtual void funct001283a8() {
            this.ee.pc = 1213380u;
            // @001283A8   SLL t5, t5, 4
            MobUt.SLL(this.ee.t5, this.ee.t5, 4);
            // @001283AC   LWC1 $f0, $0004(t3)
            MobUt.LWC1(this.ee, this.ee.fpr[0], (4u + this.ee.t3.UL0));
            // @001283B0   SLL t7, t7, 2
            MobUt.SLL(this.ee.t7, this.ee.t7, 2);
            // @001283B4   ADDU t6, t6, t5
            this.ee.t6.SD0 = ((int)((this.ee.t6.SD0 + this.ee.t5.SD0)));
            // @001283B8   ADDU t6, t6, t7
            this.ee.t6.SD0 = ((int)((this.ee.t6.SD0 + this.ee.t7.SD0)));
            // @001283BC   BEQ zero, zero, $00128370
            if ((this.ee.r0.UD0 == this.ee.r0.UD0)) {
                this.ee.pc = 1213296u;
            }
            // @001283C0   SWC1 $f0, $0000(t6)
            MobUt.SWC1(this.ee, this.ee.fpr[0], (0u + this.ee.t6.UL0));
        }
        
        public virtual void funct001283c4() {
            this.ee.pc = 1213400u;
            // @001283C4   ANDI t7, t4, $ffff
            this.ee.t7.UD0 = ((ushort)((this.ee.t4.US0 & 65535)));
            // @001283C8   LHU t5, $0000(t3)
            MobUt.LHU(this.ee, this.ee.t5, (0u + this.ee.t3.UL0));
            // @001283CC   LW t6, $0024(s0)
            MobUt.LW(this.ee, this.ee.t6, (36u + this.ee.s0.UL0));
            // @001283D0   BEQ zero, zero, $001283a8
            if ((this.ee.r0.UD0 == this.ee.r0.UD0)) {
                this.ee.pc = 1213352u;
            }
            // @001283D4   ADDIU t7, t7, $fffa
            this.ee.t7.SD0 = ((int)((this.ee.t7.SD0 + -6)));
        }
        
        private void initstate() {
            this.initregs();
            this.initfns();
        }
        
        private void initregs() {
            this.ee.r0.UD0 = 0ul;
            this.ee.r0.UD1 = 0ul;
            this.ee.at.UD0 = 1070141403ul;
            this.ee.at.UD1 = 0ul;
            this.ee.v0.UD0 = 229ul;
            this.ee.v0.UD1 = 0ul;
            this.ee.v1.UD0 = 3728656ul;
            this.ee.v1.UD1 = 4575657222566786353ul;
            this.ee.a0.UD0 = 10454288ul;
            this.ee.a0.UD1 = 9259542123273814144ul;
            this.ee.a1.UD0 = 28030352ul;
            this.ee.a1.UD1 = 0ul;
            this.ee.a2.UD0 = 28029104ul;
            this.ee.a2.UD1 = 4575657222566786353ul;
            this.ee.a3.UD0 = 0ul;
            this.ee.a3.UD1 = 0ul;
            this.ee.t0.UD0 = 3730800ul;
            this.ee.t0.UD1 = 0ul;
            this.ee.t1.UD0 = 4575657221408423936ul;
            this.ee.t1.UD1 = 0ul;
            this.ee.t2.UD0 = 0ul;
            this.ee.t2.UD1 = 1065353216ul;
            this.ee.t3.UD0 = 0ul;
            this.ee.t3.UD1 = 4575657221408423936ul;
            this.ee.t4.UD0 = 28108432ul;
            this.ee.t4.UD1 = 4575657221408423936ul;
            this.ee.t5.UD0 = 3638276ul;
            this.ee.t5.UD1 = 0ul;
            this.ee.t6.UD0 = 0ul;
            this.ee.t6.UD1 = 0ul;
            this.ee.t7.UD0 = 3473408ul;
            this.ee.t7.UD1 = 0ul;
            this.ee.s0.UD0 = 10454432ul;
            this.ee.s0.UD1 = 0ul;
            this.ee.s1.UD0 = 10454288ul;
            this.ee.s1.UD1 = 0ul;
            this.ee.s2.UD0 = 28030352ul;
            this.ee.s2.UD1 = 0ul;
            this.ee.s3.UD0 = 3730800ul;
            this.ee.s3.UD1 = 0ul;
            this.ee.s4.UD0 = 28029104ul;
            this.ee.s4.UD1 = 0ul;
            this.ee.s5.UD0 = 0ul;
            this.ee.s5.UD1 = 0ul;
            this.ee.s6.UD0 = 3730800ul;
            this.ee.s6.UD1 = 0ul;
            this.ee.s7.UD0 = 0ul;
            this.ee.s7.UD1 = 0ul;
            this.ee.t8.UD0 = 3728624ul;
            this.ee.t8.UD1 = 0ul;
            this.ee.t9.UD0 = 3728640ul;
            this.ee.t9.UD1 = 0ul;
            this.ee.k0.UD0 = 1879247891ul;
            this.ee.k0.UD1 = 0ul;
            this.ee.k1.UD0 = 0ul;
            this.ee.k1.UD1 = 0ul;
            this.ee.gp.UD0 = 3704508ul;
            this.ee.gp.UD1 = 0ul;
            this.ee.sp.UD0 = 3730592ul;
            this.ee.sp.UD1 = 0ul;
            this.ee.s8.UD0 = 0ul;
            this.ee.s8.UD1 = 0ul;
            this.ee.ra.UD0 = 1248188ul;
            this.ee.ra.UD1 = 0ul;
            this.ee.fpr[0].f = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.fpr[1].f = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.fpr[2].f = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.fpr[3].f = ee1Dec.C.MobUt.UL2F(1114636288u);
            this.ee.fpr[4].f = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.fpr[5].f = ee1Dec.C.MobUt.UL2F(3212836864u);
            this.ee.fpr[6].f = ee1Dec.C.MobUt.UL2F(1073741824u);
            this.ee.fpr[7].f = ee1Dec.C.MobUt.UL2F(1124007936u);
            this.ee.fpr[8].f = ee1Dec.C.MobUt.UL2F(3279945728u);
            this.ee.fpr[9].f = ee1Dec.C.MobUt.UL2F(1108805435u);
            this.ee.fpr[10].f = ee1Dec.C.MobUt.UL2F(1108805435u);
            this.ee.fpr[11].f = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.fpr[12].f = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.fpr[13].f = ee1Dec.C.MobUt.UL2F(1086918619u);
            this.ee.fpr[14].f = ee1Dec.C.MobUt.UL2F(3308588622u);
            this.ee.fpr[15].f = ee1Dec.C.MobUt.UL2F(3298955796u);
            this.ee.fpr[16].f = ee1Dec.C.MobUt.UL2F(1218199392u);
            this.ee.fpr[17].f = ee1Dec.C.MobUt.UL2F(3308588622u);
            this.ee.fpr[18].f = ee1Dec.C.MobUt.UL2F(1009504234u);
            this.ee.fpr[19].f = ee1Dec.C.MobUt.UL2F(1065353216u);
            this.ee.fpr[20].f = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.fpr[21].f = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.fpr[22].f = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.fpr[23].f = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.fpr[24].f = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.fpr[25].f = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.fpr[26].f = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.fpr[27].f = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.fpr[28].f = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.fpr[29].f = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.fpr[30].f = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.fpr[31].f = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.fpracc.f = ee1Dec.C.MobUt.UL2F(2147483648u);
            this.ee.VF[0].x = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[0].y = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[0].z = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[0].w = ee1Dec.C.MobUt.UL2F(1065353216u);
            this.ee.VF[1].x = ee1Dec.C.MobUt.UL2F(1059810839u);
            this.ee.VF[1].y = ee1Dec.C.MobUt.UL2F(2147483648u);
            this.ee.VF[1].z = ee1Dec.C.MobUt.UL2F(3208519719u);
            this.ee.VF[1].w = ee1Dec.C.MobUt.UL2F(2147483648u);
            this.ee.VF[2].x = ee1Dec.C.MobUt.UL2F(3212836864u);
            this.ee.VF[2].y = ee1Dec.C.MobUt.UL2F(4294967295u);
            this.ee.VF[2].z = ee1Dec.C.MobUt.UL2F(1059810839u);
            this.ee.VF[2].w = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[3].x = ee1Dec.C.MobUt.UL2F(3207294487u);
            this.ee.VF[3].y = ee1Dec.C.MobUt.UL2F(2147483648u);
            this.ee.VF[3].z = ee1Dec.C.MobUt.UL2F(1061036071u);
            this.ee.VF[3].w = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[4].x = ee1Dec.C.MobUt.UL2F(3306101109u);
            this.ee.VF[4].y = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[4].z = ee1Dec.C.MobUt.UL2F(3301574943u);
            this.ee.VF[4].w = ee1Dec.C.MobUt.UL2F(1065353216u);
            this.ee.VF[5].x = ee1Dec.C.MobUt.UL2F(1061036071u);
            this.ee.VF[5].y = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[5].z = ee1Dec.C.MobUt.UL2F(1059810839u);
            this.ee.VF[5].w = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[6].x = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[6].y = ee1Dec.C.MobUt.UL2F(1065353216u);
            this.ee.VF[6].z = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[6].w = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[7].x = ee1Dec.C.MobUt.UL2F(3207294487u);
            this.ee.VF[7].y = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[7].z = ee1Dec.C.MobUt.UL2F(1061036071u);
            this.ee.VF[7].w = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[8].x = ee1Dec.C.MobUt.UL2F(3306101109u);
            this.ee.VF[8].y = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[8].z = ee1Dec.C.MobUt.UL2F(3301574943u);
            this.ee.VF[8].w = ee1Dec.C.MobUt.UL2F(1065353216u);
            this.ee.VF[9].x = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[9].y = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[9].z = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[9].w = ee1Dec.C.MobUt.UL2F(1065353216u);
            this.ee.VF[10].x = ee1Dec.C.MobUt.UL2F(1063096909u);
            this.ee.VF[10].y = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[10].z = ee1Dec.C.MobUt.UL2F(1056979462u);
            this.ee.VF[10].w = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[11].x = ee1Dec.C.MobUt.UL2F(3424065268u);
            this.ee.VF[11].y = ee1Dec.C.MobUt.UL2F(3420346891u);
            this.ee.VF[11].z = ee1Dec.C.MobUt.UL2F(1266697133u);
            this.ee.VF[11].w = ee1Dec.C.MobUt.UL2F(3326928012u);
            this.ee.VF[12].x = ee1Dec.C.MobUt.UL2F(1157627904u);
            this.ee.VF[12].y = ee1Dec.C.MobUt.UL2F(1157627904u);
            this.ee.VF[12].z = ee1Dec.C.MobUt.UL2F(1249902592u);
            this.ee.VF[12].w = ee1Dec.C.MobUt.UL2F(1249902592u);
            this.ee.VF[13].x = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[13].y = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[13].z = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[13].w = ee1Dec.C.MobUt.UL2F(1132462080u);
            this.ee.VF[14].x = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[14].y = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[14].z = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[14].w = ee1Dec.C.MobUt.UL2F(1153433600u);
            this.ee.VF[15].x = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[15].y = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[15].z = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[15].w = ee1Dec.C.MobUt.UL2F(1065353216u);
            this.ee.VF[16].x = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[16].y = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[16].z = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[16].w = ee1Dec.C.MobUt.UL2F(1065353216u);
            this.ee.VF[17].x = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[17].y = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[17].z = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[17].w = ee1Dec.C.MobUt.UL2F(1065353216u);
            this.ee.VF[18].x = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[18].y = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[18].z = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[18].w = ee1Dec.C.MobUt.UL2F(1065353216u);
            this.ee.VF[19].x = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[19].y = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[19].z = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[19].w = ee1Dec.C.MobUt.UL2F(1065353216u);
            this.ee.VF[20].x = ee1Dec.C.MobUt.UL2F(1132396544u);
            this.ee.VF[20].y = ee1Dec.C.MobUt.UL2F(1132396544u);
            this.ee.VF[20].z = ee1Dec.C.MobUt.UL2F(1132396544u);
            this.ee.VF[20].w = ee1Dec.C.MobUt.UL2F(1132396544u);
            this.ee.VF[21].x = ee1Dec.C.MobUt.UL2F(3255228128u);
            this.ee.VF[21].y = ee1Dec.C.MobUt.UL2F(3271692116u);
            this.ee.VF[21].z = ee1Dec.C.MobUt.UL2F(1045034100u);
            this.ee.VF[21].w = ee1Dec.C.MobUt.UL2F(3179088223u);
            this.ee.VF[22].x = ee1Dec.C.MobUt.UL2F(1107802079u);
            this.ee.VF[22].y = ee1Dec.C.MobUt.UL2F(3264346834u);
            this.ee.VF[22].z = ee1Dec.C.MobUt.UL2F(3176702955u);
            this.ee.VF[22].w = ee1Dec.C.MobUt.UL2F(1015527391u);
            this.ee.VF[23].x = ee1Dec.C.MobUt.UL2F(3356440612u);
            this.ee.VF[23].y = ee1Dec.C.MobUt.UL2F(3359265845u);
            this.ee.VF[23].z = ee1Dec.C.MobUt.UL2F(1266672937u);
            this.ee.VF[23].w = ee1Dec.C.MobUt.UL2F(3268806144u);
            this.ee.VF[24].x = ee1Dec.C.MobUt.UL2F(1125429961u);
            this.ee.VF[24].y = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[24].z = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[24].w = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[25].x = ee1Dec.C.MobUt.UL2F(1135922052u);
            this.ee.VF[25].y = ee1Dec.C.MobUt.UL2F(1135922052u);
            this.ee.VF[25].z = ee1Dec.C.MobUt.UL2F(3205516298u);
            this.ee.VF[25].w = ee1Dec.C.MobUt.UL2F(1043647364u);
            this.ee.VF[26].x = ee1Dec.C.MobUt.UL2F(692546556u);
            this.ee.VF[26].y = ee1Dec.C.MobUt.UL2F(3268856155u);
            this.ee.VF[26].z = ee1Dec.C.MobUt.UL2F(2761894368u);
            this.ee.VF[26].w = ee1Dec.C.MobUt.UL2F(600271868u);
            this.ee.VF[27].x = ee1Dec.C.MobUt.UL2F(1157627904u);
            this.ee.VF[27].y = ee1Dec.C.MobUt.UL2F(1157627904u);
            this.ee.VF[27].z = ee1Dec.C.MobUt.UL2F(1249902592u);
            this.ee.VF[27].w = ee1Dec.C.MobUt.UL2F(1249902592u);
            this.ee.VF[28].x = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[28].y = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[28].z = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[28].w = ee1Dec.C.MobUt.UL2F(1132462080u);
            this.ee.VF[29].x = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[29].y = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[29].z = ee1Dec.C.MobUt.UL2F(0u);
            this.ee.VF[29].w = ee1Dec.C.MobUt.UL2F(1153433600u);
            this.ee.VF[30].x = ee1Dec.C.MobUt.UL2F(1006632960u);
            this.ee.VF[30].y = ee1Dec.C.MobUt.UL2F(1006632960u);
            this.ee.VF[30].z = ee1Dec.C.MobUt.UL2F(1006632960u);
            this.ee.VF[30].w = ee1Dec.C.MobUt.UL2F(1006632960u);
            this.ee.VF[31].x = ee1Dec.C.MobUt.UL2F(3338985616u);
            this.ee.VF[31].y = ee1Dec.C.MobUt.UL2F(3348670570u);
            this.ee.VF[31].z = ee1Dec.C.MobUt.UL2F(1266672761u);
            this.ee.VF[31].w = ee1Dec.C.MobUt.UL2F(3260025856u);
        }
        
        private void initfns() {
            this.dicti2a[1181328u] = new ee1Dec.C.MobUt.Tx8(this.funct00120690);
            this.dicti2a[1181344u] = new ee1Dec.C.MobUt.Tx8(this.funct001206a0);
            this.dicti2a[1181372u] = new ee1Dec.C.MobUt.Tx8(this.funct001206bc);
            this.dicti2a[1181380u] = new ee1Dec.C.MobUt.Tx8(this.funct001206c4);
            this.dicti2a[1213024u] = new ee1Dec.C.MobUt.Tx8(this.funct00128260);
            this.dicti2a[1213076u] = new ee1Dec.C.MobUt.Tx8(this.funct00128294);
            this.dicti2a[1213084u] = new ee1Dec.C.MobUt.Tx8(this.funct0012829c);
            this.dicti2a[1213108u] = new ee1Dec.C.MobUt.Tx8(this.funct001282b4);
            this.dicti2a[1213136u] = new ee1Dec.C.MobUt.Tx8(this.funct001282d0);
            this.dicti2a[1213164u] = new ee1Dec.C.MobUt.Tx8(this.funct001282ec);
            this.dicti2a[1213188u] = new ee1Dec.C.MobUt.Tx8(this.funct00128304);
            this.dicti2a[1213200u] = new ee1Dec.C.MobUt.Tx8(this.funct00128310);
            this.dicti2a[1213216u] = new ee1Dec.C.MobUt.Tx8(this.funct00128320);
            this.dicti2a[1213236u] = new ee1Dec.C.MobUt.Tx8(this.funct00128334);
            this.dicti2a[1213260u] = new ee1Dec.C.MobUt.Tx8(this.funct0012834c);
            this.dicti2a[1213296u] = new ee1Dec.C.MobUt.Tx8(this.funct00128370);
            this.dicti2a[1213312u] = new ee1Dec.C.MobUt.Tx8(this.funct00128380);
            this.dicti2a[1213336u] = new ee1Dec.C.MobUt.Tx8(this.funct00128398);
            this.dicti2a[1213352u] = new ee1Dec.C.MobUt.Tx8(this.funct001283a8);
            this.dicti2a[1213380u] = new ee1Dec.C.MobUt.Tx8(this.funct001283c4);
        }
    }
}
