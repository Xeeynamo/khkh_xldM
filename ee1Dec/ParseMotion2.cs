using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using khkh_xldM;
using System.Diagnostics;

namespace ee1Dec.P {
    public class ParseMotion2 {
        public void Parse(Stream bar1, string motionId, int off) {
            ReadBar.Barent[] entsbar1 = ReadBar.Explode(bar1, off, true);
            for (int ei = 0; ei < entsbar1.Length; ei++) {
                ReadBar.Barent entb1 = entsbar1[ei];
                if (0x11 == (entb1.k & 0xffff) && entb1.len != 0) {
                    ReadBar.Barent[] entsbar2 = ReadBar.Explode(bar1, entb1.off, true);
                    for (int ei2 = 0; ei2 < entsbar2.Length; ei2++) {
                        ReadBar.Barent ent = entsbar2[ei2];
                        if (ent.len >= 256) {
                            MemoryStream si = new MemoryStream(ent.bin, false);
                            BinaryReader br = new BinaryReader(si);
                            PosTblx px = new PosTblx(si, 0);
                            TemplDOM tl = new TemplDOM(motionId + "#" + ei + "." + entb1.id + "#" + ei2 + "." + ent.id, ei);
                            int tbloff = 0x90;
                            int mtoff = ent.off + tbloff; // mtoff=msetoff 

                            // 1) ent.off is already absolute-offset
                            // 2) offset in mset contents needs to add 0x90

                            int cntt9 = 0, cntt10 = 0, cntt12 = 0;
                            if (true) { // cntt9
                                si.Position = tbloff + px.vc0;
                                for (int i2 = 0; i2 < px.vc4; i2++) { // t2
                                    br.ReadByte();
                                    br.ReadByte();
                                    br.ReadByte();
                                    int tcx = br.ReadByte();
                                    int tx = br.ReadUInt16();
                                    cntt9 = Math.Max(cntt9, tx + tcx);
                                }

                                if (true) { // cntt10, cntt12
                                    si.Position = tbloff + px.vd0;
                                    for (int i9 = 0; i9 < cntt9; i9++) { // t9
                                        br.ReadUInt16();
                                        int ti10 = br.ReadUInt16(); cntt10 = Math.Max(cntt10, ti10 + 1);
                                        int ti12a = br.ReadUInt16(); cntt12 = Math.Max(cntt12, ti12a + 1);
                                        int ti12b = br.ReadUInt16(); cntt12 = Math.Max(cntt12, ti12b + 1);
                                    }
                                }
                            }
                            int cntt8 = 0;
                            if (true) {
                                si.Position = tbloff + px.ve0; // t3
                                for (int i3 = 0; i3 < px.ve4; i3++) {
                                    br.ReadUInt16();
                                    br.ReadUInt16();
                                    br.ReadUInt16();
                                    int ti8 = br.ReadInt16(); cntt8 = Math.Max(cntt8, ti8 + 1);
                                    br.ReadUInt16();
                                    br.ReadUInt16();
                                }
                            }

                            //add2DOM(tl, "t0", "dddddddddddddddddddddddddddddddddddd", mtoff - tbloff, tbloff, tbloff);
                            add2DOM(tl, "t1", "wwf", mtoff + px.vb4, px.vb8 * 8, 8);
                            add2DOM(tl, "t2", "bbbbw", mtoff + px.vc0, px.vc4 * 6, 6);
                            add2DOM(tl, "t2x", "bbbbw", mtoff + px.vc8, px.vcc * 6, 6);
                            add2DOM(tl, "t3", "bbwwwd", mtoff + px.ve0, px.ve4 * 12, 12);
                            add2DOM(tl, "t4", "ww", mtoff + px.vac, px.va2 * 4, 4);
                            add2DOM(tl, "t5", "wwwwqvvv", mtoff + px.va8, (px.va2 - px.va0) * 64, 64);
                            add2DOM(tl, "t6", "wwfww", mtoff + px.vf8, px.vfc * 12, 12);
                            add2DOM(tl, "t7", "wwww", mtoff + px.vf0, px.vf4 * 8, 8);
                            add2DOM(tl, "t8", "wwfffvv", mtoff + px.vec, cntt8 * 48, 48);
                            add2DOM(tl, "t9", "wwww", mtoff + px.vd0, cntt9 * 8, 8);
                            add2DOM(tl, "t10", "f", mtoff + px.vd8, cntt10 * 4, 4);
                            add2DOM(tl, "t11", "f", mtoff + px.vd4, px.vb0 * 4, 4);
                            add2DOM(tl, "t12", "f", mtoff + px.vdc, cntt12 * 4, 4);
                        }
                    }
                }
            }
        }

        public string Find(uint off, Stream sieeMem) {
            foreach (int k in dictDOM.Keys) {
                if (k <= off) {
                    DOM o = dictDOM[k];
                    if (off < o.off + o.totallen) {
                        uint ei = (off - o.off) / o.unitlen;
                        uint eo = (off - o.off) % o.unitlen;
                        UtStf stf = new UtStf();
                        string prefix = "";
                        string contents = "";
                        if (stf.Match(o, eo)) {
                            BinaryReader br = new BinaryReader(sieeMem);
                            sieeMem.Position = off;
                            switch (stf.varty) {
                                case UtStf.Varty.Byte: contents = br.ReadByte().ToString("X2"); break;
                                case UtStf.Varty.Word: contents = br.ReadUInt16().ToString("X4"); break;
                                case UtStf.Varty.DWord: contents = br.ReadUInt32().ToString("X8"); break;
                                case UtStf.Varty.QWord: contents = br.ReadUInt64().ToString("X16"); break;
                                case UtStf.Varty.Float: contents = br.ReadSingle().ToString(); break;
                            }
                            prefix = stf.f;
                        }
                        return o.motionId + "  " + o.id + "[" + ei + "]." + prefix + eo.ToString() + ((contents != null) ? " = " + contents : "");
                    }
                }
            }
            return null;
        }

        private void add2DOM(TemplDOM tl, string id, string stf, int absoff, int totallen, int unitlen) {
            if (totallen == 0) return;
            DOM o = new DOM();
            o.ei = tl.ei;
            o.motionId = tl.motionId;
            o.id = id;
            o.stf = stf;
            o.off = (uint)absoff;
            o.totallen = (uint)totallen;
            o.unitlen = (uint)unitlen;
            if (dictDOM.ContainsKey(absoff)) {
                DOM z = dictDOM[absoff];
                Debug.Assert(o.off == z.off && o.totallen == z.totallen && o.unitlen == z.unitlen);
            }
            dictDOM[absoff] = o;
        }

        SortedList<int, DOM> dictDOM = new SortedList<int, DOM>();

        class TemplDOM {
            public string motionId;
            public int ei;

            public TemplDOM(string motionId, int ei) {
                this.motionId = motionId;
                this.ei = ei;
            }
        }

        class DOM {
            public int ei = -1;
            public string id = "", motionId = "", stf = "";
            public uint off = 0, totallen = 0, unitlen = 0;
        }

        class UtStf {
            public string f = "";
            public int off = 0;
            public Varty varty = Varty.Invalid;

            public enum Varty {
                Invalid, Byte, Word, DWord, QWord, Float,
            }

            public bool Match(DOM o, uint offtest) {
                off = 0;
                foreach (char c in o.stf) {
                    switch (c) {
                        case 'b': // byte
                            if (off == offtest) {
                                f = "b";
                                varty = Varty.Byte;
                                return true;
                            }
                            else {
                                off += 1;
                                break;
                            }
                        case 'w': // intel cpu word
                            if (off == offtest) {
                                f = "w";
                                varty = Varty.Word;
                                return true;
                            }
                            else {
                                off += 2;
                                break;
                            }
                        case 'd': // intel cpu double word
                            if (off == offtest) {
                                f = "d";
                                varty = Varty.DWord;
                                return true;
                            }
                            else {
                                off += 4;
                                break;
                            }
                        case 'q': // intel cpu quad word
                            if (off == offtest) {
                                f = "q";
                                varty = Varty.QWord;
                                return true;
                            }
                            else {
                                off += 8;
                                break;
                            }
                        case 'f': // float
                            if (off == offtest) {
                                f = "f";
                                varty = Varty.Float;
                                return true;
                            }
                            else {
                                off += 4;
                                break;
                            }
                        case 'v': // vector (float x4)
                            if (off + 0 == offtest) {
                                f = "vx";
                                varty = Varty.Float;
                                return true;
                            }
                            else if (off + 4 == offtest) {
                                f = "vy";
                                varty = Varty.Float;
                                return true;
                            }
                            else if (off + 8 == offtest) {
                                f = "vz";
                                varty = Varty.Float;
                                return true;
                            }
                            else if (off + 12 == offtest) {
                                f = "vw";
                                varty = Varty.Float;
                                return true;
                            }
                            else {
                                off += 16;
                                break;
                            }
                        default: throw new NotSupportedException(c.ToString());
                    }
                }
                return false;
            }
        }

        class PosTblx {
            public int va0;
            public int va2; // cnt t4
            public int va8; // off t5 (each 64 bytes)  { cnt_t5 = va2 -va0 }
            public int vac; // off t4 (each 4 bytes)
            public int vb0; // cnt t11
            public int vb4; // off t1 (each 8 bytes)
            public int vb8; // cnt t1
            public int vc0; // off t2 (each 6 bytes)
            public int vc4; // cnt t2
            public int vc8; // off t2` (each 6 bytes)
            public int vcc; // cnt t2`
            public int vd0; // off t9 (each 8 bytes)
            public int vd4; // off t11 (each 4 bytes)
            public int vd8; // off t10 (each 4 bytes)
            public int vdc; // off t12 (each 4 bytes)
            public int ve0; // off t3 (each 12 bytes)
            public int ve4; // cnt t3
            public int ve8;
            public int vec; // off t8 (each 48 bytes)  { cnt_t8 = cnt_t2` }
            public int vf0; // off t7 (each 8 bytes)
            public int vf4; // cnt t7
            public int vf8; // off t6 (each 12 bytes)
            public int vfc; // cnt t6

            public PosTblx(Stream si, int off) {
                BinaryReader br = new BinaryReader(si);

                // ORG��
                si.Position = off + 0xA0;
                va0 = br.ReadUInt16();
                va2 = br.ReadUInt16(); // cnt t4
                si.Position = off + 0xA8;
                va8 = br.ReadInt32(); // off t5 (each 64 bytes)  { cnt_t5 = va2 -va0 }
                vac = br.ReadInt32(); // off t4 (each 4 bytes)

                si.Position = off + 0xB0;
                vb0 = br.ReadInt32(); // cnt t11
                vb4 = br.ReadInt32(); // off t1 (each 8 bytes)
                vb8 = br.ReadInt32(); // cnt t1
                si.Position = off + 0xC0;
                vc0 = br.ReadInt32(); // off t2 (each 6 bytes)
                vc4 = br.ReadInt32(); // cnt t2
                vc8 = br.ReadInt32(); // off t2` (each 6 bytes)
                vcc = br.ReadInt32(); // cnt t2`
                si.Position = off + 0xD0;
                vd0 = br.ReadInt32(); // off t9 (each 8 bytes)
                vd4 = br.ReadInt32(); // off t11 (each 4 bytes)
                vd8 = br.ReadInt32(); // off t10 (each 4 bytes)
                vdc = br.ReadInt32(); // off t12 (each 4 bytes)
                si.Position = off + 0xE0;
                ve0 = br.ReadInt32(); // off t3 (each 12 bytes)
                ve4 = br.ReadInt32(); // cnt t3
                ve8 = br.ReadInt32();
                vec = br.ReadInt32(); // off t8 (each 48 bytes)  { cnt_t8 = cnt_t2` }
                si.Position = off + 0xF0;
                vf0 = br.ReadInt32(); // off t7 (each 8 bytes)
                vf4 = br.ReadInt32(); // cnt t7
                vf8 = br.ReadInt32(); // off t6 (each 12 bytes)
                vfc = br.ReadInt32(); // cnt t6
                // ��ORG
            }
        }

        internal void DEBUG() {
            StringBuilder s = new StringBuilder();
            foreach (DOM o in dictDOM.Values) {
                switch (o.id) {
                    case "t10":
                    case "t11":
                    case "t12":
                        //s.AppendFormat("\t|| (0x{0:x6} <= mem && mem < 0x{1:x6}) // {2}\r\n", o.off, o.off + o.totallen, o.motionId + " " + o.id);
                        s.AppendFormat("\t|| ((mem -0x{0:x6}U) < 0x{1:x6}U) // {2}\r\n", o.off, o.totallen, o.motionId + " " + o.id);
                        break;
                }
            }
            System.Windows.Forms.Clipboard.SetText(s.ToString());
            System.Windows.Forms.MessageBox.Show("Copied to Clipboard.");
        }
    }
}
