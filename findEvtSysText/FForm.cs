using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using findEvtSysText.Properties;

namespace findEvtSysText {
    public partial class FForm : Form {
        public FForm() {
            InitializeComponent();
        }

        class StrTbl {
            public StrTbl(String rows, int cx) {
                int y = 0;
                foreach (String row in rows.Replace("\r\n", "\n").Split('\n')) {
                    String[] cols = row.Split('\t');
                    if (cols.Length == cx) {
                        for (int x = 0; x < cols.Length; x++) {
                            if (cols[x].Length == 2) {
                                cols[x + 1] = "" + cols[x][1];
                                cols[x + 0] = "" + cols[x][0];
                            }

                            dict[cols[x]] = cx * y + x;

                            dictRev[cx * y + x] = cols[x];
                        }
                    }
                    y++;
                }
            }

            SortedDictionary<string, int> dict = new SortedDictionary<string, int>();

            public int[] GetPattern(String s) {
                try {
                    int[] ali = new int[s.Length];
                    for (int x = 0; x < s.Length; x++) {
                        ali[x] = dict["" + s[x]];
                    }
                    return ali;
                }
                catch (KeyNotFoundException) {
                    return null;
                }
            }

            SortedDictionary<int, string> dictRev = new SortedDictionary<int, string>();

            public string[] GetTable() {
                string[] al = new string[256];
                for (int x = 32; x < 256; x++) {
                    String s = dictRev.ContainsKey(x - 32) ? dictRev[x - 32] : "��";
                    if (s == "") s = string.Format("[{0:x2}]", x);
                    al[x] = s;
                }
                return al;
            }
            public string[] GetTable(int map) {
                string[] al = new string[256];
                for (int x = 0; x < 256; x++) {
                    int k = x;
                    switch (map) {
                        case 0x19: k += 224; break;
                        case 0x1a: k += 480; break;
                        case 0x1b: k += 736; break;
                        case 0x1c: k += 992; break;
                        case 0x1d: k += 1248; break;
                        case 0x1e: k += 1504; break;
                        case 0x1f: k += 1504 + 256; break;
                    }
                    String s = dictRev.ContainsKey(k) ? dictRev[k] : "��";
                    if (s == "") s = string.Format("[{0:x2} {1:x2}]", map, x);
                    al[x] = s;
                }
                return al;
            }
        }

        class FmtUt {
            public static string Str(int[] ali) {
                String s = "";
                foreach (int i in ali) s += i + " ";
                return s;
            }
        }

        StrTbl evt, sys;

        private void bSearch_Click(object sender, EventArgs e) {
            evt = new StrTbl(tbEVT.Text, 21);
            sys = new StrTbl(tbSYS.Text, 28);

            List<int[]> alai = new List<int[]>();
            alai.Add(evt.GetPattern(cbIn.Text));
            alai.Add(sys.GetPattern(cbIn.Text));

            levt.Text = FmtUt.Str(alai[0] ?? new int[0]);
            lsys.Text = FmtUt.Str(alai[1] ?? new int[0]);

            int cnt = 0;

            StringWriter wr = new StringWriter();
            foreach (String fp in Directory.GetFiles(tbDirin.Text, "*.*", SearchOption.AllDirectories)) {
                using (FileStream fs = File.OpenRead(fp)) {
                    for (int x = 0; x < alai.Count; x++) {
                        if (alai[x] != null) {
                            fs.Position = 0;
                            {
                                List<int> alr = FUt.Find8(fs, alai[x]);
                                if (alr.Count != 0) {
                                    foreach (int pos in alr) {
                                        wr.WriteLine(fp + " " + x + " b " + pos);

                                        cnt++; if (cnt > 100) throw new ArgumentOutOfRangeException("Too much matches!");
                                    }
                                }
                            }
                            {
                                List<int> alr = FUt.Find16(fs, alai[x]);
                                if (alr.Count != 0) {
                                    foreach (int pos in alr) {
                                        wr.WriteLine(fp + " " + x + " w " + pos);

                                        cnt++; if (cnt > 100) throw new ArgumentOutOfRangeException("Too much matches!");
                                    }
                                }
                            }
                        }
                    }
                }
            }

            tbRes.Text = wr.ToString();
        }

        class FUt {
            public static List<int> Find8(Stream si, int[] ali) {
                List<int> alFound = new List<int>();

                int cb1 = 4096;
                int cb2 = cb1 + cb1;
                int cb2Mask = cb2 - 1;
                byte[] buff = new byte[cb2];
                int left = 0;
                int pos = 0;
                bool lower = true;
                int cbPat = ali.Length;
                while (true) {
                    if (left < cbPat) {
                        left += si.Read(buff, lower ? 0 : cb1, cb1);
                        if (left < cbPat)
                            break;
                        lower = !lower;
                    }
                    while (left >= cbPat) {
                        int i0 = ali[0];
                        byte b0 = buff[pos & cb2Mask];
                        int x = 1;
                        for (; x < cbPat; x++) {
                            if (((ali[x] - i0) & 255) != ((buff[(pos + x) & cb2Mask] - b0) & 255))
                                break;
                        }
                        if (x == cbPat) {
                            alFound.Add(pos);
                        }
                        ++pos;
                        --left;
                    }
                }

                return alFound;
            }

            public static List<int> Find16(Stream si, int[] ali) {
                List<int> alFound = new List<int>();

                int cb1 = 4096;
                int cb2 = cb1 + cb1;
                int cb2Mask = cb2 - 1;
                byte[] buff = new byte[cb2];
                int left = 0;
                int pos = 0;
                bool lower = true;
                int cbPat = ali.Length * 2;
                int cntPat = ali.Length;
                while (true) {
                    if (left < cbPat) {
                        left += si.Read(buff, lower ? 0 : cb1, cb1);
                        if (left < cbPat)
                            break;
                        lower = !lower;
                    }
                    while (left >= cbPat) {
                        int i0 = ali[0];
                        int w0 = (buff[pos & cb2Mask]) | (buff[(pos + 1) & cb2Mask] << 8);
                        int x = 1;
                        for (; x < cntPat; x++) {
                            if (((ali[x] - i0) & 65535) != ((((buff[(pos + x + x + 0) & cb2Mask]) | (buff[(pos + x + x + 1) & cb2Mask] << 8)) - w0) & 65535))
                                break;
                        }
                        if (x == cbPat) {
                            alFound.Add(pos);
                        }
                        ++pos;
                        --left;
                    }
                }

                return alFound;
            }
        }

        private void FForm_Load(object sender, EventArgs e) {
            //List<int> alr = FUt.Find8("", alai[x]);
        }

        private void bRead_Click(object sender, EventArgs e) {
            byte[] bin = Resources.es_02;

            evt = new StrTbl(tbEVT.Text, 21);
            String[] alt = evt.GetTable();
            String[] alt19 = evt.GetTable(0x19);
            String[] alt1a = evt.GetTable(0x1a);
            String[] alt1b = evt.GetTable(0x1b);
            String[] alt1c = evt.GetTable(0x1c);
            String[] alt1d = evt.GetTable(0x1d);
            String[] alt1e = evt.GetTable(0x1e);
            String[] alt1f = evt.GetTable(0x1f);

            MemoryStream si = new MemoryStream(bin, false);
            BinaryReader br = new BinaryReader(si);

            if (br.ReadInt32() != 1) throw new NotSupportedException("!01");
            int cnt = br.ReadInt32();

            StringWriter wr = new StringWriter();
            for (int x = 0; x < cnt; x++) {
                si.Position = 8 + 8 * x;
                int off1 = br.ReadInt32();
                int off2 = br.ReadInt32();

                String s = "";
                si.Position = off2;
                while (true) {
                    byte b = br.ReadByte();
                    if (b == 0)
                        break;
                    if (32 <= b) {
                        s += alt[b];
                    }
                    else if (b == 0x19) {
                        b = br.ReadByte();
                        s += alt19[b];
                    }
                    else if (b == 0x1a) {
                        b = br.ReadByte();
                        s += alt1a[b];
                    }
                    else if (b == 0x1b) {
                        b = br.ReadByte();
                        s += alt1b[b];
                    }
                    else if (b == 0x1c) {
                        b = br.ReadByte();
                        s += alt1c[b];
                    }
                    else if (b == 0x1d) {
                        b = br.ReadByte();
                        s += alt1d[b];
                    }
                    else if (b == 0x1e) {
                        b = br.ReadByte();
                        s += alt1e[b];
                    }
                    else if (b == 0x1f) {
                        b = br.ReadByte();
                        s += alt1f[b];
                    }
                    else if (b == 0x14 || b == 0x09 || b == 0x13 || b == 0x0f) {
                        byte b1 = br.ReadByte();
                        s += string.Format("[{0:x2} {1:x2}]", b, b1);
                    }
                    else if (b == 4) {
                        s += "<�ΐF>";
                    }
                    else if (b == 3) {
                        s += "<�ʏ�>";
                    }
                    else if (b == 2) {
                        s += "\r\n";
                    }
                    else if (b == 1) {
                        s += "�@";
                    }
                    else if (b == 16) {
                        s += "\r\n�Y\r\n";
                    }
                    else {
                        s += string.Format("<{0:x2}>", b);
                    }
                    //else throw new NotSupportedException("charcode: " + b.ToString("x2"));
                }

                wr.WriteLine("Str{0:000} Num {1:x8} Off {2:x8}", x, off1, off2);
                wr.WriteLine(s);
                wr.WriteLine();
            }

            tbOuta.Text = wr.ToString();
        }

    }
}