using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using hex04BinTrack;
using System.IO;
using System.Diagnostics;
using SlimDX.Direct3D9;
using SlimDX;
using khiiMapv.CollTest;
using khiiMapv.Put;

namespace khiiMapv {
    public partial class Visf : Form {
        public Visf() {
            InitializeComponent();
        }
        public Visf(List<DC> aldc, CollReader coll) {
            this.aldc = aldc;
            this.coll = coll;
            InitializeComponent();
        }

        List<DC> aldc = null;
        CollReader coll;

        Device device;

        class CI {
            public uint[] ali;
            public int texi, vifi;
        }

        List<CI> alci = new List<CI>();
        List<CI[]> alalci = new List<CI[]>();
        List<Texture> altex = new List<Texture>();

        Direct3D p3D;

        class LMap {
            public byte[] al = new byte[256];

            public LMap() {
                for (int x = 0; x < 256; x++) {
                    al[x] = (byte)Math.Min(255, 2 * x);
                }
            }
        }

        LMap lm = new LMap();

        PresentParameters PP {
            get {
                PresentParameters pp = new PresentParameters();
                pp.Windowed = true;
                pp.SwapEffect = SwapEffect.Discard;
                pp.AutoDepthStencilFormat = Format.D24X8;
                pp.EnableAutoDepthStencil = true;
                pp.BackBufferHeight = 1024;
                pp.BackBufferWidth = 1024;
                return pp;
            }
        }

        private void p1_Load(object sender, EventArgs e) {
            p3D = new Direct3D();
            alDeleter.Add(p3D);
            device = new Device(p3D, 0, DeviceType.Hardware, p1.Handle, CreateFlags.HardwareVertexProcessing, PP);
            alDeleter.Add(device);

            device.SetRenderState(RenderState.Lighting, false);
            device.SetRenderState(RenderState.ZEnable, true);

            device.SetRenderState(RenderState.AlphaTestEnable, true);
            device.SetRenderState(RenderState.AlphaRef, 2);
            device.SetRenderState<Compare>(RenderState.AlphaFunc, Compare.GreaterEqual);

            device.SetRenderState(RenderState.AlphaBlendEnable, true);
            device.SetRenderState(RenderState.SourceBlend, Blend.SourceAlpha);
            device.SetRenderState(RenderState.SourceBlendAlpha, Blend.SourceAlpha);
            device.SetRenderState(RenderState.DestinationBlend, Blend.InverseSourceAlpha);
            device.SetRenderState(RenderState.DestinationBlendAlpha, Blend.InverseSourceAlpha);

            device.SetRenderState<Cull>(RenderState.CullMode, Cull.Counterclockwise);

            device.SetRenderState(RenderState.FogColor, p1.BackColor.ToArgb());
            device.SetRenderState(RenderState.FogStart, 5.0f);
            device.SetRenderState(RenderState.FogEnd, 30000.0f);
            device.SetRenderState(RenderState.FogDensity, 0.0001f);
            device.SetRenderState<FogMode>(RenderState.FogVertexMode, FogMode.Exponential);

            p1.MouseWheel += new MouseEventHandler(p1_MouseWheel);

            List<CustomVertex.PositionColoredTextured> alv3 = new List<CustomVertex.PositionColoredTextured>();

            foreach (DC dc in aldc) {
                ToolStripButton tsiIfRender = new ToolStripButton("Show " + dc.name);
                tsiIfRender.DisplayStyle = ToolStripItemDisplayStyle.Text;
                tsiIfRender.CheckOnClick = true;
                tsiIfRender.Tag = dc.dcId;
                tsiIfRender.Checked = true;
                tsiIfRender.CheckedChanged += new EventHandler(tsiIfRender_CheckedChanged);

                toolStrip1.Items.Insert(toolStrip1.Items.IndexOf(tsbShowColl), tsiIfRender);
            }

            alci.Clear();
            altex.Clear();
            int[] alrecent = new int[4];
            int recenti = 0;
            foreach (DC dc in aldc) {
                int texi = altex.Count;
                foreach (Bitmap pic in dc.o7.pics) {
                    MemoryStream os = new MemoryStream();
                    pic.Save(os, System.Drawing.Imaging.ImageFormat.Png);
                    os.Position = 0;
                    Texture t;
                    altex.Add(t = Texture.FromStream(device, os));
                    alDeleter.Add(t);
                }
                if (dc.o4Mdlx != null) {
                    foreach (KeyValuePair<int, Parse4Mdlx.Model> kv in dc.o4Mdlx.dictModel) {
                        CI ci = new CI();
                        int bi = alv3.Count;

                        Parse4Mdlx.Model model = kv.Value;
                        alv3.AddRange(model.alv);

                        List<uint> altris = new List<uint>();
                        for (int x = 0; x < model.alv.Count; x++) {
                            altris.Add((uint)(bi + x));
                        }

                        ci.ali = altris.ToArray();
                        ci.texi = texi + kv.Key;
                        ci.vifi = 0;
                        alci.Add(ci);
                    }
                }
                else if (dc.o4Map != null) {
                    for (int tari = 0; tari < dc.o4Map.alvifpkt.Count; tari++) {
                        {
                            Vifpli vli = dc.o4Map.alvifpkt[tari];
                            byte[] vifpkt = vli.vifpkt;
                            VU1Mem memo = new VU1Mem();
                            ParseVIF1 pv1 = new ParseVIF1(memo);
                            pv1.Parse(new MemoryStream(vifpkt, false), 0x00);
                            foreach (byte[] ram in pv1.almsmem) {
                                CI ci = new CI();

                                MemoryStream si = new MemoryStream(ram, false);
                                BinaryReader br = new BinaryReader(si);

                                br.ReadInt32();
                                br.ReadInt32();
                                br.ReadInt32();
                                br.ReadInt32();
                                int v10 = br.ReadInt32(); // v10: cnt tex&vi&flg verts
                                int v14 = br.ReadInt32(); // v14: off tex&vi&flg verts
                                br.ReadInt32();
                                br.ReadInt32();
                                int v20 = br.ReadInt32(); // v20: cnt clr verts
                                int v24 = br.ReadInt32(); // v24: off clr verts
                                br.ReadInt32();
                                br.ReadInt32();
                                int v30 = br.ReadInt32(); // v30: cnt pos vert
                                int v34 = br.ReadInt32(); // v34: off pos vert

                                List<uint> altris = new List<uint>();
                                int bi = alv3.Count;
                                for (int i = 0; i < v10; i++) {
                                    si.Position = 16 * (v14 + i);
                                    int tx = br.ReadInt16(); br.ReadInt16();
                                    int ty = br.ReadInt16(); br.ReadInt16();
                                    int vi = br.ReadInt16(); br.ReadInt16();
                                    int fl = br.ReadInt16(); br.ReadInt16();
                                    si.Position = 16 * (v34 + vi);
                                    Vector3 v3;
                                    v3.X = -br.ReadSingle();
                                    v3.Y = +br.ReadSingle();
                                    v3.Z = +br.ReadSingle();

                                    si.Position = 16 * (v24 + i);
                                    int fR = (byte)br.ReadUInt32();
                                    int fG = (byte)br.ReadUInt32();
                                    int fB = (byte)br.ReadUInt32();
                                    int fA = (byte)br.ReadUInt32();

                                    if (v24 == 0) {
                                        fR = 255;
                                        fG = 255;
                                        fB = 255;
                                        fA = 255;
                                    }

                                    alrecent[recenti & 3] = bi + i;
                                    recenti++;

                                    //Debug.WriteLine("# " + fl.ToString("x2"));

                                    if (fl == 0x00) {
                                    }
                                    else if (fl == 0x10) {
                                    }
                                    else if (fl == 0x20) {
                                        altris.Add(Convert.ToUInt32(alrecent[(recenti - 1) & 3]));
                                        altris.Add(Convert.ToUInt32(alrecent[(recenti - 2) & 3]));
                                        altris.Add(Convert.ToUInt32(alrecent[(recenti - 3) & 3]));
                                    }
                                    else if (fl == 0x30) {
                                        altris.Add(Convert.ToUInt32(alrecent[(recenti - 1) & 3]));
                                        altris.Add(Convert.ToUInt32(alrecent[(recenti - 3) & 3]));
                                        altris.Add(Convert.ToUInt32(alrecent[(recenti - 2) & 3]));
                                    }

                                    Color clr = Color.FromArgb(lm.al[fA], lm.al[fR], lm.al[fG], lm.al[fB]);

                                    CustomVertex.PositionColoredTextured cv = new CustomVertex.PositionColoredTextured(
                                        v3,
                                        clr.ToArgb(),
                                        +tx / 16.0f / 256.0f,
                                        +ty / 16.0f / 256.0f
                                        );
                                    alv3.Add(cv);
                                }

                                ci.ali = altris.ToArray();
                                ci.texi = texi + vli.texi;
                                ci.vifi = tari;
                                alci.Add(ci);
                            }
                        }
                    }
                }
                alalci.Add(alci.ToArray());
                alci.Clear();
            }

            if (alalci.Count != 0) {
                alci.Clear();
                alci.AddRange(alalci[0]);
            }

            if (alv3.Count == 0) {
                alv3.Add(new CustomVertex.PositionColoredTextured());
            }

            {
                vb = new VertexBuffer(
                    device,
                    (cntVerts = alv3.Count) * CustomVertex.PositionColoredTextured.Size,
                    Usage.Points,
                    CustomVertex.PositionColoredTextured.Format,
                    Pool.Managed
                    );
                alDeleter.Add(vb);
                DataStream gs = vb.Lock(0, 0, LockFlags.None);
                try {
                    foreach (CustomVertex.PositionColoredTextured v3 in alv3) {
                        gs.Write(v3);
                    }
                }
                finally {
                    vb.Unlock();
                }
            }

            lCntVert.Text = cntVerts.ToString("#,##0");

            int cntIdx = 0;

            alib.Clear();
            int ialci = 0;
            foreach (CI[] localalci in alalci) {
                foreach (CI ci in localalci) {
                    if (ci.ali.Length != 0) {
                        IndexBuffer ib = new IndexBuffer(
                            device,
                            4 * ci.ali.Length,
                            Usage.None,
                            Pool.Managed,
                            false
                            );
                        cntIdx += ci.ali.Length;
                        alDeleter.Add(ib);
                        DataStream gs = ib.Lock(0, 0, LockFlags.None);
                        try {
                            foreach (uint i in ci.ali) {
                                gs.Write(i);
                            }
                        }
                        finally {
                            ib.Unlock();
                        }
                        RIB rib = new RIB();
                        rib.ib = ib;
                        rib.cnt = ci.ali.Length;
                        rib.texi = ci.texi;
                        rib.vifi = ci.vifi;
                        rib.name = aldc[ialci].name;
                        rib.dcId = aldc[ialci].dcId;
                        alib.Add(rib);
                    }
                    else {
                        RIB rib = new RIB();
                        rib.ib = null;
                        rib.cnt = 0;
                        rib.texi = ci.texi;
                        rib.vifi = ci.vifi;
                        rib.name = aldc[ialci].name;
                        rib.dcId = aldc[ialci].dcId;
                        alib.Add(rib);
                    }
                }
                ialci++;
            }

            lCntTris.Text = (cntIdx / 3).ToString("#,##0");

            {
                foreach (Co2 o1 in coll.alCo2) {
                    alpf.Add(putb.Add(o1));
                }
                if (putb.alv.Count != 0)
                    pvi = new Putvi(putb, device);
            }

            Console.Write("");
        }

        void tsiIfRender_CheckedChanged(object sender, EventArgs e) {
            ToolStripButton tsi = (ToolStripButton)sender;
            bool f = tsi.Checked;
            Guid dcId = (Guid)tsi.Tag;

            foreach (RIB rib in alib) {
                if (rib.dcId.Equals(dcId)) rib.render = f;
            }

            p1.Invalidate();
        }

        Putbox putb = new Putbox();
        Putvi pvi = null;
        List<Putfragment> alpf = new List<Putfragment>();

        List<ComObject> alDeleter = new List<ComObject>();

        void p1_MouseWheel(object sender, MouseEventArgs e) {
            fov.Value = (Decimal)Math.Max(
                Convert.ToSingle(fov.Minimum),
                Math.Min(
                    Convert.ToSingle(fov.Maximum),
                    Math.Max(
                        1.0f,
                        Convert.ToSingle(fov.Value) + e.Delta / 200.0f
                        )
                    )
                );
            p1.Invalidate();
        }

        class RIB {
            public IndexBuffer ib;
            public int cnt, texi, vifi;
            public bool render = true;
            public string name = "";
            public Guid dcId;
        }

        List<RIB> alib = new List<RIB>();
        VertexBuffer vb;
        int cntVerts = 0;

        Vector3 CameraEye {
            get {
                return new Vector3(Convert.ToSingle(eyeX.Value), Convert.ToSingle(eyeY.Value), Convert.ToSingle(eyeZ.Value));
            }
            set {
                eyeX.Value = Math.Max(eyeX.Minimum, Math.Min(eyeX.Maximum, (decimal)value.X));
                eyeY.Value = Math.Max(eyeY.Minimum, Math.Min(eyeY.Maximum, (decimal)value.Y));
                eyeZ.Value = Math.Max(eyeZ.Minimum, Math.Min(eyeZ.Maximum, (decimal)value.Z));
            }
        }

        Vector3 Target {
            get {
                return Vector3.TransformCoordinate(
                    Vector3.UnitX,
                    Matrix.RotationYawPitchRoll(
                        Convert.ToSingle(yaw.Value) / 180.0f * 3.14159f,
                        Convert.ToSingle(pitch.Value) / 180.0f * 3.14159f,
                        Convert.ToSingle(roll.Value) / 180.0f * 3.14159f
                        )
                    );
            }
        }

        Vector3 CameraUp {
            get {
                return Vector3.TransformCoordinate(
                    Vector3.UnitY,
                    Matrix.RotationYawPitchRoll(
                        0,
                        Convert.ToSingle(pitch.Value) / 180.0f * 3.14159f,
                        0
                    )
                );
            }
        }

        Vector3 TargetX {
            get {
                return Vector3.TransformCoordinate(
                    Vector3.UnitX,
                    Matrix.RotationYawPitchRoll(
                        Convert.ToSingle(yaw.Value) / 180.0f * 3.14159f,
                        0,
                        0
                        )
                    );
            }
        }

        private void p1_Paint(object sender, PaintEventArgs e) {
            Size size = p1.ClientSize;
            float aspect = (size.Height != 0) ? size.Width / (float)size.Height : 0;

            // http://reply.mydns.jp/reply/2009/07/visual-basic2008-slimdx-game-no4.html

            device.SetTransform(TransformState.World, Matrix.Identity);
            device.SetTransform(TransformState.View, Matrix.LookAtLH(
                CameraEye,
                CameraEye + Target,
                CameraUp
                ));
            device.SetTransform(TransformState.Projection, Matrix.PerspectiveFovLH(
                Convert.ToSingle(fov.Value) / 180.0f * 3.14159f,
                aspect,
                Convert.ToSingle(50),
                Convert.ToSingle(5000000)
                ));

            device.Clear(ClearFlags.Target | ClearFlags.ZBuffer, p1.BackColor, 1, 0);
            device.BeginScene();

            {
                device.SetTextureStageState(0, TextureStage.ColorOperation, (cbVertexColor.Checked) ? TextureOperation.Modulate : TextureOperation.SelectArg1);
                device.SetTextureStageState(0, TextureStage.ColorArg1, TextureArgument.Texture);
                device.SetTextureStageState(0, TextureStage.ColorArg2, TextureArgument.Diffuse);

                device.SetTextureStageState(0, TextureStage.AlphaOperation, TextureOperation.Modulate);
                device.SetTextureStageState(0, TextureStage.AlphaArg1, TextureArgument.Texture);
                device.SetTextureStageState(0, TextureStage.AlphaArg2, TextureArgument.Diffuse);

                device.SetRenderState(RenderState.FogEnable, cbFog.Checked);

                if (vb != null) {
                    device.SetStreamSource(0, vb, 0, CustomVertex.PositionColoredTextured.Size);
                    device.VertexFormat = CustomVertex.PositionColoredTextured.Format;
                    foreach (RIB rib in alib) {
                        if (rib.ib != null && rib.render) {
                            device.SetRenderState(RenderState.FogEnable, cbFog.Checked && rib.name.Equals("MAP"));

                            device.Indices = rib.ib;
                            device.SetTexture(0, altex[rib.texi & 65535]);
                            device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, cntVerts, 0, rib.cnt / 3);
                        }
                    }
                }
            }

            if (tsbShowColl.Checked) {
                vut.Clear();
                foreach (Co2 co2 in coll.alCo2) {
                    for (int x = co2.Co3frm; x < co2.Co3to; x++) {
                        Co3 co3 = coll.alCo3[x];
                        int clr = Color.Yellow.ToArgb();
                        if (0 <= co3.vi0 && 0 <= co3.vi1 && 0 <= co3.vi2) {
                            {
                                vut.AddTri(coll.alCo4[co3.vi0], coll.alCo4[co3.vi2], coll.alCo4[co3.vi1], clr);
                            }
                            if (0 <= co3.vi3) {
                                vut.AddTri(coll.alCo4[co3.vi3], coll.alCo4[co3.vi2], coll.alCo4[co3.vi0], clr);
                            }
                        }
                    }
                }

                if (vut.alv.Count != 0) {
                    device.SetTextureStageState(0, TextureStage.ColorOperation, TextureOperation.SelectArg1);
                    device.SetTextureStageState(0, TextureStage.ColorArg1, TextureArgument.Diffuse);

                    device.SetRenderState(RenderState.AlphaBlendEnable, false);
                    device.SetRenderState(RenderState.Lighting, true);
                    device.SetRenderState(RenderState.AlphaTestEnable, false);
                    device.SetRenderState(RenderState.ShadeMode, ShadeMode.Gouraud);
                    device.SetRenderState(RenderState.NormalizeNormals, true);

                    device.EnableLight(0, true);
                    Light l = device.GetLight(0);
                    l.Direction = Target;
                    l.Diffuse = new Color4(-1);
                    device.SetLight(0, l);

                    Material M = device.Material;
                    device.Material = M;

                    Matrix Mw = Matrix.Scaling(-1, -1, -1);
                    device.SetTransform(TransformState.World, Mw);

                    device.VertexFormat = CustomVertex.PositionNormalColored.Format;
                    device.DrawUserPrimitives<CustomVertex.PositionNormalColored>(PrimitiveType.TriangleList, vut.alv.Count / 3, vut.alvtmp = vut.alv.ToArray());

                    device.SetRenderState(RenderState.AlphaBlendEnable, true);
                    device.SetRenderState(RenderState.Lighting, false);
                    device.SetRenderState(RenderState.AlphaTestEnable, true);
                }
            }

            device.EndScene();
            device.Present();
        }

        VUt vut = new VUt();

        class VUt {
            public List<CustomVertex.PositionNormalColored> alv = new List<CustomVertex.PositionNormalColored>();

            public CustomVertex.PositionNormalColored[] alvtmp = null;

            public void AddTri(Vector4 w0, Vector4 w1, Vector4 w2, int clr) {
                Vector3 v0 = new Vector3(w0.X, w0.Y, w0.Z);
                Vector3 v1 = new Vector3(w1.X, w1.Y, w1.Z);
                Vector3 v2 = new Vector3(w2.X, w2.Y, w2.Z);

                Vector3 nv = Vector3.Cross(v1 - v0, v1 - v2);

                alv.Add(new CustomVertex.PositionNormalColored(v0, nv, clr));
                alv.Add(new CustomVertex.PositionNormalColored(v1, nv, clr));
                alv.Add(new CustomVertex.PositionNormalColored(v2, nv, clr));
            }

            public void Clear() {
                alv.Clear();
                alvtmp = null;
            }
        }

        Point curs = Point.Empty, firstcur = Point.Empty;

        private void p1_MouseDown(object sender, MouseEventArgs e) {
            if (e.Button == MouseButtons.Left || e.Button == MouseButtons.Right) {
                firstcur = p1.PointToScreen(curs = new Point(e.X, e.Y));
            }
        }

        private void p1_MouseMove(object sender, MouseEventArgs e) {
            if (curs != Point.Empty) {
                int dX = e.X - curs.X;
                int dY = e.Y - curs.Y;
                if (dX != 0 || dY != 0) {
                    if (0 != (e.Button & MouseButtons.Left)) {
                        yaw.Value += (decimal)(dX / 3.0f);
                        roll.Value = (decimal)(Math.Max(-89, Math.Min(+89, (Convert.ToSingle(roll.Value) % 360.0f) - dY / 3.0f)));
                        //roll.Value += (decimal)(-dY / 3.0f);

                        Cursor.Position = firstcur;
                        p1.Invalidate();
                    }
                    else if (0 != (e.Button & MouseButtons.Right)) {
                        yaw.Value += (decimal)(dX / 3.0f);
                        CameraEye += Target * -dY;

                        Cursor.Position = firstcur;
                        p1.Invalidate();
                    }
                }
                return;
            }
        }

        private void p1_MouseUp(object sender, MouseEventArgs e) {
            if (e.Button == MouseButtons.Left) {
                curs = Point.Empty;
            }
        }

        private void Visf_Load(object sender, EventArgs e) {
            SetStyle(ControlStyles.ResizeRedraw, true);
        }

        private void tsbExpBlenderpy_Click(object sender, EventArgs e) {
            Directory.CreateDirectory("bpyexp");

            DataStream gs = vb.Lock(0, 0, LockFlags.ReadOnly);
            try {
                CustomVertex.PositionColoredTextured[] verts = new CustomVertex.PositionColoredTextured[cntVerts];
                for (int x = 0; x < cntVerts; x++) {
                    verts[x] = (CustomVertex.PositionColoredTextured)gs.Read<CustomVertex.PositionColoredTextured>();
                }

                int basepici = 0;
                for (int w = 0; w < alalci.Count; w++) {
                    Mkbpy maker = new Mkbpy();
                    maker.StartTex();

                    String basedir = "bpyexp\\" + aldc[w].name;
                    Directory.CreateDirectory(basedir);

                    int pici = 0;
                    {
                        DC dc = aldc[w];
                        foreach (Bitmap pic in dc.o7.pics) {
                            String fppic = String.Format("t{0:000}.png", pici);
                            pic.Save(basedir + "\\" + fppic, System.Drawing.Imaging.ImageFormat.Png);
                            maker.AddTex(Path.GetFullPath(basedir + "\\" + fppic)
                                , String.Format("Tex{0:000}", pici)
                                , String.Format("Mat{0:000}", pici)
                                );
                            pici++;
                        }
                    }
                    maker.EndTex();
                    foreach (CI ci in alalci[w]) {
                        maker.StartMesh();
                        for (int trii = 0; trii < ci.ali.Length / 3; trii++) {
                            maker.AddV(verts[ci.ali[trii * 3 + 0]]);
                            maker.AddV(verts[ci.ali[trii * 3 + 2]]);
                            maker.AddV(verts[ci.ali[trii * 3 + 1]]);
                            maker.AddColorVtx(new Color[] {
                            Color.FromArgb(verts[ci.ali[trii * 3 + 0]].Color),
                            Color.FromArgb(verts[ci.ali[trii * 3 + 2]].Color),
                            Color.FromArgb(verts[ci.ali[trii * 3 + 1]].Color),
                            });
                            maker.AddTuv(
                                (ci.texi & 65535) - basepici,
                                verts[ci.ali[trii * 3 + 0]].Tu, 1 - verts[ci.ali[trii * 3 + 0]].Tv,
                                verts[ci.ali[trii * 3 + 2]].Tu, 1 - verts[ci.ali[trii * 3 + 2]].Tv,
                                verts[ci.ali[trii * 3 + 1]].Tu, 1 - verts[ci.ali[trii * 3 + 1]].Tv
                                );
                        }
                        maker.EndMesh(ci.vifi);
                    }
                    maker.Finish();
                    File.WriteAllText(basedir + "\\mesh.py", maker.ToString(), Encoding.ASCII);

                    basepici += pici;
                }
            }
            finally {
                vb.Unlock();
            }

            Process.Start("explorer.exe", " bpyexp");
        }

        class Mkbpy {
            int i = 0;
            int cntv = 0;
            StringWriter wr = new StringWriter();

            String vcoords = "";
            String vfaces = "";
            StringWriter uvs = new StringWriter();
            int uvi = 0;
            List<int> alRefMati = new List<int>();
            StringWriter vcs = new StringWriter();

            Matrix mtxLoc2Blender;

            public Mkbpy() {
                wr.WriteLine("# http://f11.aaa.livedoor.jp/~hige/index.php?%5B%5BPython%A5%B9%A5%AF%A5%EA%A5%D7%A5%C8%5D%5D");
                wr.WriteLine("# http://www.blender.org/documentation/248PythonDoc/index.html");
                wr.WriteLine();
                wr.WriteLine("# good for Blender 2.4.8a");
                wr.WriteLine("# with Python 2.5.4");
                wr.WriteLine();
                wr.WriteLine("# Import instruction:");
                wr.WriteLine("# * Launch Blender 2.4.8a");
                wr.WriteLine("# * In Blender, type Shift+F11, then open then Script Window");
                wr.WriteLine("# * Type Alt+O or [Text]menu -> [Open], then select and open mesh.py");
                wr.WriteLine("# * Type Alt+P or [Text]menu -> [Run Python Script] to run the script!");
                wr.WriteLine("# * Use Ctrl+LeftArrow, Ctrl+RightArrow to change window layout.");
                wr.WriteLine();
                wr.WriteLine("print \"-- Start importing \"");
                wr.WriteLine();
                wr.WriteLine("import Blender");
                wr.WriteLine();
                wr.WriteLine("scene = Blender.Scene.GetCurrent()");
                wr.WriteLine();

                mtxLoc2Blender = Matrix.RotationX(90.0f / 180.0f * 3.14159f);
                float f = 1.0f / 100;
                mtxLoc2Blender = Matrix.Multiply(mtxLoc2Blender, Matrix.Scaling(-f, f, f));
            }

            public void StartTex() {
                wr.WriteLine("imgs = []");
                wr.WriteLine("mats = []");
            }
            public void AddTex(String fp, String tid, String mid) {
                wr.WriteLine("img = Blender.Image.Load('{0}')", fp.Replace("\\", "/"));
                wr.WriteLine("tex = Blender.Texture.New('{0}')", tid);
                wr.WriteLine("tex.image = img");
                wr.WriteLine("mat = Blender.Material.New('{0}')", mid);
                wr.WriteLine("mat.setTexture(0, tex, Blender.Texture.TexCo.UV, Blender.Texture.MapTo.COL)");
                wr.WriteLine("mat.setMode('Shadeless')");
                wr.WriteLine("mats += [mat]");
                wr.WriteLine("imgs += [img]");
            }
            public void EndTex() {

            }

            public void StartMesh() {
                vcoords = "";
                cntv = 0;

                uvs = new StringWriter();
                uvi = 0;

                alRefMati.Clear();

                vcs = new StringWriter();
            }
            public void AddV(CustomVertex.PositionColoredTextured v) {
                if (vcoords != "")
                    vcoords += ",";
                Vector3 v3 = Vector3.TransformCoordinate(v.Position, mtxLoc2Blender);
                vcoords += string.Format("[{0},{1},{2}]", v3.X, v3.Y, v3.Z);

                cntv++;
            }

            public void AddColorVtx(Color[] clrs) {
                for (int x = 0; x < clrs.Length; x++) {
                    vcs.WriteLine("me.faces[{0}].col[{1}].a = {2}", uvi, x, clrs[x].A);
                    vcs.WriteLine("me.faces[{0}].col[{1}].r = {2}", uvi, x, clrs[x].R);
                    vcs.WriteLine("me.faces[{0}].col[{1}].g = {2}", uvi, x, clrs[x].G);
                    vcs.WriteLine("me.faces[{0}].col[{1}].b = {2}", uvi, x, clrs[x].B);
                }
            }

            public void AddTuv(int texi, float tu0, float tv0, float tu1, float tv1, float tu2, float tv2) {
                Debug.Assert(texi < 256);

                if (alRefMati.IndexOf(texi) < 0)
                    alRefMati.Add(texi);
                int mati = alRefMati.IndexOf(texi);

                uvs.WriteLine("me.faces[{0}].uv = [Blender.Mathutils.Vector({1:0.000},{2:0.000}),Blender.Mathutils.Vector({3:0.000},{4:0.000}),Blender.Mathutils.Vector({5:0.000},{6:0.000}),]", uvi, tu0, tv0, tu1, tv1, tu2, tv2);
                uvs.WriteLine("me.faces[{0}].mat = {1}", uvi, mati);
                uvs.WriteLine("me.faces[{0}].image = imgs[{1}]", uvi, texi);
                uvi++;
            }

            public void EndMesh(int vifi) {
                if (cntv == 0)
                    return;

                vfaces = "";
                for (int x = 0; x < cntv / 3; x++) {
                    if (vfaces != "")
                        vfaces += ",";
                    vfaces += string.Format("[{0},{1},{2}]", 3 * x, 3 * x + 1, 3 * x + 2);
                }

                String meshName = string.Format("vifpkt{0:0000}-mesh", vifi);
                String objName = string.Format("vifpkt{0:0000}-obj{1}", vifi, i);
                i++;

                wr.WriteLine("coords = [" + vcoords + "]");
                wr.WriteLine("faces = [" + vfaces + "]");
                wr.WriteLine("me = Blender.Mesh.New('" + meshName + "')");
                wr.WriteLine("me.verts.extend(coords)");
                wr.WriteLine("me.faces.extend(faces)");
                wr.WriteLine("me.faceUV = True");
                for (int x = 0; x < alRefMati.Count; x++) {
                    wr.WriteLine("me.materials += [mats[{0}]]", alRefMati[x]);
                }
                wr.Write(uvs.ToString());
                wr.WriteLine("me.vertexColors = True");
                wr.Write(vcs.ToString());
                wr.WriteLine("ob = scene.objects.new(me, '" + objName + "')");
                wr.WriteLine("");
            }

            public void Finish() {
                wr.WriteLine("print \"-- Ended importing \"");
            }

            public override string ToString() {
                return wr.ToString();
            }
        }

        delegate void _SetPos(Vector3 v);

        private void eyeX_ValueChanged(object sender, EventArgs e) {
            p1.Invalidate();
        }

        [Flags]
        enum Keyrun {
            None = 0,
            W = 1, S = 2, A = 4, D = 8, Up = 16, Down = 32,
        }

        Keyrun kr = Keyrun.None;

        private void p1_KeyDown(object sender, KeyEventArgs e) {
            switch (e.KeyCode) {
                case Keys.W: kr |= Keyrun.W; break;
                case Keys.S: kr |= Keyrun.S; break;
                case Keys.A: kr |= Keyrun.A; break;
                case Keys.D: kr |= Keyrun.D; break;
                case Keys.Up: kr |= Keyrun.Up; break;
                case Keys.Down: kr |= Keyrun.Down; break;
            }
            if (kr != Keyrun.None && !timerRun.Enabled) timerRun.Start();
        }

        private void p1_KeyUp(object sender, KeyEventArgs e) {
            switch (e.KeyCode) {
                case Keys.W: kr &= ~Keyrun.W; break;
                case Keys.S: kr &= ~Keyrun.S; break;
                case Keys.A: kr &= ~Keyrun.A; break;
                case Keys.D: kr &= ~Keyrun.D; break;
                case Keys.Up: kr &= ~Keyrun.Up; break;
                case Keys.Down: kr &= ~Keyrun.Down; break;
            }
            if (kr == Keyrun.None && timerRun.Enabled) timerRun.Stop();
        }

        Vector3 LeftVec {
            get {
                return
                    Vector3.TransformCoordinate(
                        Vector3.TransformCoordinate(
                            TargetX,
                            Matrix.RotationY(-90.0f / 180.0f * 3.14159f)
                            ),
                        Matrix.RotationYawPitchRoll(
                            0,
                            Convert.ToSingle(pitch.Value) / 180.0f * 3.14159f,
                            0
                            )
                        );
            }
        }

        int Speed { get { return 0 != (ModifierKeys & Keys.Shift) ? 60 : 30; } }

        private void timerRun_Tick(object sender, EventArgs e) {
            if (0 != (kr & Keyrun.W)) {
                CameraEye += Target * Speed;
            }
            if (0 != (kr & Keyrun.S)) {
                CameraEye -= Target * Speed;
            }
            if (0 != (kr & Keyrun.A)) {
                CameraEye += LeftVec * Speed;
            }
            if (0 != (kr & Keyrun.D)) {
                CameraEye -= LeftVec * Speed;
            }
            if (0 != (kr & Keyrun.Up)) {
                CameraEye += Vector3.UnitY * Speed;
            }
            if (0 != (kr & Keyrun.Down)) {
                CameraEye -= Vector3.UnitY * Speed;
            }
            p1.Invalidate();
        }

        private void p1_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e) {
            switch (e.KeyCode) {
                case Keys.Up:
                case Keys.Down:
                    e.IsInputKey = true;
                    break;
            }
        }

        private void Visf_FormClosing(object sender, FormClosingEventArgs e) {
            alDeleter.Reverse();
            foreach (ComObject o in alDeleter) {
                o.Dispose();
            }
            if (pvi != null)
                pvi.Dispose();
        }

        private void cbFog_CheckedChanged(object sender, EventArgs e) {
            p1.Invalidate();
        }

        bool TestColl(Vector3 v) {
            v.X = -v.X;
            v.Y = -v.Y;
            v.Z = -v.Z;
            bool hit = false;
            foreach (Co2 o2 in coll.alCo2) {
                if (o2.Min.X <= v.X && o2.Min.Y <= v.Y && o2.Min.Z <= v.Z && v.X <= o2.Max.X && v.Y <= o2.Max.Y && v.Z <= o2.Max.Z) {
                    int cnt = 0, total = 0;
                    for (int t = o2.Co3frm; t < o2.Co3to; t++) {
                        Co3 o3 = coll.alCo3[t];
                        cnt += (Plane.DotCoordinate(coll.alCo5[o3.PlaneCo5], v) > 0) ? 1 : 0;
                        total++;
                    }
                    hit |= (total != 0) && (cnt == total);
                }
            }
            return hit;
        }

        private void tsbShowColl_Click(object sender, EventArgs e) { p1.Invalidate(); }

        private void tsbShowGeo_Click(object sender, EventArgs e) { p1.Invalidate(); }

        private void p1_SizeChanged(object sender, EventArgs e) {
            p1.Invalidate();
        }

    }

    namespace Put {
        public interface IPuteffect {
            void Save();
            void Apply();
            void Restore();
        }

        public class Putfragment {
            public int baseVertexIndex = 0, minimumVertexIndex = 0, vertexCount = 0, startIndex = 0, primitiveCount = 0;
            public IPuteffect effect = null;

            public Putfragment(int baseVertexIndex, int minimumVertexIndex, int vertexCount, int startIndex, int primitiveCount) {
                this.baseVertexIndex = baseVertexIndex;
                this.minimumVertexIndex = minimumVertexIndex;
                this.vertexCount = vertexCount;
                this.startIndex = startIndex;
                this.primitiveCount = primitiveCount;
            }
        }

        public class Putvi : IDisposable {
            public VertexBuffer vb = null;
            public IndexBuffer ib = null;

            public Putvi(Putbox p, Device dev) {
                {
                    vb = new VertexBuffer(
                        dev,
                        CustomVertex.Position.Size * p.alv.Count,
                        Usage.Points,
                        CustomVertex.Position.Format,
                        Pool.Managed
                        );

                    DataStream ds = vb.Lock(0, 0, LockFlags.Discard);
                    try {
                        ds.WriteRange(p.alv.ToArray());
                    }
                    finally {
                        vb.Unlock();
                    }
                }

                {
                    ib = new IndexBuffer(
                        dev,
                        4 * p.ali.Count,
                        Usage.Points,
                        Pool.Managed,
                        false
                        );

                    DataStream ds = ib.Lock(0, 0, LockFlags.Discard);
                    try {
                        ds.WriteRange(p.ali.ToArray());
                    }
                    finally {
                        ib.Unlock();
                    }
                }
            }

            #region IDisposable メンバ

            public void Dispose() {
                if (vb != null) vb.Dispose();
                if (ib != null) ib.Dispose();
            }

            #endregion
        }

        public class Putbox {
            public List<Vector3> alv = new List<Vector3>();
            public List<int> ali = new List<int>();
            public List<Putfragment> alf = new List<Putfragment>();

            public Putfragment Add(Co1 o1) {
                Putfragment res = AddBox(o1.Min, o1.Max);
                return res;
            }
            public Putfragment Add(Co2 o1) {
                Putfragment res = AddBox(o1.Min, o1.Max);
                return res;
            }

            public Putfragment AddBox(Vector3 v0, Vector3 v1) {
                int iv = alv.Count;
                int ii = ali.Count;

                alv.Add(new Vector3(Math.Min(v0.X, v1.X), Math.Min(v0.Y, v1.Y), Math.Min(v0.Z, v1.Z)));
                alv.Add(new Vector3(Math.Max(v0.X, v1.X), Math.Min(v0.Y, v1.Y), Math.Min(v0.Z, v1.Z)));
                alv.Add(new Vector3(Math.Min(v0.X, v1.X), Math.Max(v0.Y, v1.Y), Math.Min(v0.Z, v1.Z)));
                alv.Add(new Vector3(Math.Max(v0.X, v1.X), Math.Max(v0.Y, v1.Y), Math.Min(v0.Z, v1.Z)));
                alv.Add(new Vector3(Math.Min(v0.X, v1.X), Math.Min(v0.Y, v1.Y), Math.Max(v0.Z, v1.Z)));
                alv.Add(new Vector3(Math.Max(v0.X, v1.X), Math.Min(v0.Y, v1.Y), Math.Max(v0.Z, v1.Z)));
                alv.Add(new Vector3(Math.Min(v0.X, v1.X), Math.Max(v0.Y, v1.Y), Math.Max(v0.Z, v1.Z)));
                alv.Add(new Vector3(Math.Max(v0.X, v1.X), Math.Max(v0.Y, v1.Y), Math.Max(v0.Z, v1.Z)));

                //       * 6 - 7
                // 2 - 3 | | U |
                // | D | | 4 - 5
                // 0 - 1 *

                AddQuad(0, 2, 3, 1); // Bottom
                AddQuad(4, 5, 7, 6); // Up
                AddQuad(2, 6, 7, 3); // n
                AddQuad(0, 1, 5, 4); // s
                AddQuad(0, 4, 6, 2); // w
                AddQuad(1, 3, 7, 5); // e

                Putfragment res = new Putfragment(iv, 0, 8, ii, 2 * 6);
                return res;
            }

            void AddQuad(int i0, int i1, int i2, int i3) {
                ali.Add(i0); ali.Add(i1); ali.Add(i2);
                ali.Add(i2); ali.Add(i3); ali.Add(i0);
            }
        }
    }
}