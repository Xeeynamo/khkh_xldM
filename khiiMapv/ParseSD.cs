using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Diagnostics;

namespace khiiMapv {
    public class Wavo {
        public string fn;
        public byte[] bin;

        public Wavo(string fn, byte[] bin) {
            this.fn = fn;
            this.bin = bin;
        }
    }
    public class ParseSD {
        public static Wavo[] ReadIV(Stream fs) {
            BinaryReader br = new BinaryReader(fs);
            BER ber = new BER(br);

            fs.Position = 0x0C;
            int cnt = br.ReadInt32();
            KeyValuePair<int, int>[] poslen = new KeyValuePair<int, int>[cnt];

            fs.Position = 0x10;
            for (int x = 0; x < cnt; x++) {
                int k = br.ReadInt32();
                if (k >= 0) k += 16 + (8 * cnt);
                int v = br.ReadInt32();
                poslen[x] = new KeyValuePair<int, int>(k, v);
            }

            List<Wavo> al = new List<Wavo>();
            for (int x = 0; x < poslen.Length; x++) {
                int vagoff = poslen[x].Key;
                if (vagoff < 0)
                    continue;
                {
                    fs.Position = vagoff + 0x0C;
                    int cb = ber.ReadInt32() - 0x20;

                    fs.Position = vagoff + 0x10;
                    int sps = ber.ReadInt32();

                    fs.Position = vagoff + 0x40;
                    al.Add(new Wavo(
                        x.ToString("00") + ".wav", 
                        SPUConv.ToWave(new MemoryStream(br.ReadBytes(cb)), sps)
                        ));
                }
            }

            return al.ToArray();
        }
        public static Wavo[] ReadWD(Stream fs) {
            BinaryReader br = new BinaryReader(fs);

            fs.Position = 0x08;
            int cnt1 = br.ReadInt32(); // tbl1 
            int cnt2 = br.ReadInt32(); // tbl2

            int[] alofftbl2 = new int[cnt1];

            fs.Position = 0x20; // tbl1
            for (int x = 0; x < cnt1; x++) {
                alofftbl2[x] = br.ReadInt32();
            }
            int gakki = 0, ontei = 0;
            List<Wavi> alwavi = new List<Wavi>();
            for (int x = 0; x < cnt2; x++) {
                int off = 0x20 + 4 * ((cnt1 + 3) & (~3)) + 32 * x;
                int fki = Array.IndexOf(alofftbl2, off);
                if (fki >= 0) { gakki = fki; ontei = 0; }
                fs.Position = off + 16;
                if (br.ReadInt64() == 0 && br.ReadInt64() == 0) continue;
                fs.Position = off + 4;
                Wavi o = new Wavi();
                o.off = br.ReadInt32();
                o.gakki = gakki;
                o.ontei = ontei; ontei++;
                fs.Position = off + 22;
                o.sps = 0 + br.ReadUInt16();
                alwavi.Add(o);
            }
            List<Wavo> al = new List<Wavo>();
            for (int x = 0; x < alwavi.Count; x++) {
                Wavi o = alwavi[x];
                int off = 0x20 + 16 * ((cnt1 + 3) & (~3)) + 32 * cnt2 + o.off;
                fs.Position = off;
                while (fs.Position < fs.Length) {
                    byte[] bin = br.ReadBytes(16);
                    int t = 0;
                    while (t < 16 && bin[t] == 0) t++;
                    if (t == 16) break;
                }
                int sps = o.sps;
                int cb = Convert.ToInt32(fs.Position) - off - 0x20;
                fs.Position = off;
                al.Add(new Wavo(
                    o.gakki.ToString("000") + "." + o.ontei.ToString("00") + ".wav", 
                    SPUConv.ToWave(new MemoryStream(br.ReadBytes(cb)), sps))
                    );
            }
            return al.ToArray();
        }

        class Wavi {
            public int off;
            public int gakki, ontei, sps;
        }

        class BER { // BIG Endian Reader
            BinaryReader br;

            public BER(BinaryReader br) {
                this.br = br;
            }

            public int ReadInt32() {
                byte[] bin = br.ReadBytes(4);
                return (bin[0] << 24) | (bin[1] << 16) | (bin[2] << 8) | (bin[3]);
            }

            public short ReadUInt16() {
                int r = br.ReadByte() << 8;
                return (short)(r | br.ReadByte());
            }
        }

        class SPUConv {
            static int[,] f = new int[,] {
                        {    0,  0  },
                        {   60,  0  },
                        {  115, -52 },
                        {   98, -55 },
                        {  122, -60 } };

            public static byte[] ToWave(MemoryStream fs, int nSamplesPerSec) {
                int s_1 = 0, s_2 = 0;
                List<int> SB = new List<int>();
                while (fs.Position + 16 <= fs.Length) {
                    byte[] start = new byte[16];
                    Trace.Assert(16 == fs.Read(start, 0, 16));

                    {
                        int predict_nr = (int)start[0];
                        int shift_factor = predict_nr & 0xf;
                        predict_nr >>= 4;
                        int flags = (int)start[1];

                        for (int nSample = 0; nSample < 14; nSample++) {
                            int d = (int)start[2 + nSample];
                            int s = ((d & 0xf) << 12);
                            if (0 != (s & 0x8000)) s = (int)((uint)s | 0xffff0000U);

                            int fa = (s >> shift_factor);
                            fa = fa + ((s_1 * f[predict_nr, 0]) >> 6) + ((s_2 * f[predict_nr, 1]) >> 6);
                            s_2 = s_1; s_1 = fa;
                            s = ((d & 0xf0) << 8);

                            SB.Add(fa);

                            if (0 != (s & 0x8000)) s = (int)((uint)s | 0xffff0000U);
                            fa = (s >> shift_factor);
                            fa = fa + ((s_1 * f[predict_nr, 0]) >> 6) + ((s_2 * f[predict_nr, 1]) >> 6);
                            s_2 = s_1; s_1 = fa;

                            SB.Add(fa);
                        }
                    }
                }

                MemoryStream os = new MemoryStream();
                int nSamples = SB.Count;
                //int nSamplesPerSec = 44100;
                BinaryWriter wr = new BinaryWriter(os);
                wr.Write(Encoding.ASCII.GetBytes("RIFF"));
                wr.Write((int)(4 + (8) + (2 + 2 + 4 + 4 + 2 + 2 + 2) + (8) + (4) + (8) + (2 * nSamples)));
                wr.Write(Encoding.ASCII.GetBytes("WAVE"));
                wr.Write(Encoding.ASCII.GetBytes("fmt "));
                wr.Write((int)(2 + 2 + 4 + 4 + 2 + 2 + 2));
                wr.Write((short)(1));
                wr.Write((short)(1));
                wr.Write((int)(nSamplesPerSec));
                wr.Write((int)(nSamplesPerSec * 2));
                wr.Write((short)(2));
                wr.Write((short)(16));
                wr.Write((short)(0));
                wr.Write(Encoding.ASCII.GetBytes("fact"));
                wr.Write((int)(4));
                wr.Write((int)(nSamples));
                wr.Write(Encoding.ASCII.GetBytes("data"));
                wr.Write((int)(2 * nSamples));
                for (int x = 0; x < nSamples; x++) {
                    int v = Math.Max(-32768, Math.Min(32767, SB[x]));
                    wr.Write((ushort)(v));
                }
                return os.ToArray();
            }

            public static byte[] ToWave2ch(MemoryStream fs, int nSamplesPerSec) {
                int s_1x = 0, s_2x = 0; // left channel
                int s_1y = 0, s_2y = 0; // right channel
                List<int> SBx = new List<int>();
                List<int> SBy = new List<int>();
                while (fs.Position + 32 <= fs.Length) {
                    byte[] start = new byte[16];

                    {
                        Trace.Assert(16 == fs.Read(start, 0, 16));

                        int predict_nr = (int)start[0];
                        int shift_factor = predict_nr & 0xf;
                        predict_nr >>= 4;
                        int flags = (int)start[1];

                        for (int nSample = 0; nSample < 14; nSample++) {
                            int d = (int)start[2 + nSample];
                            int s = ((d & 0xf) << 12);
                            if (0 != (s & 0x8000)) s = (int)((uint)s | 0xffff0000U);

                            int fa = (s >> shift_factor);
                            fa = fa + ((s_1x * f[predict_nr, 0]) >> 6) + ((s_2x * f[predict_nr, 1]) >> 6);
                            s_2x = s_1x; s_1x = fa;
                            s = ((d & 0xf0) << 8);

                            SBx.Add(fa);

                            if (0 != (s & 0x8000)) s = (int)((uint)s | 0xffff0000U);
                            fa = (s >> shift_factor);
                            fa = fa + ((s_1x * f[predict_nr, 0]) >> 6) + ((s_2x * f[predict_nr, 1]) >> 6);
                            s_2x = s_1x; s_1x = fa;

                            SBx.Add(fa);
                        }
                    }

                    {
                        Trace.Assert(16 == fs.Read(start, 0, 16));

                        int predict_nr = (int)start[0];
                        int shift_factor = predict_nr & 0xf;
                        predict_nr >>= 4;
                        int flags = (int)start[1];

                        for (int nSample = 0; nSample < 14; nSample++) {
                            int d = (int)start[2 + nSample];
                            int s = ((d & 0xf) << 12);
                            if (0 != (s & 0x8000)) s = (int)((uint)s | 0xffff0000U);

                            int fa = (s >> shift_factor);
                            fa = fa + ((s_1y * f[predict_nr, 0]) >> 6) + ((s_2y * f[predict_nr, 1]) >> 6);
                            s_2y = s_1y; s_1y = fa;
                            s = ((d & 0xf0) << 8);

                            SBy.Add(fa);

                            if (0 != (s & 0x8000)) s = (int)((uint)s | 0xffff0000U);
                            fa = (s >> shift_factor);
                            fa = fa + ((s_1y * f[predict_nr, 0]) >> 6) + ((s_2y * f[predict_nr, 1]) >> 6);
                            s_2y = s_1y; s_1y = fa;

                            SBy.Add(fa);
                        }
                    }
                }

                MemoryStream os = new MemoryStream();
                int nSamples = SBx.Count;
                //int nSamplesPerSec = 44100;
                BinaryWriter wr = new BinaryWriter(os);
                wr.Write(Encoding.ASCII.GetBytes("RIFF"));
                wr.Write((int)(4 + (8) + (2 + 2 + 4 + 4 + 2 + 2 + 2) + (8) + (4) + (8) + (4 * nSamples)));
                wr.Write(Encoding.ASCII.GetBytes("WAVE"));
                wr.Write(Encoding.ASCII.GetBytes("fmt "));
                wr.Write((int)(2 + 2 + 4 + 4 + 2 + 2 + 2));
                wr.Write((short)(1));
                wr.Write((short)(2));
                wr.Write((int)(nSamplesPerSec));
                wr.Write((int)(nSamplesPerSec * 4));
                wr.Write((short)(4));
                wr.Write((short)(16));
                wr.Write((short)(0));
                wr.Write(Encoding.ASCII.GetBytes("fact"));
                wr.Write((int)(4));
                wr.Write((int)(nSamples));
                wr.Write(Encoding.ASCII.GetBytes("data"));
                wr.Write((int)(4 * nSamples));
                for (int x = 0; x < nSamples; x++) {
                    {
                        int v = Math.Max(-32768, Math.Min(32767, SBx[x]));
                        wr.Write((ushort)(v));
                    }
                    {
                        int v = Math.Max(-32768, Math.Min(32767, SBy[x]));
                        wr.Write((ushort)(v));
                    }
                }
                return os.ToArray();
            }
        }
    }
}
