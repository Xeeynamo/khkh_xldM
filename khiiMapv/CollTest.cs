using System;
using System.Collections.Generic;
using System.Text;
using SlimDX;
using System.IO;

namespace khiiMapv.CollTest {
    public class CollReader {
        public List<Co1> alCo1 = new List<Co1>();
        public List<Co2> alCo2 = new List<Co2>();
        public List<Co3> alCo3 = new List<Co3>();
        public List<Vector4> alCo4 = new List<Vector4>();
        public List<Plane> alCo5 = new List<Plane>();

        public void Read(Stream si) {
            BinaryReader br = new BinaryReader(si);
            if (br.ReadByte() != (byte)'C') throw new InvalidDataException();
            if (br.ReadByte() != (byte)'O') throw new InvalidDataException();
            if (br.ReadByte() != (byte)'C') throw new InvalidDataException();
            if (br.ReadByte() != (byte)'T') throw new InvalidDataException();
            if (br.ReadInt32() != 1) throw new InvalidDataException();

            {
                si.Position = 8;
                int cntCo1 = br.ReadInt32();
                si.Position = 0x18;
                int offCo1 = br.ReadInt32();
                int lenCo1 = br.ReadInt32();

                si.Position = offCo1;
                for (int x = 0; x < cntCo1; x++) alCo1.Add(new Co1(br));
            }

            {
                si.Position = 0x20;
                int off = br.ReadInt32();
                int len = br.ReadInt32();

                si.Position = off;
                while (si.Position < off + len) alCo2.Add(new Co2(br));
            }

            {
                si.Position = 0x28;
                int off = br.ReadInt32();
                int len = br.ReadInt32();

                si.Position = off;
                while (si.Position < off + len) alCo3.Add(new Co3(br));
            }

            {
                si.Position = 0x30;
                int off = br.ReadInt32();
                int len = br.ReadInt32();

                si.Position = off;
                while (si.Position < off + len) {
                    Vector4 v = new Vector4();
                    v.X = br.ReadSingle();
                    v.Y = br.ReadSingle();
                    v.Z = br.ReadSingle();
                    v.W = br.ReadSingle();
                    alCo4.Add(v);
                }
            }

            {
                si.Position = 0x38;
                int off = br.ReadInt32();
                int len = br.ReadInt32();

                si.Position = off;
                while (si.Position < off + len) {
                    Plane v = new Plane();
                    v.Normal.X = br.ReadSingle();
                    v.Normal.Y = br.ReadSingle();
                    v.Normal.Z = br.ReadSingle();
                    v.D = br.ReadSingle();
                    alCo5.Add(v);
                }
            }
        }
    }
    
    public class Co3 {
        public short v0, v2, v4, v6, v8, va, vc, ve;

        public int vi0 { get { return v2; } }
        public int vi1 { get { return v4; } }
        public int vi2 { get { return v6; } }
        public int vi3 { get { return v8; } }

        public int PlaneCo5 { get { return va; } }

        public override string ToString() {
            return String.Format("{0,4} PolyCo4({1,4},{2,4},{3,4},{4,4}) PlaneCo5({5,3}) {6,3} {7,3}"
                , v0, v2, v4, v6, v8, va, vc, ve);
        }

        public Co3(BinaryReader br) {
            v0 = br.ReadInt16();
            v2 = br.ReadInt16();
            v4 = br.ReadInt16();
            v6 = br.ReadInt16();
            v8 = br.ReadInt16();
            va = br.ReadInt16();
            vc = br.ReadInt16();
            ve = br.ReadInt16();
        }
    }

    public class Co2 {
        public short v00, v02, v04, v06, v08, v0a, v0c, v0e, v10, v12;

        public Vector3 Min { get { return new Vector3(v00, v02, v04); } }
        public Vector3 Max { get { return new Vector3(v06, v08, v0a); } }
        public int Co3frm { get { return v0c; } }
        public int Co3to { get { return v0e; } }

        public override string ToString() {
            return String.Format("bbox-min({0,6}, {1,6}, {2,6}) bbox-max({3,6}, {4,6}, {5,6}) Co3frmTo({6,3}, {7,3}) {8:x4} {9,3}"
                , v00, v02, v04, v06, v08, v0a, v0c, v0e, v10, v12
                );
        }

        public Co2(BinaryReader br) {
            v00 = br.ReadInt16();
            v02 = br.ReadInt16();
            v04 = br.ReadInt16();
            v06 = br.ReadInt16();
            v08 = br.ReadInt16();
            v0a = br.ReadInt16();
            v0c = br.ReadInt16();
            v0e = br.ReadInt16();
            v10 = br.ReadInt16();
            v12 = br.ReadInt16();
        }
    }

    public class Co1 {
        public short v00, v02, v04, v06, v08, v0a, v0c, v0e;
        public short v10, v12, v14, v16, v18, v1a, v1c, v1e;

        public Vector3 Min { get { return new Vector3(v10, v12, v14); } }
        public Vector3 Max { get { return new Vector3(v16, v18, v1a); } }

        public override string ToString() {
            return String.Format("?({0,3},{1,3},{2,3},{3,3},{4,3},{5,3},{6,3},{7,3}) "
                , v00
                , v02
                , v04
                , v06
                , v08
                , v0a
                , v0c
                , v0e
                ) +
                String.Format("bbox-min({0,7}, {1,6}, {2,6}) bbox-max({3,6}, {4,6}, {5,6}) {6,3} {7,3}"
                , v10
                , v12
                , v14
                , v16
                , v18
                , v1a
                , v1c
                , v1e
                );
        }

        public Co1(BinaryReader br) {
            v00 = br.ReadInt16();
            v02 = br.ReadInt16();
            v04 = br.ReadInt16();
            v06 = br.ReadInt16();
            v08 = br.ReadInt16();
            v0a = br.ReadInt16();
            v0c = br.ReadInt16();
            v0e = br.ReadInt16();

            v10 = br.ReadInt16();
            v12 = br.ReadInt16();
            v14 = br.ReadInt16();
            v16 = br.ReadInt16();
            v18 = br.ReadInt16();
            v1a = br.ReadInt16();
            v1c = br.ReadInt16();
            v1e = br.ReadInt16();
        }
    }
}
